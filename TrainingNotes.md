Training Notes
=====================

These notes might be organized by  topic some days.

 * MDN loss : do not use Relu as last activation. Seems to harm significantly the convergence. Use a saturating activation instead like tanh(x) or (1+tanh(x))
 * Use matrix weight & bias regularization, at least at the begining of training.  Regularization parameters can be set to 0 once the training has converged enough (these are kernel_regularizer and bias_regularizer in keras, see ModelDefinitions.py)
 * LGK loss : alpha parameter < 5e-3 can produce significant fluctuations when the width of the target distribution is large
 * MDN loss : truncation <0.8sigma can produce significant fluctuations when the width of the target distribution is large
 * Input nornalization is important. Here chosen in [-1,1].
 * Output normalization is important if we want to predict modes >> 1 . If mode ~ 1, output normalization is not necessary with final activation set as tanhp=(1+tanh)
 * for jet JES & JMS training with O(100M) events, large batch size ~100K seem to work better.
 * ...