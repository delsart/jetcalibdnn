"""Defintions of models to perform Jet calibration

This module defines helper objects which build keras.Model for a Trainer object.
The objects are instances of ModelDefiner and have a __call__ methods so can be used as a function.
Concrete model building classes typically inherits ModelDefiner as well as other classes, each defining
the head, the core and the tail of the NN.

Such model defining objects are communicated to the trainer through the configuration by setting them as in
  config.modelBuilder = MyDefinerClass() 

The object is then called in the Trainer.setupModel() function.

"""
import numpy as np

from .ImportKeras import *
from . import NNUtils as utils


## ***********************************************************
from collections import defaultdict
_layerNames = defaultdict( int )
def _layerName(n=None):
    global _layerNames
    if n is None :
        _layerNames= defaultdict( int )
        return
    if isinstance(n,layers.Layer):
        n = n.__class__.__name__
    i = _layerNames[n]
    _layerNames[n] = i+1
    if i==0:
        return n
    return f'{n}_{i}'


def denseBatchNormF(layer,*args, **kwargs):
    """A shortcut to add a Dense layer with dropout, batch normalization (broken ???), custom activations,... """
    batchNorm = kwargs.pop('doBatchNorm',False)
    dropout = kwargs.pop('dropout', None)
    activ = kwargs.pop('activation', 'relu')    
    activName = kwargs.pop('activ_name', None )

    kwargs['name'] = _layerName(kwargs.get('name','dense') )
    nl =  layers.Dense( *args,  **kwargs)(layer)
    if batchNorm:
        nl =  layers.BatchNormalization(name='batchnorm')(nl)
    nl = layers.Activation( utils.activationDict.get(activ, activ) , name=activName)(nl)

    if dropout:
        nl= layers.Dropout(dropout, ) (nl)
    return nl



def symetrize(l, f=-1):
    return [f*u for u in  l[::-1] ] + l[1:]

def mirror(l, f=-1):
    return [f*u for u in  l[::-1] ] 




## ***********************************************************

class ModelDefiner:
    N = 100
    core_constraints_raw = dict(
        #batchNorm=True,
        kernel_regularizer=keras.regularizers.l2(0.0001),
        bias_regularizer = keras.regularizers.l2(0.0001),
    )

    tail_constraints_raw = dict(
        bias_regularizer = keras.regularizers.l2(0.001),
        kernel_regularizer=keras.regularizers.l2(0.001)
    )

    

    core_constraints_ni = dict(
        #batchNorm=True,
        kernel_regularizer=keras.regularizers.l2(1e-6),
        bias_regularizer = keras.regularizers.l2(1e-4),
    )
    tail_constraints_ni = dict(
        bias_regularizer = keras.regularizers.l2(0.001),
        kernel_regularizer=keras.regularizers.l2(0.001)
    )

    batchNorm = False

    last_activation='tanh'
    scale_output = True  # this is set automatically from last_activation
    

    # If not None, bypass the evaluation of number of output values from trainer.config
    forceNPredValues = None
    #  (used to build copies of NN predicting only some of the output)
    
    def __init__(self, **args):
        for (k,v) in args.items():
            setattr(self,k,v)        
        self.scale_output = ( self.last_activation in ['tanh', 'tanh0']  )
            
        
    def __call__(self,trainer, ):
        self.modelTags = []

        _layerName() # reset count
        
        if not trainer.config.isNIstep:
            self.core_constraints = self.core_constraints_raw
            self.tail_constraints = self.tail_constraints_raw
        else:
            self.core_constraints = self.core_constraints_ni
            self.tail_constraints = self.tail_constraints_ni
            
        inputN = len(trainer.config.features)
        inputs =layers.Input(shape=(inputN,) )

        self.inputs = inputs

        if trainer.config.lossType !="MDN":
            if self.last_activation=='tanh0':
                self.last_activation='tanh'
        
        layer = self.modelHead(trainer, inputs)
        layer = self.modelCore(trainer, layer)
        layer = self.modelTail(trainer, layer)

        net = Model(inputs=inputs, outputs=layer)

        if not self.scale_output:
            self.modelTags +=['uo']
        self.modelTags += ['N'+str(self.N)]
            
        self.setModelTag(trainer)

        return net

    def setModelTag(self,trainer):
        trainer.config.modelTag = ''.join(self.modelTags)


    def batchNormTag(self):
        return ['BN'] if self.batchNorm else []


    def nPredictedValues(self, trainer):
        if self.forceNPredValues is not None:
            return self.forceNPredValues

        return trainer.config.nPredictedParam
        # ntype =  len(config.originalTargets) # 1 if only predicting E, 2 if predicting E and M

        # return len(config.targets) // ntype

    
class DenseCore0:
    def modelCore(self,trainer, layer):
        N = self.N
        l = layer
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        l=denseBatchNormF( l, int(0.8*N),activation='mish' ,dropout=0.15,**l_args )
        l=denseBatchNormF( l, int(1.3*N),activation='mish' ,dropout=0.2, **l_args)
        l=denseBatchNormF( l, int(0.3*N),activation='mish' ,dropout=0.2, **l_args)
        self.modelTags += ["Co0"]
        return l

    

class DenseCore1:
    def modelCore(self,trainer, layer):
        N = self.N
        l = layer
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        l=denseBatchNormF( l, int(0.8*N),activation='mish' ,dropout=0.15,**l_args )
        l=denseBatchNormF( l, int(1.1*N),activation='mish' ,dropout=0.2, **l_args)
        l=denseBatchNormF( l, int(0.3*N),activation='mish' ,dropout=0.2, **l_args)
        self.modelTags += ["Co1"]
        return l


    
class DenseCore00:
    def modelCore(self,trainer, layer):
        N = self.N
        l = layer
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        l=denseBatchNormF( l, int(N),activation='mish' ,dropout=0.15,**l_args )
        l=denseBatchNormF( l, int(N),activation='mish' ,dropout=0.2, **l_args)
        l=denseBatchNormF( l, int(N),activation='mish' ,dropout=0.2, **l_args)
        self.modelTags += ["Co00"]
        return l

class DenseCore1L :
    def modelCore(self,trainer, layer):
        N = self.N
        l = layer
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        do = lambda x : None if self.batchNorm else x
        l=denseBatchNormF( l, int(N),activation='mish' ,dropout=do(0.15),**l_args )
        self.modelTags += ["1L"]+self.batchNormTag()
        self.dense1 = l
        return l

class DenseCoreAtt :
    def modelCore(self,trainer, layer):
        N = self.N
        l = layer
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        do = lambda x : None if self.batchNorm else x
        l=denseBatchNormF( l, int(N),activation='mish' ,dropout=do(0.15),**l_args )
        l=denseBatchNormF( l, int(N),activation='mish' ,dropout=do(0.15),**l_args )
        
        self.modelTags += ["1L"]+self.batchNormTag()
        self.dense1 = l
        return l

class DenseCore2L :
    def modelCore(self,trainer, layer):
        N = self.N
        l = layer
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        l=denseBatchNormF( l, int(N),activation='mish' ,dropout=0.15,**l_args )
        l=denseBatchNormF( l, int(0.6*N),activation='mish' ,dropout=0.15, **l_args)
        self.modelTags += ["T0"]
        return l

    
class DenseCoreT0 :
    def modelCore(self,trainer, layer):
        N = self.N
        l = layer
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        l=denseBatchNormF( l, int(N),activation='mish' ,dropout=0.15,**l_args )
        l=denseBatchNormF( l, int(0.6*N),activation='mish' ,dropout=0.15, **l_args)
        l=denseBatchNormF( l, int(0.4*N),activation='mish' ,dropout=0.15, **l_args)
        self.modelTags += ["T0"]
        return l

    

class DenseCoreT1 :
    def modelCore(self,trainer, layer):
        N = self.N
        l = layer
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        l=denseBatchNormF( l, int(0.3*N),activation='mish' ,dropout=0.15,**l_args )
        l=denseBatchNormF( l, int(0.8*N),activation='mish' ,dropout=0.2, **l_args)
        l=denseBatchNormF( l, int(1.3*N),activation='mish' ,dropout=0.2, **l_args)
        self.modelTags += ["T1"]
        return l


class DenseCoreT2:
    def modelCore(self,trainer, layer):
        N = self.N
        l = layer
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        l=denseBatchNormF( l, int(1.5*N),activation='mish' ,dropout=0.15,**l_args )
        l=denseBatchNormF( l, int(1.5*N),activation='mish' ,dropout=0.2, **l_args)
        l=denseBatchNormF( l, int(1.5*N),activation='mish' ,dropout=0.2, **l_args)
        self.modelTags += ["T2"]
        return l


class DenseCoreT3:
    def modelCore(self,trainer, layer):
        N = self.N
        l = layer
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        l=denseBatchNormF( l, int(0.3*N),activation='mish' ,dropout=0.15,**l_args )
        l=denseBatchNormF( l, int(0.3*N),activation='mish' ,dropout=0.2, **l_args)
        l=denseBatchNormF( l, int(0.3*N),activation='mish' ,dropout=0.2, **l_args)
        self.modelTags += ["T3"]
        return l




    
class Identity:
    def modelHead(self, trainer, inputs):
        self.modelTags += ['NoAnnot']
        return inputs
    

def toCenters( l ):
    n = len(l)
    print( n-1 )
    for i in range(n-1):
        a,b=l[i:i+2]
        print ( (0.5*(a+b), (b-a)*0.5 ) , ",")  
    
etaCoverages = dict(
    smallR4 = [ (0, 0.2), (0.3, 0.2), (0.6, 0.2), # 0 1 2
                (0.92, 0.22), (1.15, 0.15), (1.3, 0.15), # 3 4 5
                (1.4, 0.1),  (2.0, 0.4),  #  6 7 
                (2.5, 0.1), (2.6, 0.3), # 8 9
                (3.2, 0.4), (3.4, 0.4), (4., 0.6) ],

    # obtained from :
    #  toCenters( [0, 0.15, 0.62, 0.91, 1.15, 1.44, 1.8, 2.4, 2.72, 2.31 , 3.65, 7] )
    gateSR = [ (0.075, 0.075) ,(0.385, 0.235) , (0.765, 0.145) ,# 0 1 2 
               (1.03, 0.12) , (1.295, 0.145) , (1.62, 0.18) ,   # 3 4 5 
               (2.1, 0.3) , (2.56, 0.16) ,                      # 6 7   
               (2.965, 0.245) ,(3.43, 0.22) ,(5.325, 1.675) ,   # 8 9 10
               ],
    # toCenters( [0, 0.15, 0.44, 0.73, 1.06, 1.31, 1.6, 2.1, 2.5, 3.1 , 3.45, 3.8, 6]   ),
    gateSR2 = [ (0.075,0.075) ,(0.295,0.145) ,(0.585,0.145) ,  # 0 1 2 
                (0.895,0.165) , (1.185,0.125) ,(1.455,0.145) , # 3 4 5 
                (1.850,0.250) , (2.300,0.200) ,(2.800,0.300) , # 6 7 8  
                (3.275,0.175) ,(3.625,0.175) ,(4.900,1.100) ,], # 9 10 11


    
    smallR3 = [ (0, 0.2), (0.3, 0.2), (0.6, 0.2),
                                  (0.92, 0.22), (1.15, 0.15), (1.3, 0.15),
                                  (1.4, 0.1),  (2.0, 0.4),
                                  (2.5, 0.1), (2.6, 0.2), (3.25, 0.3), (3.7, 0.3), (4.1, 0.3) ],

    full = [(0, 0.2), (0.3, 0.2), (0.6, 0.2), (0.92, 0.22), (1.15, 0.15), (1.3, 0.15), (1.4, 0.15), (1.6, 0.3), (2.0, 0.5), (2.8, 1.2), (3.6, 1.5)],
    
)
# older for ref : 
# elif coverage=='cracks':
#     l_eta   = [0,    1.0,   1.4, ]
#     l_eta_w = [0.05, 0.2,  0.25]
# elif coverage=='cracks2':
#     l_eta   = [0,    0.92, 1.15, 1.3,]
#     l_eta_w = [0.05, 0.22, 0.15, 0.15,]
# elif coverage=='smallR':
#     l_eta =   [0, 0.3, 0.6, 0.92, 1.15, 1.3, 1.4, 1.6,  2. , 2.8, 3.6 , 4] 
#     l_eta_w = [0.2, 0.2, 0.2, 0.22, 0.15, 0.15, 0.15, 0.3, 0.5, 1., 1., 2. ]
# elif coverage=='smallR2':
#     l_eta, l_eta_w = zip( (0, 0.2), (0.3, 0.2), (0.6, 0.2),
#                           (0.92, 0.22), (1.15, 0.15), (1.3, 0.15),
#                           (1.4, 0.15), (1.6, 0.2), (2.0, 0.2),
#                           (2.4, 0.2), (2.6, 0.2), (3.2, 0.4), (3.6, 0.2), (4.2, 0.4) )



def eTrapAnnotations(e_ind, inputs):
    #e_ind = trainer.config.features.index('e_var')
    e_mins = [ -10, -0.8, -0.6 ]
    e_maxs = [ -0.7, -0.5,  10 ]
    e_deltas = [0.1 , 0.05, 0.1]
    l_e=utils.TrapezoidAnnotation(e_ind, np.array([e_mins, e_maxs, e_deltas]),name='E_TrapzAnnot' )(inputs)
    return l_e 


class EtaAnnotBase:
    etaAnnotClass = None
    eta_scale = 4.02 # IMPORTANT should be set to inscaler.full_scale_factor(), this is done in sample-specific setup files such as setupAntiKt4EMPFlowJets.py

    coverage = 'full'

    etavar= 'eta' # name of the variable. could be eta_det or rapidity 

    offset= -0.5

    def etaAnnotations(self, coverage, dosymetrize=False):
        """returns the centers and squared widths of eta regions according to coverage (see the etaCoverages dict for the definitions of regions)
        centers value and width are properly scaled according to self.eta_scale . 
        """
        l_eta, l_eta_w = zip( * etaCoverages[coverage ] ) 

        if dosymetrize:
            l_eta = symetrize( list(l_eta) )
            l_eta_w = symetrize(list(l_eta_w), 1)

        eta_centers = np.array( l_eta  )  / self.eta_scale
        eta_w = np.array( l_eta_w )/ self.eta_scale


        print(self.eta_scale)
        return  eta_centers, eta_w
    
    
    
class _EtaG_Annot(EtaAnnotBase):

    def buildEtaAnnotLayer(self, trainer, inputs):

        s= '' if self.symetrizeCenters else 'S'
        c = 'c' if self.coverage=='cracks2' else ''
        self.modelTags += [ f'EtaG{s}{c}']

        eta_centers, eta_w = self.etaAnnotations( self.coverage, self.symetrizeCenters)
        if self.etaAnnotClass.__name__.startswith('Gaus'):
            eta_w *=eta_w # the custom layers actually expects the sqared widths 

        eta_ind = trainer.config.features.index(self.etavar)
        # build the layer by calling etaAnnotClass, which is GausAnnotationSym or GausAnnotation, see below 
        l_eta = self.etaAnnotClass(eta_ind, eta_centers, eta_w,name='Eta_GausAnnot', includeInput=False, offset=self.offset )(inputs)

        l= layers.concatenate( [l_eta, inputs ] )
        
        return l
    
class _EtaBlockG_Annot(EtaAnnotBase):
    def buildEtaAnnotLayer(self, trainer, inputs):
        N = self.N

        s= '' if self.symetrizeCenters else 'S'
        c = 'c' if self.coverage=='cracks2'  else ''
        self.modelTags += [ f'EtaG{s}Block{c}']
        
        eta_centers, eta_w = self.etaAnnotations(  self.coverage, self.symetrizeCenters)
        if self.etaAnnotClass.__name__.startswith('Gaus'):
            eta_w *=eta_w # the custom layers actually expects the sqared widths 
        eta_ind = trainer.config.features.index(self.etavar)        
        # build the layer by callingg etaAnnotClass, which is GausAnnotationSym or GausAnnotation, see below 
        l_eta = self.etaAnnotClass(eta_ind, eta_centers, eta_w,name='Eta_GausAnnot', includeInput=True, offset=self.offset )(inputs)

        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        neta = max(20, min(int(N/15),40))
        l_eta=denseBatchNormF( l_eta, neta ,activation='mish' , **l_args )                

        # concatenate the eta inputs after dense block, with the rest of inputs.
        # ExcludeIndices -> do not concatenate eta again, since it is part of the block
        l= layers.concatenate( [l_eta, utils.ExcludeIndices([eta_ind])(inputs) ] )
        return l

class EtaBlockGSAnnot(_EtaBlockG_Annot):
    etaAnnotClass = utils.GausAnnotationSym
    symetrizeCenters = False # No need to symetrize the eta centers, since we're using abs(eta) in this case
    modelHead = _EtaBlockG_Annot.buildEtaAnnotLayer
    
class EtaBlockGAnnot(_EtaBlockG_Annot):
    etaAnnotClass = utils.GausAnnotation
    symetrizeCenters = True
    modelHead = _EtaBlockG_Annot.buildEtaAnnotLayer

class EtaGSAnnot(_EtaG_Annot):
    etaAnnotClass = utils.GausAnnotationSym
    symetrizeCenters = False # No need to symetrize the eta centers, since we're using abs(eta) in this case
    modelHead = _EtaG_Annot.buildEtaAnnotLayer

class EtaGAnnot(_EtaG_Annot):
    etaAnnotClass = utils.GausAnnotation
    symetrizeCenters = True
    modelHead = _EtaG_Annot.buildEtaAnnotLayer



class EtaSplitCore(_EtaG_Annot):
    etaAnnotClass = utils.GausAnnotationSym
    symetrizeCenters = False
    offset = 0
    annotTag=''
    def modelCore(self,trainer, layer):
        self.modelTags +=['SplitC'+self.annotTag]

        l_eta = self.buildEtaAnnotLayer(trainer, self.inputs,)
        n_eta = l_eta.shape[1]


        lsize = int(self.N*0.8)
        
        allEtaL = []
        for i in range(n_eta):
            l= self.repeatedPart(layer, i) 
            l_i = utils.DuplicateEntry(i, l.shape[1])(l_eta)
            l = layers.multiply( [l,l_i] )
            allEtaL.append( l )
        return layers.average( allEtaL )


    def repeatedPart(self, inlayer, ieta):
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        return denseBatchNormF( inlayer, int(self.N*0.8) , activation='mish', name=f'dense_etaR{ieta}',**l_args)
    
class EtaSplitgaCore(EtaSplitCore):
    etaAnnotClass = utils.GateAnnotationSym
    annotTag = 'ga'
    coverage = 'gateSR2'
    
class EtaSplitCoreAE(EtaSplitCore):
    def modelCore(self,trainer, layer):
        l = super().modelCore(trainer, layer)
        self.modelTags +=['AE']
        
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        l=denseBatchNormF( l, int(self.N*0.6) , activation='mish', **l_args)
        l=denseBatchNormF( l, int(self.N*0.4) , activation='mish', **l_args)
        l=denseBatchNormF( l, int(self.N*0.2) , activation='mish', **l_args)
        return l

class EtaLargeSplitCore(EtaSplitCore):
    def modelCore(self,trainer, layer):
        l = super().modelCore(trainer, layer)
        self.modelTags +=['LS']    
        return l

    def repeatedPart(self, inlayer, ieta):
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N=self.N
        l= denseBatchNormF( inlayer, int(N*0.5) , activation='mish', name=f'dense1_etaR{ieta}',**l_args)
        l= denseBatchNormF( l, int(N*0.3) , activation='mish', name=f'dense2_etaR{ieta}',**l_args)
        return l
# ----------------------------------
# Classes for model tails

class TailLayer:
    mFromE = False

    def multEandMOuputs(self, l1, l2):
        if self.scale_output:
            print("ERROR can't multiply E and m output if output is scaled")
            raise
        l1_bis =  layers.Lambda(lambda x: tf.stop_gradient(x), name='Lambda')(l1)
        l2 = layers.multiply( [l1_bis, l2  ], name='outputM')                
        return l2


def etaTailCorrE(cls):
    class TailClass(cls):
        r_e_scale = ( 0.69256588 , 0.8630715) # outscaler.full_scale_factor() and outscaler.full_offset()
        
        def modelTail( self, trainer,l):
            lastLayers = cls.modelTail(self, trainer, l)
            self.modelTags +=['etaTC']
            ntargets = len(trainer.config.originalTargets)
            
            eta_ind = trainer.config.features.index(cls.etavar)

            l  = lastLayers[0]
            # create scale factors as function of eta ( increasing with abs(eta) )
            etaF = utils.EtaLinC(ntargets, eta_ind, 3.05, 0.83, EtaAnnotBase.eta_scale )(self.inputs)
            # apply this scale factor
            l = utils.ScaleOutput( *self.r_e_scale, name='outputEetaC' ) ( [ l,etaF  ] ) 
            lastLayers[0] = l

            return lastLayers
    return TailClass

def addJesF(cls):
    class TailClass(cls):
        def modelTail( self, trainer,l):
            lastLayers = cls.modelTail(self, trainer, l)
            self.modelTags +=['aJES']
            ntargets = len(trainer.config.originalTargets)
            
            jes_ind = trainer.config.features.index('jesR')

            l  = lastLayers[0]
            l = utils.AddJES(name='outputEaddJES')([l,utils.DuplicateEntry(jes_ind , 1 )(self.inputs) ])
            lastLayers[0] = l
            return lastLayers
    return TailClass


def addPtFactor(cls):
    class TailClass(cls):
        def modelTail( self, trainer,l):
            lastLayers = cls.modelTail(self, trainer, l)
            self.modelTags +=['PtF']
            
            pt_ind = trainer.config.features.index('pt_reco')

            l0  = lastLayers[0]
            l = denseBatchNormF( utils.DuplicateEntry(pt_ind , 1 )(self.inputs), l0.shape[1], activation = 'tanhSF' , name='dense_pt')
            
            lastLayers[0] = layers.multiply( [l0,l] , name='outputE_ptF')
            return lastLayers

        @staticmethod
        def setptFweights( trainer, a, b):
            # solve :
            # 2-tanhSF( a*scale*pt0 +a*o +b ) = f0
            # 2-tanhSF( a*scale*pt1 +a*o +b ) = f1

            # good values for f0=1.05 at pt=20 and f1~1 at 90 --> a=16.5, b=16.2 (with sf,o = (0.00123, -0.903) )

            l = trainer.net.get_layer('dense_pt')
            l.set_weights( [np.array([[a]]),np.array([b])] )
            
    return TailClass


class SimpleTail(TailLayer):
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        if len(trainer.config.originalTargets)==2:
            #l1=denseBatchNormF( l, N*0.3, activation='mish', **lastregs)
            l1=denseBatchNormF( l, self.nPredictedValues(trainer), activation=self.last_activation,  activ_name='outputE',**lastregs)
            l2=denseBatchNormF( l, self.nPredictedValues(trainer), activation=self.last_activation,  activ_name='outputM',**lastregs)
            l = [l1,l2]
        else:
            l=denseBatchNormF( l, self.nPredictedValues(trainer), activation=self.last_activation, activ_name='outputE',**lastregs)

        self.modelTags += ["TS"]
        return l

class Layer1Tail(TailLayer):
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N
        if len(trainer.config.originalTargets)==2:
            l1=denseBatchNormF( l, N*0.3, activation='mish', **l_args)
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputE', **lastregs)
            l2=denseBatchNormF( l, N*0.3, activation='mish', **l_args)
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputM', **lastregs)
            l = [l1,l2]
        else:
            l1=denseBatchNormF( l, N*0.3, activation='mish', **l_args)
            l=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation, activ_name='outputE',**lastregs)
        self.modelTags += ["T1"]
        return l


class ReInputTail(TailLayer):
        
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)

        n = self.inputs.shape[1] 
        l = layers.concatenate( [  l, utils.ExtractN( (n-1,1) )(self.inputs) ] ) # add the inputs except eta (assuming it is a pos 0)
        
        N = self.N
        if len(trainer.config.originalTargets)==2:
            l1=denseBatchNormF( l, int(N*0.5), activation='mish', **l_args)
            l1=denseBatchNormF( l1, int(N*0.2), activation='mish', **l_args)
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputE', **lastregs)
            l2=denseBatchNormF( l, int(N*0.5), activation='mish', **l_args)
            l2=denseBatchNormF( l2, int(N*0.2), activation='mish', **l_args)
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputM', **lastregs)
            l = [l1,l2]
        else:
            l1=denseBatchNormF( l, int(N*0.5), activation='mish', **l_args)
            l1=denseBatchNormF( l1, int(N*0.2), activation='mish', **l_args)
            l=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation, activ_name='outputE',**lastregs)
        self.modelTags += ["ReIT"]
        return l
    

class DeepTailAE(TailLayer):
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N
        if len(trainer.config.originalTargets)==2:
            l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)            
            l1=denseBatchNormF( l1, N*0.4, activation='mish', **l_args)
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,  activ_name='outputE',**lastregs)

            l2=denseBatchNormF( l, N*0.6, activation='mish', **l_args)            
            l2=denseBatchNormF( l2, N*0.4, activation='mish', **l_args)
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,  activ_name='outputM',**lastregs)
            
            l = [l1,l2]
        else:
            l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)            
            l1=denseBatchNormF( l1, N*0.4, activation='mish', **l_args)
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,  activ_name='outputE',**lastregs)
            l = l1
            self.modelTags += ["1"]

        self.modelTags += ["TAE"]
        return l

@addPtFactor
class DeepTailAEptSF(DeepTailAE):
    pass
    
class TailAESplitE(TailLayer):
    ecenter=28
    ewidth=80 # GeV
    scaleparams = (1,0) # scaling parameters as obtain from trainer.inscaler (can not be found automatically yet)
    ename = 'e_reco_nolog'
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N
        
        e_ind = trainer.config.features.index(self.ename)
        a,b = self.scaleparams
        ecenter = self.ecenter*a +b
        #ewidth = (self.ewidth*a)**2 # need sigma squared if GausAnnotationSym
        ewidth = (self.ewidth*a) #
        # build the layer by callingg etaAnnotClass, which is GausAnnotationSym or GausAnnotation, see below 
        l_gausse = utils.GateAnnotationSym(e_ind, [ecenter] , [ewidth],name='E_GausAnnot', includeInput=False, offset=0 )(self.inputs)
        l_igausse = utils.OneMinus()(l_gausse)
        
        nout = int(self.nPredictedValues(trainer))
        if self.nPredictedValues(trainer)>1:
            l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)            
            l1=denseBatchNormF( l1, N*0.4, activation='mish', **l_args)            
            l1_low=denseBatchNormF( l1, nout, activation=self.last_activation,  activ_name='outputE_lowE',**lastregs)                        
            l1_low = layers.multiply([utils.DuplicateEntry(0, nout)(l_gausse),l1_low])
            #l1_low = layers.Lambda( lambda x:0.5*x)(l1_low)
            l1_hi=denseBatchNormF( l1, nout, activation=self.last_activation,  activ_name='outputE_higE',**lastregs)                        
            l1_hi = layers.multiply([utils.DuplicateEntry(0, nout)(l_igausse),l1_hi])
            l1 = layers.add( [l1_hi, l1_low], name='outputE')
            
            
            l2=denseBatchNormF( l, N*0.6, activation='mish', **l_args)            
            l2=denseBatchNormF( l2, N*0.4, activation='mish', **l_args)
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,  activ_name='outputM',**lastregs)
            
            l = [l1,l2]
        else:
            raise Exception('TailAESplitE not available for 1 output')
        

        
        self.modelTags += ["TSp"+self.ename[0].upper()]
        return l
    

class LayerDeepTail(TailLayer):
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N

        if len(trainer.config.originalTargets)==2:
            l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.4, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.2, activation='mish', **l_args)
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputE', **lastregs)

            l2=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.4, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.2, activation='mish', **l_args)
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,activ_name=None if self.mFromE else 'outputM', **lastregs)
            if self.mFromE:
                l2 = self.multEandMOuputs(l1,l2)
            l = [l1,l2]
        else:
            raise Exception("LayerDeepTail requires 2 outputs")
        self.modelTags += ["DT"]
        if self.mFromE:self.modelTags += ["m"]
        self.modelTags +=  self.batchNormTag()
        return l


class LayerEtaAnDeepTail(TailLayer, _EtaG_Annot):

    # paraneters for _EtaG_Annot
    etaAnnotClass = utils.GausAnnotation
    symetrizeCenters = True 
    
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N

        etaAnot = self.buildEtaAnnotLayer(trainer, self.inputs)
        l = layers.concatenate( [l, etaAnot] )
        
        if len(trainer.config.originalTargets)==2:
            l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.4, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.2, activation='mish', **l_args)
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputE', **lastregs)

            l2=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.4, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.2, activation='mish', **l_args)
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,activ_name=None if self.mFromE else 'outputM', **lastregs)
            if self.mFromE:
                l2 = self.multEandMOuputs(l1,l2)
            l = [l1,l2]
        else:
            raise Exception("LayerDeepTail requires 2 outputs")
        self.modelTags += ["DT"]
        if self.mFromE:self.modelTags += ["m"]
        self.modelTags +=  self.batchNormTag()
        return l
    
@etaTailCorrE
class LayerDeepTailEtaC(LayerDeepTail):
    etavar = 'eta'
    pass



class LayerDeepTailA(TailLayer):
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N

        if len(trainer.config.originalTargets)==2:
            l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.5, activation='mish', **l_args)
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputE', **lastregs)

            l2=denseBatchNormF( l, N*0.7, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.6, activation='mish', **l_args)
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,activ_name=None if self.mFromE else 'outputM', **lastregs)
            if self.mFromE:
                l2 = self.multEandMOuputs(l1,l2)
            l = [l1,l2]
        else:
            raise Exception("LayerDeepTail requires 2 outputs")
        self.modelTags += ["DA"]
        if self.mFromE:self.modelTags += ["m"]
        self.modelTags +=  self.batchNormTag()
        return l
    
class LayerDeepTailD(TailLayer):
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N

        if len(trainer.config.originalTargets)==2:
            l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.2, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.4, activation='mish', **l_args)
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputE', **lastregs)

            l2=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.2, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.4, activation='mish', **l_args)
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,activ_name=None if self.mFromE else 'outputM', **lastregs)
            if self.mFromE:
                l2 = self.multEandMOuputs(l1,l2)
            l = [l1,l2]
        else:
            raise Exception("LayerDeepTail requires 2 outputs")
        self.modelTags += ["DTD"]
        if self.mFromE:self.modelTags += ["m"]
        self.modelTags +=  self.batchNormTag()
        return l



class SplitEtaTailT0(TailLayer,_EtaG_Annot):
    etaAnnotClass = utils.GausAnnotationSym
    symetrizeCenters = False
    offset = 0
    fullSplit=False

    concatEETa = True
    def modelTail(self, trainer, lcore):
        if len(trainer.config.originalTargets)!=2:
            raise Exception("LayerDeepTail requires 2 outputs")

        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N

        self.modelTags +=['SplitT0' ]

        l_eta = self.buildEtaAnnotLayer(trainer, self.inputs)
        n_eta = l_eta.shape[1]

        if self.concatEETa:
            lcore = layers.concatenate( [  lcore, utils.ExtractN( (2,0) )(self.inputs) ] ) # add the 2 1st inputs (assuming its eta and e_reco)
        else:
            self.modelTags+=['b']

        outN = int(self.nPredictedValues(trainer))
        lsize =  int(self.N*0.2) 

        etaMultLayers = [ utils.DuplicateEntry(i, outN)(l_eta) for i in range(n_eta) ]
        
        
        allEtaL = []
        for i in range(n_eta):
            l=denseBatchNormF( lcore, int(self.N*0.2) , activation='mish', name=f'dense_splitE_1_{i}', **l_args)
            l=denseBatchNormF( l, int(self.N*0.1) , activation='mish', name=f'dense_splitE_2_{i}', **l_args)
            l = denseBatchNormF( l, outN , activation=self.last_activation,activ_name=f'outputE{i}', **lastregs)
            l = layers.multiply( [l,etaMultLayers[i]] ,name=f'multiplyE_{i}' )                
            allEtaL.append( l )

        l1=layers.average( allEtaL, name='outputEAvg' )

        allEtaL = []
        for i in range(n_eta):
            l=denseBatchNormF( lcore, int(self.N*0.2) , activation='mish', name=f'dense_splitM_1_{i}', **l_args)
            l=denseBatchNormF( l, int(self.N*0.1) , activation='mish', name=f'dense_splitM_2_{i}', **l_args)
            l = denseBatchNormF( l, outN , activation=self.last_activation,activ_name=f'outputM{i}', **lastregs)
            l = layers.multiply( [l,etaMultLayers[i]],name=f'multiplyM_{i}' )                
            allEtaL.append( l )

        l2=layers.average( allEtaL, name='outputMAvg' )
        return [l1,l2]
        
class LayerSplitEtaTail(TailLayer,_EtaG_Annot):
    etaAnnotClass = utils.GausAnnotationSym
    symetrizeCenters = False
    offset = 0
    fullSplit=False
    
    def modelTail(self, trainer, lcore):
        if len(trainer.config.originalTargets)!=2:
            raise Exception("LayerDeepTail requires 2 outputs")

        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N

        self.modelTags +=['SplitTF' if self.fullSplit else 'SplitT']

        l_eta = self.buildEtaAnnotLayer(trainer, self.inputs)
        n_eta = l_eta.shape[1]

        outN = int(self.nPredictedValues(trainer))
        lsize = outN if self.fullSplit else int(self.N*0.2) 



        l1=denseBatchNormF( lcore, N*0.6, activation='mish', **l_args)
        l1=denseBatchNormF( l1, N*0.4, activation='mish', **l_args)
        allEtaL = []
        for i in range(n_eta):
            l=denseBatchNormF( l1, int(self.N*0.2) , activation='mish', name=f'dense_splitE{i}', **l_args)
            if self.fullSplit:
                l = denseBatchNormF( l, outN , activation=self.last_activation,activ_name=f'outputE{i}', **lastregs)
            l_i = utils.DuplicateEntry(i, lsize)(l_eta)
            l = layers.multiply( [l,l_i] )                
            allEtaL.append( l )

        l1=layers.average( allEtaL, name='outputEAvg' )
        if not self.fullSplit:
            l1=denseBatchNormF( l1, outN, activation=self.last_activation,activ_name='outputE', **lastregs)

        l2=denseBatchNormF( lcore, N*0.6, activation='mish', **l_args)
        l2=denseBatchNormF( l2, N*0.4, activation='mish', **l_args)
        allEtaL = []
        for i in range(n_eta):
            l=denseBatchNormF( l2, int(self.N*0.2) , activation='mish', name=f'dense_splitM{i}',**l_args)
            if self.fullSplit:
                l = denseBatchNormF( l, outN, activation=self.last_activation,activ_name=f'outputM{i}', **lastregs)
            l_i = utils.DuplicateEntry(i, lsize)(l_eta)
            l = layers.multiply( [l,l_i] )
            allEtaL.append( l )
        l2=layers.average( allEtaL, name='outputMAvg' )
        if not self.fullSplit:
            l2=denseBatchNormF( l2, outN, activation=self.last_activation,activ_name=None if self.mFromE else 'outputM', **lastregs)

        l = [l1,l2]
        
        
        return l

@addJesF
class LayerSplitEtaTailJES(LayerSplitEtaTail):
    pass
    

def absoluteCol( x ):
    xa = tf.nn.relu(x[:,1])
    return tf.stack( [x[:,0],xa], 1)

class AttentionTail(TailLayer):
    """requires the 1st layer is saved in  self.dense1 """
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N
        inputsN = self.inputs.shape[1]
        outputsN = self.nPredictedValues(trainer)

        
        
        def attentionFinal( l, lw , tag ):
            # mix the attention weights with the last layer (we reshape to (n,2) because we predict 2 numbers)
            ll = layers.Reshape( (inputsN,2) ) (layers.multiply( [l, lw]))
            ll = layers.Activation('softmax', name='attention'+tag)(ll)
            # combine with the inputs to form the output
            ll = layers.dot([self.inputs, ll], axes=1 )
            ll = layers.Lambda( absoluteCol,name='output'+tag )(ll)
            return ll


        l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
        l1=denseBatchNormF( l1, N*0.3, activation='mish', **l_args)
        attW1 = denseBatchNormF(self.inputs, l1.shape[1], activation='softmax', **l_args)
        l1 = layers.multiply( [l1,attW1] )
        l1=denseBatchNormF( l1, outputsN, activation=self.last_activation, activ_name='outputE',**lastregs)
        #l1 = attentionFinal(l1,attW1,'E')
        
      
        l2=denseBatchNormF( l, N*0.7, activation='mish', **l_args)
        attW2 = denseBatchNormF(self.inputs, l2.shape[1], activation='softmax', **l_args)
        l2 = layers.multiply( [l2,attW2] )
        l2=denseBatchNormF( l2, outputsN, activation=self.last_activation, activ_name='outputM', **lastregs)

        self.modelTags += ["AtT"]
        self.modelTags +=  self.batchNormTag()
        return [l1,l2]
        

class AttentionMDeepTail1(TailLayer):
    """requires the 1st layer is saved in  self.dense1 """
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N
        inputsN = self.inputs.shape[1]
        outputsN = self.nPredictedValues(trainer)

        if len(trainer.config.originalTargets)==2:
            l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.4, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.2, activation='mish', **l_args)
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputE', **lastregs)

            attl = denseBatchNormF(self.inputs, N*0.5, activation='mish', **l_args)
            attl = denseBatchNormF(attl, N, activation='mish', **l_args)

            l2=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.4, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.2, activation='mish', **l_args)
            attl = denseBatchNormF(attl, l2.shape[1], activation='softmax', **l_args)
            l2 = layers.multiply( [l2,attl] )
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,activ_name=None if self.mFromE else 'outputM', **lastregs)
            if self.mFromE:
                l2 = self.multEandMOuputs(l1,l2)
            l = [l1,l2]
        else:
            raise Exception("LayerDeepTail requires 2 outputs")
        self.modelTags += ["AtMDT1"]
        if self.mFromE:self.modelTags += ["m"]
        self.modelTags +=  self.batchNormTag()
        return l

class AttentionMDeepTail2(TailLayer):
    """requires the 1st layer is saved in  self.dense1 """
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N
        inputsN = self.inputs.shape[1]
        outputsN = self.nPredictedValues(trainer)

        if len(trainer.config.originalTargets)==2:
            l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.4, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.2, activation='mish', **l_args)
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputE', **lastregs)

            attl = denseBatchNormF(self.inputs, N, activation='mish', **l_args)
            attl = denseBatchNormF(attl, N*0.5, activation='mish', **l_args)

            l2=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.4, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.2, activation='mish', **l_args)
            attl = denseBatchNormF(attl, l2.shape[1], activation='softmax', **l_args)
            l2 = layers.multiply( [l2,attl] )
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,activ_name=None if self.mFromE else 'outputM', **lastregs)
            if self.mFromE:
                l2 = self.multEandMOuputs(l1,l2)
            l = [l1,l2]
        else:
            raise Exception("LayerDeepTail requires 2 outputs")
        self.modelTags += ["AtMDT2"]
        if self.mFromE:self.modelTags += ["m"]
        self.modelTags +=  self.batchNormTag()
        return l


class AttentionMDeepTail3(TailLayer):
    """requires the 1st layer is saved in  self.dense1 """
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N
        inputsN = self.inputs.shape[1]
        outputsN = self.nPredictedValues(trainer)

        if len(trainer.config.originalTargets)==2:
            l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.4, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.2, activation='mish', **l_args)
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputE', **lastregs)

            attl = denseBatchNormF(self.inputs, N*0.3, activation='mish', **l_args)
            attl = denseBatchNormF(attl, N*0.6, activation='mish', **l_args)
            attl = denseBatchNormF(attl, N*0.6, activation='mish', **l_args)

            l2=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.4, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.2, activation='mish', **l_args)
            attl = denseBatchNormF(attl, l2.shape[1], activation='softmax', **l_args)
            l2 = layers.multiply( [l2,attl] )
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,activ_name=None if self.mFromE else 'outputM', **lastregs)
            if self.mFromE:
                l2 = self.multEandMOuputs(l1,l2)
            l = [l1,l2]
        else:
            raise Exception("LayerDeepTail requires 2 outputs")
        self.modelTags += ["AtMDT3"]
        if self.mFromE:self.modelTags += ["m"]
        self.modelTags +=  self.batchNormTag()
        return l


class AttentionMDeepTail4(TailLayer):
    """requires the 1st layer is saved in  self.dense1 """
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N
        inputsN = self.inputs.shape[1]
        outputsN = self.nPredictedValues(trainer)

        if len(trainer.config.originalTargets)==2:
            l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.4, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.2, activation='mish', **l_args)
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputE', **lastregs)

            attl = denseBatchNormF(self.inputs, N, activation='mish', **l_args)

            l2=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.4, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.2, activation='mish', **l_args)
            attl = denseBatchNormF(attl, l2.shape[1], activation='softmax', **l_args)
            l2 = layers.multiply( [l2,attl] )
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,activ_name=None if self.mFromE else 'outputM', **lastregs)
            if self.mFromE:
                l2 = self.multEandMOuputs(l1,l2)
            l = [l1,l2]
        else:
            raise Exception("LayerDeepTail requires 2 outputs")
        self.modelTags += ["AtMDT4"]
        if self.mFromE:self.modelTags += ["m"]
        self.modelTags +=  self.batchNormTag()
        return l


class AttentionMDeepTail5(TailLayer):
    """requires the 1st layer is saved in  self.dense1 """
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N
        inputsN = self.inputs.shape[1]
        outputsN = self.nPredictedValues(trainer)

        if len(trainer.config.originalTargets)==2:
            l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.4, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.2, activation='mish', **l_args)
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputE', **lastregs)

            attl = denseBatchNormF(self.inputs, N*0.5, activation='mish', **l_args)

            l2=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.4, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.2, activation='mish', **l_args)
            attl = denseBatchNormF(attl, l2.shape[1], activation='softmax', **l_args)
            l2 = layers.multiply( [l2,attl] )
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,activ_name=None if self.mFromE else 'outputM', **lastregs)
            if self.mFromE:
                l2 = self.multEandMOuputs(l1,l2)
            l = [l1,l2]
        else:
            raise Exception("LayerDeepTail requires 2 outputs")
        self.modelTags += ["AtMDT5"]
        if self.mFromE:self.modelTags += ["m"]
        self.modelTags +=  self.batchNormTag()
        return l


class AttentionMDeepTail6(TailLayer):
    """requires the 1st layer is saved in  self.dense1 """
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N
        inputsN = self.inputs.shape[1]
        outputsN = self.nPredictedValues(trainer)

        if len(trainer.config.originalTargets)==2:
            l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.4, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.2, activation='mish', **l_args)
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputE', **lastregs)

            attl = denseBatchNormF(self.inputs, N*1.5, activation='mish', **l_args)

            l2=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.4, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.2, activation='mish', **l_args)
            attl = denseBatchNormF(attl, l2.shape[1], activation='softmax', **l_args)
            l2 = layers.multiply( [l2,attl] )
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,activ_name=None if self.mFromE else 'outputM', **lastregs)
            if self.mFromE:
                l2 = self.multEandMOuputs(l1,l2)
            l = [l1,l2]
        else:
            raise Exception("LayerDeepTail requires 2 outputs")
        self.modelTags += ["AtMDT6"]
        if self.mFromE:self.modelTags += ["m"]
        self.modelTags +=  self.batchNormTag()
        return l


class AttentionEMDeepTail6(TailLayer):
    """requires the 1st layer is saved in  self.dense1 """
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N
        inputsN = self.inputs.shape[1]
        outputsN = self.nPredictedValues(trainer)

        if len(trainer.config.originalTargets)==2:
            attlE = denseBatchNormF(self.inputs, N*1.5, activation='mish', **l_args)

            l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.4, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.2, activation='mish', **l_args)
            attlE = denseBatchNormF(attlE, l1.shape[1], activation='softmax', **l_args)
            l1 = layers.multiply( [l1,attlE] )
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputE', **lastregs)

            attlM = denseBatchNormF(self.inputs, N*1.5, activation='mish', **l_args)

            l2=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.4, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.2, activation='mish', **l_args)
            attlM = denseBatchNormF(attlM, l2.shape[1], activation='softmax', **l_args)
            l2 = layers.multiply( [l2,attlM] )
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,activ_name=None if self.mFromE else 'outputM', **lastregs)
            if self.mFromE:
                l2 = self.multEandMOuputs(l1,l2)
            l = [l1,l2]
        else:
            raise Exception("LayerDeepTail requires 2 outputs")
        self.modelTags += ["AtEMDT6"]
        if self.mFromE:self.modelTags += ["m"]
        self.modelTags +=  self.batchNormTag()
        return l



##############
#
class WZweightTail(TailLayer):
    def modelTail(self, trainer, l):
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N

        if len(trainer.config.originalTargets)==2:

            l1=denseBatchNormF( l, 1, activation='softmax',activ_name='weightE', **lastregs)

            l2=denseBatchNormF( l, 1, activation='softmax',activ_name='weightM', **lastregs)
    
            l = [l1,l2]
        else:
            raise Exception("WZweightTail requires 2 outputs")
        self.modelTags += ["WT"]
        if self.mFromE:self.modelTags += ["m"]
        self.modelTags +=  self.batchNormTag()
        return l

def sub_network(trainer, input_layer, ):

    #####
    hack = HeadEtaBlockCoreT3AttMDeepT6(N=700)
    hack(trainer)
    #####
    #l = DenseCoreT3.modelCore(trainer, input_layer)
    N=700
    l=denseBatchNormF( input_layer.output, int(0.3*N),activation='mish' ,dropout=0.15, )
    l=denseBatchNormF( l, int(0.3*N),activation='mish' ,dropout=0.2, )
    l=denseBatchNormF( l, int(0.3*N),activation='mish' ,dropout=0.2, )

    if len(trainer.config.originalTargets)==2:

        l1=denseBatchNormF( l, 1, activation='softmax',activ_name='weightE', )

        l2=denseBatchNormF( l, 1, activation='softmax',activ_name='weightM', )

        l = [l1,l2]
    else:
        raise Exception("WZweightTail requires 2 outputs")
    
    return l


## ***********************************************************
    


# class HeadEtaGETCore1Tail1(ModelDefiner, EtaGausETAnnot,  DenseCore1, Layer1Tail):
#     N = 200

# class HeadEtaGETCore00Tail1(ModelDefiner, EtaGausETAnnot,  DenseCore00, Layer1Tail):
#     N = 120

# class HeadEtaGsSETCore00Tail1(ModelDefiner, EtaGsSETAnnot,  DenseCore00, Layer1Tail):
#     N = 140
# class HeadEtaGsSETCore1Tail1(ModelDefiner, EtaGsSETAnnot,  DenseCore1, Layer1Tail):
#     N = 140

# class HeadEtaGsSETCoreTrTail1(ModelDefiner, EtaGsSETAnnot,  DenseCoreTr, Layer1Tail):
#     N = 140


class HeadEtaBlockCore1Tail1(ModelDefiner, EtaBlockGSAnnot,  DenseCore1, Layer1Tail):
    N = 140


# class HeadEtaBlockCoreT0Tail1(ModelDefiner, EtaBlockGSAnnot,  DenseCoreT0, Layer1Tail):
#     N = 200
# class HeadEtaBlockCoreT1Tail1(ModelDefiner, EtaBlockGSAnnot,  DenseCoreT1, Layer1Tail):
#     N = 200
class HeadEtaBlockCoreT2Tail1(ModelDefiner, EtaBlockGSAnnot,  DenseCoreT2, Layer1Tail):
    N = 200
class HeadEtaBlockCoreT3Tail1(ModelDefiner, EtaBlockGSAnnot,  DenseCoreT3, Layer1Tail):
    N = 200

class HeadEtaBlockCoreT3LDeepT(ModelDefiner, EtaBlockGSAnnot, DenseCoreT3, LayerDeepTail):
    N = 200


class HeadEtaBlockCore1LDeepT(ModelDefiner, EtaBlockGSAnnot,  DenseCore1L, LayerDeepTail):
    N = 200

class HeadEtaGSCore1LDeepT(ModelDefiner, EtaGSAnnot,  DenseCoreT0, Layer1Tail):
    N = 200

class HeadEtaBlockCore1LDeepTA(ModelDefiner, EtaBlockGSAnnot,  DenseCore1L, LayerDeepTailA):
    N = 200

class HeadEtaBlockCore1LAttT(ModelDefiner, EtaBlockGSAnnot,  DenseCore1L, AttentionTail):
    N = 200

class HeadIdlockCore1LAttT(ModelDefiner, Identity,  DenseCore1L, AttentionTail):
    N = 200

class HeadEtaBlockCoreT3AttMDeepT1(ModelDefiner, EtaBlockGSAnnot, DenseCoreT3, AttentionMDeepTail1):
    N = 200

class HeadEtaBlockCoreT3AttMDeepT2(ModelDefiner, EtaBlockGSAnnot, DenseCoreT3, AttentionMDeepTail2):
    N = 200

class HeadEtaBlockCoreT3AttMDeepT3(ModelDefiner, EtaBlockGSAnnot, DenseCoreT3, AttentionMDeepTail3):
    N = 200

class HeadEtaBlockCoreT3AttMDeepT4(ModelDefiner, EtaBlockGSAnnot, DenseCoreT3, AttentionMDeepTail4):
    N = 200

class HeadEtaBlockCoreT3AttMDeepT5(ModelDefiner, EtaBlockGSAnnot, DenseCoreT3, AttentionMDeepTail5):
    N = 200

class HeadEtaBlockCoreT3AttMDeepT6(ModelDefiner, EtaBlockGSAnnot, DenseCoreT3, AttentionMDeepTail6):
    N = 200

class HeadIdCoreT3WZweightT(ModelDefiner, Identity, DenseCoreT3, WZweightTail):
    N = 200

class HeadEtaBlockCoreT3AttEMDeepT6(ModelDefiner, EtaBlockGSAnnot, DenseCoreT3, AttentionEMDeepTail6):
    N = 200

class HeadIdCore1LDeepT(ModelDefiner, Identity,  DenseCore1L, LayerDeepTail):
    N = 200
    
class EtaBlockGCore1LDeepT(ModelDefiner, EtaBlockGAnnot,  DenseCore1L, LayerDeepTail):
    N = 200

class EtaBlockGCore1LDeepTec(ModelDefiner, EtaBlockGAnnot,  DenseCore1L, LayerDeepTailEtaC):
    N = 200

# conversion command :
# trainer2 = trainer.copy( modelBuilder = ModelDefinitions.EtaBlockGCore1LDeepTec(N = 500, coverage= 'smallR3', last_activation='tanh'), targets=['r_e', 'r_m'])


class Core1LEtaDeepT(ModelDefiner, Identity,  DenseCore1L, LayerEtaAnDeepTail):
    N = 200

class EtaSplitCoreDeepT(ModelDefiner, Identity,  EtaSplitCore, LayerDeepTail):
    N = 200

class EtaSplitgaCoreDeepT(ModelDefiner, Identity,  EtaSplitgaCore, LayerDeepTail):
    N = 200
    
class EtaSplitCoreAEDeepT(ModelDefiner, Identity,  EtaSplitCoreAE, DeepTailAE):
    N = 200

class EtaLSplitCoreDeepT(ModelDefiner, Identity,  EtaLargeSplitCore, ReInputTail):
    N = 200
    
class EtaSplitCoreAEDeepTptSF(ModelDefiner, Identity,  EtaSplitCoreAE, DeepTailAEptSF):
    N = 200

    
class EtaSplitCoreAESplitET(ModelDefiner, Identity,  EtaSplitCoreAE, TailAESplitE):
    N = 200

class CoreT0EtaSplitT0(ModelDefiner, Identity,  DenseCoreT0, SplitEtaTailT0):
    N = 200
class Core2LEtaSplitT0(ModelDefiner, Identity,  DenseCore2L, SplitEtaTailT0):
    N = 200
    


    
class Core1SplitetaTail(ModelDefiner, Identity,  DenseCore1L, LayerSplitEtaTail):
    N = 200

class Core1SplitetaTailJES(ModelDefiner, Identity,  DenseCore1L, LayerSplitEtaTailJES):
    N = 200
    

    
class EtaBlockGCore1LAttT(ModelDefiner, EtaBlockGAnnot,  DenseCore1L, AttentionTail):
    N = 200

    
    
# class HeadEEtaBlockCore1LDeepT(ModelDefiner, EtaBlockGsSETAnnot,  DenseCore1L, LayerDeepTail):
#     N = 200

    
    
    
    


    
    


## ******************************************************
## Tests of alternative methods and developments
## ******************************************************
def testEtaMinMax():
    eta_mins0 = [-0.3, 0.2, 0.7, 1., 1.15, 1.35, 1.6 ,2.1, 2.5 ] 
    eta_maxs =  [0.3, 0.8,  1.1, 1.2, 1.4, 1.7,  2.2 , 2.6, 5 ]
    deltas =    [0.05, 0.1, 0.05,0.05,0.05,0.05, 0.05,0.1,0.1]
    deltas = symetrize(deltas, 1)
    eta_mins = mirror(eta_maxs)+eta_mins0[1:]
    eta_maxs = mirror(eta_mins0)[:-1]+eta_maxs
    print(eta_mins ,len(eta_mins))
    print(eta_maxs,len(eta_maxs))
    print(deltas,len(deltas))
    return np.array( [ eta_mins, eta_maxs, deltas])

def addGlobalWeightDecay(model, alpha):
    if isinstance(model, keras.engine.training.Model):
        layers = model.layers
    else:
        layers = [layer]
    for layer in layers:
        if isinstance(layer, keras.layers.Conv2D) or isinstance(layer, keras.layers.Dense):
            layer.add_loss(keras.regularizers.l2(alpha)(layer.kernel))
        if hasattr(layer, 'bias_regularizer') and layer.use_bias:
            layer.add_loss(keras.regularizers.l2(alpha)(layer.bias))




