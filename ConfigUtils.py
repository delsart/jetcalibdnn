# Import Keras (either standalone, either as embedded in tensorflow )
from .ImportKeras import *

## ***********************************************************************
## Configuration
## ***********************************************************************
    
class ConfigDict(dict):
    """An extended dict class allowing to access items as if they were attributes.
       that is d['key'] is equivalent to d.key
    """
    reservedAttributes = ['keys', 'clear', 'update', 'pop', 'iteritems', 'values','setdefault','get','has_key','copy','clone']
    def __init__(self, name='defaultConfig', **kwargs):
        dict.__init__(self,   **kwargs)
        kwargs.update(name=name)
        for k,v in kwargs.items():
            dict.__setattr__(self, k,  v)
    def __getattr__(self, attr):
        try:
            return self[attr]
        except:
            dict.__getattribute__(self,attr)
    def __setattr__(self, attr, value):
        if attr in self.reservedAttributes:
            print ('ConfigDict ERROR can not assign attribute ', attr)
            return
        dict.__setitem__(self, attr,  value)
        dict.__setattr__(self, attr,  value)
    def __setitem__(self, attr, value):
        if isinstance(attr, str):
            self.__setattr__( attr,value)
        else:
            dict.__setitem__(self, attr,  value)


    def pop(self, name, default=None):
        try:
            v = self[name]
        except:
            return default
        dict.__delitem__(self, name)
        dict.__delattr__(self, name)
        return v
        
    def clone(self, **kwargs):
        from copy import deepcopy
        c = deepcopy(self)
        return c.update(**kwargs)

    def update(self, **args):
        for k,v in args.items():
            setattr(self,k,v)
        return self

    def dump(self, prefix='',out=None):
        write = print
        for k,v in sorted(self.items()):
            if isinstance(v, str):
                write(k, '="{}"'.format(v), ',')
            else:
                write(k+' = '+str(v), ',')



## ***********************************************************************

defaultConfig = ConfigDict(
    'defaultConf',
    # ------------
    nnName='',
    inputFiles = '',
    inputDir='',
    outputDir='',
    treeName = "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets" ,

    inputFriends='',

    
    inputClassList = [] , # MUST be in the form [class1, class2] where class1(2) generates inputs for 1(2) targets. See GeneratorFromRootFile.py

    maxEvents = -1,
    # ------------
    
    features = ['e_var','eta', 'm_var','NPV'],
    e_var= 'true_e',
    m_var = 'true_m',

    e_transform = None,
    m_transform = None,
    

    mode = 'training_raw',
    #e_var= 'e'

    # ------------
    targets=['r_e'],
    # ------------
    useWeights = False,
    #epochList = [1000,40000,80000,100000,200000,1000,], 

    modelTag = '',
    optimTag = '',
    specialTag = '',
    
    # ------------
    lossT = -1.5,
    loss = 'logcosh',
    ni_loss= 'mse',

    loss_weights=None,
    # -------------------
    callbacks = [
        #keras.callbacks.EarlyStopping(monitor='loss', patience=2, min_delta=0.01),
        keras.callbacks.ModelCheckpoint('tmp.hdf5', monitor='loss', mode='min',save_best_only=True,verbose=1),


    ],

    # -----------------
    metrics = None,

    sampleWeights= '',
    
    # -------------------
    lr = None,
    # -------------------
    isNIstep = False,

    # ----------------
    nPredictedParam = 2,


    # -------------
    optimizer = 'rada',
    
    # ----------------
    doLookahead = False,
    #lookahead = None,

    #---------------
    cyclicOPtim = None,
    
    # -----------------
    ngpu = 1,
    # -----------------
    nInputFiles = -1, # all


    #-----------
    modelBuilder = None,

    #-----------------
    shuffle = True,

    #-----------------
    additionalVars = [],
    #-----------------
    # a function to optionnally filter variables we really want to load
    #    (typically used when loading data faster during other operations than training/predicing)
    filterLoadedVar = None,

    #
    fitVerbosity = 1, # to be used in model.fit() to show progress bar.

    # default parameters to pass to the trainer.refit() function
    refitParams = dict(),
)
