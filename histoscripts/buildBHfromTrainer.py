# USAGE : 

## 
# exec(open('HistoAnalysis/buildBHfromTrainer.py').read()) # IMPORTANT : this must be done BEFORE the trainer.train( ) call
## then : 
# trainer.runHistoFilling(conf.update(inputFiles=prefix+'AntiKt4EMPFlowJets_*root' , outputDir='' , nInputFiles = -1 , modelBuilder =ModelDefinitions.HeadEtaGSCore1LDeepT(), sampleWeights = None,inputFriends=prefix+'CalibJZ_*.root', ), [eR_in_BINS_uncal,  eR_in_BINS_cal ] , outName='AntiKt4EMPFlow_atlas'+bpsTag+'_resp.h5' , beginFileTask=None)
#

#from HistoAnalysis.
from JetCalibDNN.HistoAnalysis.BinnedArrays import *
#exec(open('BinnedArraysGraphics.py').read())
#plt.ion()
import os
import os.path


def symmetrize(l):
    """ given [0, a, b, c], returns [-c, -b, -a, 0, a, b, c]"""
    newL =[-i for i in l[1:] ] #assumee l starts with 0.
    newL.reverse()
    return newL+l

def seq(start, stop, step):
    return list(np.arange(start, stop+0.99*step, step))


#sampleType = "WZ"
#sampleType = "HHbb"
sampleType = ""

#jetType = 'CSSKUFOSoftDrop'
jetType = 'AntiKt4EMPFlow'

if sampleType=="WZ":
    massBins = [80,100]
elif sampleType=="HHbb":
    massBins = [100,140]
else: # Dijets
    massBins = [40, 80, 100,140,200]
    
atCC = not os.environ["PWD"].startswith('/home/delsart/analysis/')
#atCC =  os.environ["PWD"] == '/pbs/home/d/delsart/DNN/run'

# standard JES
eEtaBPS = BinnedPhaseSpace("eEtaBPS",
                           AxisSpec("E", title="E",edges=[60, 80, 100, 120, 150, 200, 240, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000, 2500, 3000, 3500, 4000, 5000] ),
                           # eta bins used for JES. 'seq' is a helper function defined in PyHelpers.py
                           AxisSpec("eta", title="$\eta$", edges=seq(-3. , 3, 0.1 ) , isGeV=False),
                           )
eEtaBPS.coords = ('e_true', 'eta')

eEtaBPSni = BinnedPhaseSpace("eEtaBPS",
                             #AxisSpec("E", title="E",edges=[10,20,30,40,50, 80, 100,  150, 200,  300,  500,  800,  1200, 1500, 2000, 2500, 3000,  4000, 5000] ),
                             AxisSpec("E", title="E",edges=[10,15,20,25,30,35,40, 50, 70, 100,  150, 200,  300,  500,  800,  1200, 1500, 2000, 2500, 3000,  4000, 5000] ),
                             # eta bins used for JES. 'seq' is a helper function defined in PyHelpers.py
                             AxisSpec("eta", title="$\eta$", edges=seq(-4.5, 4.5, 0.1 ) , isGeV=False),
                           )
eEtaBPSni.coords = ('e_ni', 'eta')



axis_pt = AxisSpec("pT", title="pT",edges=[10,15,20,25,30,35,40,50,60, 80, 100,  150, 200,  300,  500,  800,  1200, 1500, 2000, 2500, 3000,  4000, 5000] )
# eta bins used for JES. 'seq' is a helper function defined in PyHelpers.py
axis_eta = AxisSpec("eta", title="$\eta$", edges=seq(-4.5 , 4.5, 0.1 ) , isGeV=False)

#finer :
axis_pt_f = AxisSpec("pT_f", title="pT",edges=seq(10.,60,2)+[80,100,200,300,5000] )
# eta bins used for JES. 'seq' is a helper function defined in PyHelpers.py
axis_eta_c = AxisSpec("eta_c", title="$\eta$", edges=seq(-4.5 , 4.5, 0.2 ) , isGeV=False)


pTEtaBPS = BinnedPhaseSpace("pTEtaBPS", axis_pt, axis_eta)
pTEtaBPS.coords = ('pt_true', 'eta')

pTrEtaBPS = BinnedPhaseSpace("pTrEtaBPS", axis_pt, axis_eta)
pTrEtaBPS.coords = ('pt_reco', 'eta')

pTEtaBPS_fe = BinnedPhaseSpace("pTEtaBPS_fe", axis_pt_f, axis_eta_c)
pTEtaBPS_fe.coords = ('pt_true', 'eta')

pTrEtaBPS_fe = BinnedPhaseSpace("pTrEtaBPS_fe", axis_pt_f, axis_eta_c)
pTrEtaBPS_fe.coords = ('pt_reco', 'eta')



# ********************************************
eEtaNPVBPS = BinnedPhaseSpace("eEtaNPVBPS",
                              AxisSpec("E", title="E",edges=[100,  200,  300,  500,   1000,  2000,  4000, 6000] ),
                              # eta bins
                              AxisSpec("abseta", title="|$\eta$|", edges=seq(0 , 3, 0.1 ) , isGeV=False),
                              AxisSpec("NPV", title="NPV", edges=[0,10,15,18,20,22,26,30,35,40,60] , isGeV=False),
                              )
eEtaNPVBPS.coords = ('e_true', 'abseta', 'NPV')

# ********************************************
#maxpT = 4000. if sampleType=='WZ' else 3000.
pTMEtaBPS = BinnedPhaseSpace( "pTMEtaBPS",
                              AxisSpec("pt", title="$p_{T}$ ", edges=seq(200,1000,100)+seq(1200,3000,200)+[3500,4000,5000], isGeV=True ),
                              AxisSpec("m", title="mass", edges= massBins, isGeV=True ),
                              AxisSpec("abseta", title="$\eta$", edges=[0,0.2,0.6, 1.0, 1.1, 1.2, 1.3,  1.4, 2., 3  ], isGeV=False)
                             )
pTMEtaBPS.coords = ('pt_true', 'm_true', 'abseta', )





eMEtaBPS = BinnedPhaseSpace("eMEtaBPS",
                            AxisSpec("E",edges= [150,200, 250,350,450,600,750,900,1000,1200,1500,2000,2500, 3000, 3500, 4000, 6000]  ),
                            #AxisSpec("m",title="mass", edges=[0,30,40,50,60,70,80,100,200,400] ),
                            AxisSpec("m", title="mass", edges=massBins, isGeV=True ),
                            #AxisSpec("eta",title="$\eta$", edges=np.linspace(-3.,3.,31), isGeV=False ) , #seq(-3.,3.,0.4) )
                            AxisSpec("abseta", title="$\eta$", edges=[0,0.2,0.6, 1.0, 1.1, 1.2, 1.3,  1.4, 2., 3  ], isGeV=False)                            
                            #AxisSpec("eta",title="$\eta$", edges=symmetrize( seq(0,0.8,0.2) + seq(0.9,1.8,0.1) + seq(2,3,0.2) ), isGeV=False ) , #seq(-3.,3.,0.4) ) 
                            )
eMEtaBPS.coords = ('e_true', 'm_true','abseta')

eLMoEEtaBPS = BinnedPhaseSpace("eLMoEEtaBPS",
                               AxisSpec("E",edges= [150,200, 250,350,450,600,750,900,1000,1200,1500,2000,2500, 3000, 3500, 4000, 6000]  ),
                               #AxisSpec("m",title="mass", edges=[0,30,40,50,60,70,80,100,200,400] ),
                               AxisSpec("LMoE", title="LogMoE", edges=[-8,-6,-5,-4,-3,-2,-1,-0.5], isGeV=True ),
                               #AxisSpec("eta",title="$\eta$", edges=np.linspace(-3.,3.,31), isGeV=False ) , #seq(-3.,3.,0.4) )
                               AxisSpec("abseta", title="$\eta$", edges=[0,0.2,0.6, 1.0, 1.1, 1.2, 1.3,  1.4, 2., 3  ], isGeV=False)                            
                               #AxisSpec("eta",title="$\eta$", edges=symmetrize( seq(0,0.8,0.2) + seq(0.9,1.8,0.1) + seq(2,3,0.2) ), isGeV=False ) , #seq(-3.,3.,0.4) ) 
                            )
eLMoEEtaBPS.coords = ('e_true', 'LogMoE','abseta')


eMEtaBPSreco = BinnedPhaseSpace("eMEtaBPS",
                                AxisSpec("E",edges= [50, 120, 300,600,1400,2600, 4000,  6000]  ),
                                AxisSpec("m",title="mass", edges=[0,20, 60,100,140,300,400] ),
                                #AxisSpec("eta",title="$\eta$", edges=np.linspace(-3.,3.,31), isGeV=False ) , #seq(-3.,3.,0.4) )
                                AxisSpec("abseta",title="|$\eta$|", edges=[0,0.3,0.6,0.9,1.,1.2,1.4,1.8,2,2.5,3.1 ], isGeV=False ) , #seq(-3.,3.,0.4) ) 
                                )
eMEtaBPSreco.coords = ('e_reco', 'm_reco','abseta')


if 1:
    # Standard response histos 
    #theBPS = eEtaNPVBPS
    #theBPS = eEtaBPS
    #theBPS = pTMEtaBPS
    #theBPS = eLMoEEtaBPS
    #theBPS = eMEtaBPS

    #theBPS = eEtaBPSni    
    theBPS = pTEtaBPS
    theBPSr = pTrEtaBPS
    #theBPS = pTEtaBPS_fe
    #theBPSr = pTrEtaBPS_fe


    
    bpsTag = theBPS.axisTag()
    mR_in_BINS_dnn = BinnedHistos( "mR_in_"+bpsTag+"_dnn", theBPS, (300,(0.01,3)) )
    mR_in_BINS_dnn.coordNames = theBPS.coords
    mR_in_BINS_dnn.valueName = 'r_dnn_m'
    
    eR_in_BINS_dnn = BinnedHistos( "eR_in_"+bpsTag+"_dnn", theBPS, (240,(0.4,1.8)) )
    eR_in_BINS_dnn.coordNames = theBPS.coords
    eR_in_BINS_dnn.valueName = 'r_dnn_e'

    eR_in_BINS_r_dnn = BinnedHistos( "eR_in_"+bpsTag+"_r_dnn", theBPSr, (240,(0.4,1.8)) )
    eR_in_BINS_r_dnn.coordNames = theBPSr.coords
    eR_in_BINS_r_dnn.valueName = 'r_dnn_e'
    
    mR_in_BINS_uncal = BinnedHistos( "mR_in_"+bpsTag+"_uncal", theBPS, (300,(0.01,3)) )
    mR_in_BINS_uncal.coordNames = theBPS.coords
    mR_in_BINS_uncal.valueName = 'r_raw_m'
    
    eR_in_BINS_uncal = BinnedHistos( "eR_in_"+bpsTag+"_uncal", theBPS, (240,(0.4,1.8)) )
    eR_in_BINS_uncal.coordNames = theBPS.coords
    eR_in_BINS_uncal.valueName = 'r_raw_e'

    eR_in_BINS_r_uncal = BinnedHistos( "eR_in_"+bpsTag+"_r_uncal", theBPSr, (240,(0.4,1.8)) )
    eR_in_BINS_r_uncal.coordNames = theBPSr.coords
    eR_in_BINS_r_uncal.valueName = 'r_raw_e'
    
    
    mR_in_BINS_cal = BinnedHistos( "mR_in_"+bpsTag+"_cal", theBPS, (300,(0.01,3)) )
    mR_in_BINS_cal.coordNames = theBPS.coords
    mR_in_BINS_cal.valueName = 'r_cal_m'
    
    eR_in_BINS_cal = BinnedHistos( "eR_in_"+bpsTag+"_cal", theBPS, (240,(0.4,1.8)) )
    eR_in_BINS_cal.coordNames = theBPS.coords
    eR_in_BINS_cal.valueName = 'r_cal_e'

    eR_in_BINS_pred = BinnedHistos( "eR_in_"+bpsTag+"_pred", theBPS, (240,(0.4,1.8)) )
    eR_in_BINS_pred.coordNames = theBPS.coords
    eR_in_BINS_pred.valueName = 'r_pred_e'
    

    # in some smallR ntuple we have the intermediate "EtaJES" calibrated E
    eR_in_BINS_jes = BinnedHistos( "eR_in_"+bpsTag+"_jes", theBPS, (240,(0.4,1.8)) )
    eR_in_BINS_jes.coordNames = theBPS.coords
    eR_in_BINS_jes.valueName = 'r_jes_e'

    # in some smallR ntuple we have the intermediate "EtaJES" calibrated E
    mR_in_BINS_jes = BinnedHistos( "mR_in_"+bpsTag+"_jes", theBPS, (300,(0.01,3)) )
    mR_in_BINS_jes.coordNames = theBPS.coords
    mR_in_BINS_jes.valueName = 'r_jes_m'


    # Bins r_ni_e == prediction of r from the TRUE e == the ratio to obtain NI e (e_ni) from e_true
    bpsTagni = eEtaBPSni.axisTag()
    eR_in_BINS_rNI = BinnedHistos( "eR_in_"+bpsTagni+"_dnnNI", theBPS, (300,(0.01,3)) )
    eR_in_BINS_rNI.coordNames = eEtaBPSni.coords
    eR_in_BINS_rNI.valueName = 'r_ni_e'

    # pred_ni_e == prediction of r_ni_e from the NI e ==> 1/pred_ni_e is the calib factor when evaluated from e_reco
    eR_in_BINS_predNI = BinnedHistos( "eR_in_"+bpsTagni+"_dnnPredNI", theBPS, (300,(0.01,3)) )
    eR_in_BINS_predNI.coordNames = eEtaBPSni.coords
    eR_in_BINS_predNI.valueName = 'r_nipred_e'


    # if atCC:
    #     allBinnedHistos = [ mR_in_BINS_dnn, eR_in_BINS_dnn, eR_in_BINS_pred        ]
    # else:
    #     allBinnedHistos = [ #mR_in_BINS_cal, eR_in_BINS_cal,
    #         #mR_in_BINS_uncal,
    #         eR_in_BINS_uncal,
    #         #               eR_in_BINS_jes, mR_in_BINS_jes,
    #                        ]

        
else:
    # 
    theBPS = eMEtaBPSreco
    bpsTag = theBPS.axisTag()
    allBinnedHistos = []
    for vn in conf.features:
        if vn=='e_var': vn='e_reco'
        if vn=='m_var': vn='m_reco'
        v = Variable.referenceVars[vn]
        histoBinV = BinnedHistos( v.name , theBPS, (200,v.h_range) )
        histoBinV.coordNames = theBPS.coords
        histoBinV.valueName = v.name

        allBinnedHistos += [ histoBinV ]


# # the variables needed for histo building
# histoVars = set( theBPS.coords + tuple(bh.valueName for bh in allBinnedHistos) )


# for v in ['pt_true', 'abseta', 'e_jes', 'm_jes', 'pt_reco', 'r_cal_e']:
#     if v in theBPS.coords :
#         conf.additionalVars.append(v)

# if not  atCC :
#     conf.filterLoadedVar = lambda vL : [v for v in vL if v.name in histoVars]
#     #conf.additionalVars += ['r_cal_e', 'r_cal_m', 'r_jes_e', 'r_jes_m']
# else:    
#     #conf.additionalVars += ['r_dnn_e', 'r_dnn_m', ]
#     pass



def adaptVariables(config, bhList):
    needDNNPred = any( 'dnn' in cont.name for cont in bhList)

    # collect all variables from phase-spaces coordinates and histo variables :
    histoVars = set( sum( (bh.bps.coords+(bh.valueName,) for bh in  bhList), () ) )

    if not needDNNPred:
        # then we can filter out any variable not needed for histos :
        config.filterLoadedVar = lambda vL : [v for v in vL if v.name in histoVars]
    else:
        # remove 'r_dnn_X' vars : they will be predicted and need not to be prepared from the inputs
        for v in ['r_dnn_e', 'r_dnn_m']:
            histoVars.discard(v)
        
    conf.additionalVars += list(histoVars)
    return config
    

def adaptVar_pt_reco_lows(config,bhList):
    needDNNPred = any( 'dnn' in cont.name for cont in bhList)
    if 'pt_var_lows' in config.features and needDNNPred:
        pTrEtaBPS.coords = ('pt_reco_lows','eta');
        eR_in_BINS_r_uncal.coordNames = pTrEtaBPS.coords
        eR_in_BINS_r_dnn.coordNames = pTrEtaBPS.coords
    return adaptVariables(config, bhList)


#adaptConfigFunc = adaptVariables
adaptConfigFunc = adaptVar_pt_reco_lows



def runHistoFilling(self, config, bhList, reload=0,  beginFileTask=None, outName=None, nFile=-1, inputMode='reco'):

    # adapt config to histo building :
    config = adaptConfigFunc( config, bhList )

    # force config to be in  prediction mode
    config.mode= inputMode+':predict'

    # setup the Trainer :
    self.train(config, reload = reload, fitSequence=None)

    if outName is None:
        tag = prefix if reload==1 else prefix+reload
        outName = trainer.outName(tag).replace('.h5', bpsTag+'respDNN.h5')
    
    print("*********Running histo filling ********")
    print(" will ouput to ",outName)
    print()
    # call the actual histo filling procedure :
    self.fillBinnedHistos( bhList, useWeights=bool(config.sampleWeights), beginFileTask=beginFileTask, outName=outName,nFile=nFile)

Trainer.runHistoFilling = runHistoFilling                           
    
    
def fillBinnedHistos(self, bhList, useWeights=True, beginFileTask=None, outName=None, nFile=-1,  ):
    self.gen.reset()
    gen = self.gen

    mode = self.config.mode.split(':')[1]
    if mode not in ('predict', 'testnipred'):
        print('ERROR !! use predict OR testnipred mode, not ', self.config.mode)
        return
    if not isinstance(bhList, list): bhList = [bhList]

    if useWeights :
        for c in gen.allChains():
            c.weights_var.normalize = False


    
    # group by bps to factorize indices calculations
    bhDict = dict()
    for bh in bhList:
        bhDict.setdefault( (bh.bps,)+bh.coordNames, []).append(bh)
        
        
    maxDimBPS = max( bhList , key=lambda bps:bh.bps.nDim() ).bps

    # instantiate one array of N-dim indices suitable to the bigges BPS & input data
    binIndices = maxDimBPS.buildNCoordinates(gen.chain.max_entries)
    hIndices = np.zeros( (gen.chain.max_entries,) ,dtype=int)
    nFile = len(gen.input_files) if nFile==-1 else nFile
    for i in range(nFile):
        gen.loadFile(i, noTransform=True)
        gen.transform_current_sample() # make sure the ratios are calculated
        gen.untransform_current_sample() # make sure we revert to nominal variables

        
        allVars = self.allVariables()
        if callable(beginFileTask): beginFileTask(self)
        
        N = gen.current_file.Nentries
        if useWeights:
            w0 = gen.chain.weights_var.array[:N]
            
        #loop over the BinnedHistos which share the same bps & coordinates 
        for _, bhL in bhDict.items():            
            bps = bhL[0].bps 
            print("******************** Fill for BPS ",bps.name)
            #print(allVars.m_true.array[:7])
            # compute the common index coordinates for these BinnedHistos :
            coords = [ allVars[v].array[:N] for v in bhL[0].coordNames]
            tupleI, validCoordI = bps.findValidBins(coords, binIndices[:bps.nDim(),:N] )

            # Then fill these BH with their values at the calculated coordinates
            w = w0[validCoordI] if useWeights else 1.
            w2=w*w
            for bh in bhL:
                print("******************** ---> Fill for BinnedHistos ",bh.name)
                values = allVars[bh.valueName].array
                print('---> ',values)
                bh.fillAtIndices( values[:N], validCoordI, tupleI, w, w2, hIndices=hIndices[:N])
    if outName:
        print("Saving Binned Histo into ", outName)
        self.saveBinnedHistosAsH5(bhList, outName)



        
def predOnly(trainer, pred_vname_e= 'r_pred_e', pred_vname_m='r_pred_m'):
    N = trainer.gen.current_file.Nentries
    class _tmp:
        def __init__(self,a):
            self.array=a
    r = trainer.fullPredictions()
    trainer.gen.format_current_sample(reverse=True) # to get back e and m at normal scales.

    print( 'UUUUUUUU ', r[:10,0] )
    # assigne the prediction to a dummy Variable onto the current list of vars :
    trainer.gen.chain.allVars[pred_vname_e] = _tmp(r[:,0])
    
    hasM = len(trainer.config.originalTargets) > 1
    
    if hasM:
        trainer.gen.chain.allVars[pred_vname_m] = _tmp(r[:,trainer.config.nPredictedParam])

def predNIOnly(trainer):
    return predOnly(trainer, 'r_nipred_e', 'r_nipred_m')

        
def predCalib(trainer):
    N = trainer.gen.current_file.Nentries
    class _tmp:
        def __init__(self,a):
            self.array=a
    r = trainer.fullPredictions()
    trainer.gen.format_current_sample(reverse=True) # to get back e and m at normal scales.
    r_cal_e = r[:,0]
    hasM = len(trainer.config.originalTargets) > 1

    if hasM:
        r_cal_m = r[:,trainer.config.nPredictedParam]

        print("r_m", trainer.gen.chain.allVars.r_m.array[:7])
        print("r_cal_m", r_cal_m[:7])

    if trainer.config.optimTag != 'directSF':
        # perform r_cal = r / r_cal in-place :
        r_cal_e = np.reciprocal( r_cal_e, out=r_cal_e)
        if hasM:
            r_cal_m = np.reciprocal( r_cal_m, out=r_cal_m)  

    r_cal_e *= trainer.gen.chain.allVars.r_e.array[:N]
    trainer.gen.chain.allVars.r_dnn_e = _tmp(r_cal_e)
    if hasM:
        r_cal_m *= trainer.gen.chain.allVars.r_m.array[:N]
        print("r_cal_m", r_cal_m[:7])    
        trainer.gen.chain.allVars.r_dnn_m = _tmp(r_cal_m)


def predCalibAndResp(trainer):
    N = trainer.gen.current_file.Nentries
    class _tmp:
        def __init__(self,a):
            self.array=a
    r = trainer.fullPredictions()
    trainer.gen.format_current_sample(reverse=True) # to get back e and m at normal scales.
    r_cal_e = r[:N,0]
    hasM = len(trainer.config.originalTargets) > 1
    if hasM:
        r_cal_m = r[:N,trainer.config.nPredictedParam]

        print("r_m", trainer.gen.chain.allVars.r_m.array[:7])
        print("r_cal_m", r_cal_m[:7])

        print("r_cal_m", r_cal_m[:7])

    
    #trainer.gen.chain.allVars.r_pred_m = _tmp(r_cal_m)
    trainer.gen.chain.allVars.r_pred_e = _tmp(r_cal_e)

    c_cal_e = np.reciprocal( r_cal_e, ) 

    trainer.gen.chain.allVars.r_dnn_e = _tmp(c_cal_e*trainer.gen.chain.allVars.r_e.array[:N])

    if hasM:
        np.reciprocal( r_cal_m, out=r_cal_m)  
        trainer.gen.chain.allVars.r_dnn_m = _tmp(r_cal_m*trainer.gen.chain.allVars.r_m.array[:N])

    
def saveBinnedHistosAsROOT(self, bhList, fname):
    f = uproot.recreate(fname)
    for bh in bhList:
        print("... Saving  ", bh.name)        
        bh.saveAsROOT(f)
    f.close()

def saveBinnedHistosAsH5(self, bhList, fname):
    if os.path.exists( fname) : os.remove(fname)        
    for bh in bhList:
        print("... Saving  ", bh.name)        
        bh.saveInH5(fname)

    if self.net is not None:
        self.saveTrainSummary(fname)
    
Trainer.fillBinnedHistos = fillBinnedHistos
Trainer.saveBinnedHistosAsH5 = saveBinnedHistosAsH5
Trainer.saveBinnedHistosAsROOT = saveBinnedHistosAsROOT


prefix = sampleType+'_' if sampleType else ''
print("""trainer.prepareData(conf.update(inputFiles=prefix+'CSSKUFOSoftDrop_*root' , outputDir='' , nInputFiles = -1 , modelBuilder =ModelDefinitions.HeadEtaBlockCore1LDeepT(N = 600, coverage= 'full', batchNorm=True ), sampleWeights = "Eweights", mode= 'reco:predict' ,inputFriends=prefix+'CSSKUFOSoftDropAtlasCalib*root', doLookahead=True), initScalers=False)
""")
