###################################################################
##
## This file defines utility function to do various plots
##
## 

## **********************
# plot usage :
# basic summary plots :
# trainer.checkNN(otherVar=dict(fixedToMean=True),respRange=(0.7,0.95) )
# trainer.checkNN(otherVar=dict(fixedToMean=True,m=80),respRange=(0.7,0.95) )
# Draw many plots :
# trainer.savePDFsample()

from JetCalibDNN.ConfigUtils import ConfigDict
import numexpr as ne
import os

from JetCalibDNN.Variables import Variable, Histo2D, Histo1D


class ShakeWeights(keras.callbacks.Callback):
    """Example usage :
    shaker.shakeModel( trainer.net )
     or
    shaker.shakeModel( trainer.net, ancestorsOf='outputMAvg')
    or (shake layers NOT ancestors of outputMAvg) :
    shaker.shakeModel( trainer.net, ancestorsOf='!outputMAvg')

    """
    shakeFrac=0.05
    def on_epoch_end(self,ep, logs,):
        if doShake:
            self.shakeModel(self.model)


            
    def shakeModel(self,model, sfrac=0.05, ancestorsOf=None):
        self.shakeFrac = sfrac
        if ancestorsOf is None:
            layerL = model.layers
        else:
            if '!' == ancestorsOf[0]:
                ancestors = utils.findDenseAncestorLayers(model, initname=ancestorsOf[1:])
                layerL = [l for l in model.layers if ('dense' in l.name) and (l not in ancestors) ]
            else:
                layerL = utils.findDenseAncestorLayers(model, initname=ancestorsOf)
    
        for la in layerL:
            self.shakeLayer(la,)
            
    def shakeLayer(self, layer, s0=None):
        wL = layer.get_weights()
        if wL==[]: return
        weights = wL[0]
        shakeAmpl = []
        for row in weights.T: # use T !!!
            
            if s0 is None:
                m=np.linalg.norm(row)            
                if m<1e-7: s=0.1
                else : s =m*self.shakeFrac
            else: s=s0
            row += np.random.normal(scale=np.full_like(row,s))
            shakeAmpl.append(s)
        print( layer.name, ' --> ',shakeAmpl)
        bias = wL[1]
        bias += np.random.normal(scale=np.array(shakeAmpl))
        #weights += np.random.normal(scale=np.full_like(weights,s))
        
        layer.set_weights(wL)


        
shaker = ShakeWeights()    




def testArray(x):
    if np.any(np.isnan(x)):
        print("nan x error at ", np.flatnonzero(np.isnan(x) ) ) ; return False
    if np.any(np.isinf(x)):
        print("inf x error at ", np.flatnonzero(np.isinf(x)) ); return False
    #print(x[0])
    return True

def testWeights(model):
    for l in model.layers:
        wL=l.get_weights()
        if wL==[]:
            continue
        ok = testArray( wL[0] )
        if not ok :
            print(' --> matrix at layer ', l.name )            
        ok = testArray( wL[1] )
        if not ok :
            print(' --> bias at layer ', l.name )

def testTrainerGen(trainer):
    #trainer.setupConfig(conf.update(inputDir="data/", targets=['r_e',  ],  outputDir='testFS/', nInputFiles=6, modelBuilder=ModelDefinitions.splitedEtaE),)
    #trainer.setupConfig(conf.update(inputDir="data/", targets=['r_e',  ],  outputDir='testFS/', nInputFiles=6, modelBuilder=ModelDefinitions.splitedEtaE), )
    from JetCalibDNN.GeneratorFromRootFile import Variable
    #Variable.allVars.r_e.array_filter = None
    #trainer.prepareData()
    trainer.gen.set_batch_size(100000)
    #trainer.setupModel(
    N = len(trainer.gen)
    #return
    nview=0
    for i in range(N):
        #print(i)
        x,y= trainer.gen[i]
        nview += len(y)
        if np.any(np.isnan(x)):
            print("nan x error at ",i, x) ; return
        if np.any(np.isinf(x)):
            print("inf x error at ",i, x); return
        if np.any(np.isnan(y)):
            print("nan y error at ",i, y); return
        if np.any(np.isinf(x)):
            print("inf y error at ",i, y); return
        #print(x[0])
    return nview 



e_reco = 400
m_reco = 40
eta_reco = 0.1
Qw=30
Split23  = 15
D2 = 0.1
groomMratio = 0.6
effNConst = 1.2
mu = 21
#array0 = np.array( mean) #)[e_reco,m_reco,eta_reco,Qw,Split23,D2,groomMratio, effNConst,mu])
    
import matplotlib
import matplotlib.pyplot as plt
plt.ion()

myDPI=96.
class Fig:
    fig = None
    axs = None
    gobjects = (None,None)
    
    def get(self, name='dfig', shape=(2,2) ):
        f = getattr(self, name, None)
        if f :
            axes = getattr(self,name+'Axes')
            print('ffffff',name, shape, axes.shape)
            if shape == axes.shape:
                
                return f, getattr(self,name+'Axes')
            f.clf()
        else:
            f = plt.figure(name , figsize = (1900/myDPI,1000/myDPI) )
        
        f.set_tight_layout(True)
        axs = f.subplots(*shape)
        if not isinstance(axs, np.ndarray): axs = np.array([axs])
        #print('ffffffffff', name, axs, f.axes)
        setattr(self, name,f)
        setattr(self, name+'Axes',axs)
        return f, axs
    
if 'theFig' not in dir():
    theFig = Fig()


def call(self, **props):
    for k,v in props.items():
        f=getattr(self,k)    
        f(*v)
matplotlib.axes.Axes.callMethods = call


massMods = None
def massPeak(array):
    global massMods
    if massMods is None:
        from  scipy.interpolate import RectBivariateSpline
        fm=np.load("massMods.npz")
        x,y,z= fm['x'], fm['y'], fm['z']
        massMods = RectBivariateSpline(x,y,z)
    
    e = array[:,1]
    eta = array[:,0]
    m=massMods.ev( e, eta )
    return m


def logMoEatM( mass ):
    def logMoE(array):
        e = array[:,1]
        r=np.log(mass/e)
        return r
    logMoE.desc = 'm='+str(int(mass))
    return logMoE

def logMoE_andMany_atM(mass):
    def logMoE(array):
        e = array[:,1]
        r=np.log(mass/e)
        Qw = 0.38*mass
        array[:, trainer.featureIndex('Qw')] = Qw
        D2 = 1.6+ 2.*np.exp(-0.1*mass) # 1.5,3.,0.16
        array[:, trainer.featureIndex('D2')] = D2
        
        print('sssssssss ',e[:5], r[:5])
        return r
    logMoE.desc = 'm='+str(int(mass))+'+Qw,D2'
    return logMoE

def manyfeatures_atM(mass):
    def featuresAtM(array):
        Qw = 0.38*mass
        array[:, trainer.featureIndex('Qw')] = Qw
        D2 = 1.6+ 2.*np.exp(-0.1*mass) # 1.5,3.,0.16
        array[:, trainer.featureIndex('D2')] = D2
        
        #print('sssssssss ',e[:5], r[:5])
        return mass
    featuresAtM.desc = 'm='+str(int(mass))+'+Qw,D2'
    return featuresAtM


def transformlogMoEatM( array, marray):
    e = array[:,1]
    array[:,2] = np.log(marray/e)
    #print('sssssssss ',e[:5], r[:5])
transformlogMoEatM.vname='LogMoE'




# def massPeak(array):
#     e = array[:,1]
#     eta = array[:,0]
#     p0 = 8.67 + e*0.000773688
#     p1 = 1.71951 +np.sqrt(e)*0.548143
#     p2 = 1.53915 + 5.30281e-05 *e
#     m =p0+p1*np.exp(-eta**2/p2)
#     return m
massPeak.desc= 'm peak'
def massvsE(array):
    e = array[:,1]
    return 0.1*e+10
massvsE.desc="m=E/10+10"

def massvsEalt(array):
    e = array[:,1]
    return e/1.2+10
massvsEalt.desc="m=E/12+10"

def addToClass(*classes):
    """function decorator. When decorating a function f with '@addToClass(A,B,C)' then f will be added to class A B and C"""
    def classDecorator(func):
        for k in classes:
            setattr(k,func.__name__,func)
    return classDecorator



def readInterpolatedFeatures(trainer, fname):
    f = h5py.File(fname, mode='r')
    from types import SimpleNamespace
    interp = SimpleNamespace()
    features =[]
    for vname, array in f.items():
        setattr(interp, vname, array[()])
        if vname not in ['ecenters', 'etacenters', 'mcenters', 'm_reco']:
            features.append(vname)
        
    from scipy.interpolate import RegularGridInterpolator

    interp.etacenters[0]=0.  # just to avoid out of bounds in extrpolation
    interp.ecenters[-1]=5050.# just to avoid out of bounds in extrpolation
    interp.etacenters[-1]= 3.1# just to avoid out of bounds in extrpolation
    interp.mcenters[-1]= 430. # just to avoid out of bounds in extrpolation
    interp.mcenters[0]= 0. # just to avoid out of bounds in extrpolation
    xyz = (interp.ecenters, interp.mcenters, interp.etacenters)
    for v in features:
        setattr(interp, v+'Func', RegularGridInterpolator( xyz, getattr(interp,v) ) )
    interp.m_recoFunc = RegularGridInterpolator( (interp.ecenters, interp.etacenters) , interp.m_reco ) 



    # xyz will contain e,m and eta. Ex: e== xyz[:,0] 
    def LowMoEFunc(xyz):
        moe = xyz[:,1]/xyz[:,0]
        t = Variable.referenceVars['LowMoE'].threshold
        #print( 'e=', xyz[:,0][:6] , 'e=', xyz[:,1][:6]  )
        return np.where( moe>t, 0, moe-t)
    interp.LowMoEFunc = LowMoEFunc

    interp.LowMoESFunc = lambda xyz : np.log(1+np.exp( 3*(0.04-xyz[:,1]/xyz[:,0])) )
    
    # def lmoe(xyz):
    #     print('zzzzzzzzzz' , xyz[:2])
    #     return np.log(xyz[:,1]/xyz[:,0])
    interp.LogMoEFunc = lambda xyz : np.log(xyz[:,1]/xyz[:,0])
    
    
    
    trainer.interp = interp
 
Trainer._refInput = None   
@addToClass(Trainer)
def refInput(self, ):
    if self._refInput is None:
        trainer.gen.loadFile(0)
        x,y = trainer.gen.current_unfiltered_sample(formatted=False)
        self._refInput = x.mean(0)
    return self._refInput


@addToClass(Trainer)
def getInputArray(self, vsVar, vvalues,otherVar0={}, refInput = None):
    otherVar = dict(otherVar0) # so we can modify the copy
    xvalues = np.array(vvalues)
    features = self.config.features

    refInput = refInput if refInput is not None  else  self.refInput()
    array = np.tile(refInput, (len(vvalues),1) )

    #if not hasattr(self, 'interp'): readInterpolatedFeatures(self, 'HistoAnalysis/featureInterp.h5')

    if vsVar in otherVar:
        print("ERROR getInputArray : vsVar ", vsVar, " is also in otherVar")
        return

    e_name='e_var' if 'e_var' in features else Variable.aliasConfig.reco.e_var        
    m_name='m_var' if 'm_var' in features else Variable.aliasConfig.reco.m_var        
    eta_name= Variable.aliasConfig.reco.eta
    # set inputs
    if vsVar=='e':
        vsVar= e_name
    if vsVar=='m': vsVar=m_name
    if vsVar=='eta': vsVar=eta_name
    
    print('AAAA',vsVar, array.shape, array)
    vi0 = features.index(vsVar)
    array[:,vi0] = xvalues
    vindex = features.index

    featuresVsEEtaM = not otherVar.pop('fixedToMean',False)
    if featuresVsEEtaM :
        print("fixedToMean not supported anymore")
        return
    
    # set e
    e_i = vindex(e_name)
    if 'e' in otherVar:
        array[:,e_i] = otherVar.pop('e')
    evalues = array[:,e_i]
    
    # set eta0
    eta_i = vindex(eta_name)
    if 'eta' in otherVar: # in otherVar we expect to just use the generic 'eta' to refer to the eta Variable
        array[:,eta_i] = otherVar.pop('eta')
    etavalues = np.abs(array[:,eta_i])
    
    # set m
    if 'm' in otherVar: otherVar[m_name] = otherVar['m']
    #print("eeee", etavalues)
    if vsVar != m_name:
        if featuresVsEEtaM:
            array[:,vindex(m_name)] = self.interp.m_recoFunc( np.stack( (evalues, etavalues), axis=1 ) )

    #print( ' mm--> ', array[:,vindex(m_name)])
    if featuresVsEEtaM:
        xyz  = np.stack( (evalues, array[:,vindex(m_name)], etavalues) , axis=1)
        for v in features:
            if v in (e_name, 'eta', m_name, vsVar):continue
            func = getattr(self.interp, v+'Func')
            array[:,vindex(v) ] = func( xyz )

    for v,val in otherVar.items():
        if v == 'm':v=m_name
        #print('setting ',v,' to', val)
        array[:,vindex(v) ] = val
    #print(array[:,vindex(v) ])
    return xvalues, array


@addToClass(Trainer)
def getPredictions(self, vsVar, vvalues, otherVar={},refInput = None, rescalePreds=True):
    """returns (xvalues, predictions, otherInputs  ) where :
      -  xvalues are those corresponding to 'vsVar' and 'vvalues' and 
      - otherInputs are all input features used for evaluations (which can dependend on otherVar)
    """
    #print(array)
    xvalues , inputs = self.getInputArray(vsVar, vvalues, otherVar, refInput)
    #print(inputs)

    pred= self.predict(inputs, rescalePreds )
    return xvalues, pred, inputs

@addToClass(Trainer)
def getPredictionsNI(self, vsVar, vvalues, otherVar={},):
    #print(array)
    xvalues , scaledInputs = self.getInputArray(vsVar, vvalues, otherVar)
    pred= self.predict(scaledInputs )
    xvalues[:,0] *=pred[:,0] #works only for E
    return xvalues, pred


def expandDicOfList( dicOfList):
    """Expands a dict of list into a list of dict.
    ex : dict( a=[0,1,2], b=[10,20] ) -->[{'a': 0, 'b': 10}, 
                                          {'a': 0, 'b': 20}, 
                                          {'a': 1, 'b': 10}, 
                                          {'a': 1, 'b': 20}, {'a': 2, 'b': 10}, {'a': 2, 'b': 20}]
    """
    class V:
        def __init__(self, d):
            self.d=d
        def __mul__(self,o):
            return V(dict(self.d, **o.d))
        __rmul__ = __mul__

        @staticmethod
        def multList(l1, l2):
            return [ v1*v2 for v1 in l1 for v2 in l2]

    listOfList = []
    for (k,kL) in dicOfList.items():
        if not isinstance(kL ,(list, tuple) ):
            kL = [kL]
        listOfList.append( [ V( {k:v} ) for v in kL] )
    
    finalL = listOfList[0]
    for l in listOfList[1:]:
        finalL = V.multList(finalL, l)
    return [v.d for v in finalL]
    
    


def prettyV(k,v):
    if callable(v):
        #return v.__name__
        return v.desc        
    if v<10: return '{}={:.2f}'.format(k,v)
    return '{}={:d}'.format(k,int(v))
# return str(int(v/1000))+'GeV'

@addToClass(Trainer)
def drawNN(self, v, vvalues,   respRange=(0.9,1.), resolRange=(0,0.15) , otherVar={}, axs=None, toDraw=[1,1,1,1]):

    if axs is None:        
        fig,axs = theFig.get('ofig', shape=(1,len(toDraw)) )
        for a in axs: a.cla()
        
    allVDics = expandDicOfList(otherVar)

    allVars = Variable.referenceVars
     
    targets = [allVars[t] for t in self.config.targets]
    
    vd = allVars[v.vname] if callable(v) else allVars[v]
    plotDic = {}

    doprint=False
    for d in allVDics:

        #axi = 0
        xv, predV, allinputFeaures = self.getPredictions(v, vvalues,otherVar=d)
        # if vd.isGeV:ax
        #     print( vd.name, xv)
        #     xv /=1000.
        label = ' '.join(prettyV(k,v) for (k,v) in d.items() )

        for axi,predD in enumerate(toDraw):
            axs[axi].plot( xv, predV[:,predD.index],marker='o',label=label)
            axs[axi].set(ylim=predD.range,xlabel=vd.title,ylabel=predD.title,)
            
    
    #print(array)
    #print(pred[:,predI], pred.shape)
    for a in axs:
        ax, ay = a.get_xaxis(), a.get_yaxis()
        ax.set_label_coords(0.9,-0.08)
        ay.set_label_coords(-0.06, 0.85)
        ax.label.set_fontweight('bold')
        ay.label.set_fontweight('bold')
        a.callMethods(grid=(True,), legend=() )

    
    return axs


# *******************************************
class PredDescriptor:
    def __init__(self, index,range, title, **args):
        self.set( index=index, range=range, title=title, **args)

    def set(self,**args):
        for k,v in args.items():
            setattr(self,k,v)

npred=trainer.config.nPredictedParam
preds= ConfigDict(
    e_resp  = PredDescriptor(0, (0.7,0.95), "E response" ),
    e_resol = PredDescriptor(1, (0.,0.15), "E resolution" ),
    e_assyn = PredDescriptor(2, (0.,1.), "E assymetry" ),

    m_resp  = PredDescriptor(npred, (0.75, 1.2), "M response" ),
    m_resol = PredDescriptor(npred+1, (0.,0.5), "M resolution" ),
    m_assym = PredDescriptor(npred+2, (0.,1.), "M assymetry" ),    
)
# *******************************************

@addToClass(Trainer)
def checkNN(self,  predictionsFunc=None , clear=True, marker='o', otherVar={}, toDraw=None):
    self.refInput() # just to make sure it's setup
    config = self.config

    toDraw = toDraw or [preds.e_resp, preds.e_resol, preds.m_resp, preds.m_resol] 
    
    ncols = len(toDraw)
    fig, axs = theFig.get( shape=(2,ncols) )
    if clear:
        for a in axs.flat:
            a.cla()
    if axs.shape==(2,):
        axs=axs.reshape(2,1)
        theFig.axs=axs
        print (axs)

    if predictionsFunc is None:predictionsFunc=self.getPredictions
    otherVar = ConfigDict(**otherVar)

    self.drawNN( 'e', [150.+100.*i for i in range(0,50)], otherVar=otherVar.clone(eta= [0.05,0.5,1.0,2.0] ) ,axs=axs[0], toDraw= toDraw)

    self.drawNN('eta', [-3+i*0.1 for i in range(61)] , otherVar=otherVar.clone(e= [e for e in [200., 500, 1000, 2500, 3000, 4000]]  ) ,axs=axs[1], toDraw=toDraw)

    return fig,axs



def checkFake():
    fig, axs = checkNN()
    eMin = 150000
    def eRespFunc(e,eta):
        return 0.96*(1-(eMin)**2/e**2)*(1-eta*eta*0.005)
    def eResolFunc(e,eta):
        #print( 0.04+0.5*eMin/e+eta*eta*0.005)
        return 0.04+0.5*eMin/e+eta*eta*0.005
    
    etaIndex = config.features.index('eta')

    eV, vsEpred, allinputFeaures = getPredictions( 'e', [150000+100000*i for i in range(0,50)] , doRatio=False )
    etaV = np.array( [-4+i*0.2 for i in range(1,41)] )

    hasSig = vsEpred.shape[1]>1

    #ar=scale(array0)
    axs[0][0].plot( eV,  eRespFunc(eV,array0[etaIndex]), color='red')
    axs[0][0].text( 0.6,0.4 , "eta={:1.3f}".format(array0[etaIndex]), transform=axs[0][0].transAxes)
    if hasSig:axs[0][1].plot( eV, eResolFunc(eV,array0[etaIndex]) ,color='red')

    e=10000*np.exp(array0[0])
    axs[1][0].plot( etaV,  eRespFunc(e,etaV), color='red')
    axs[1][0].text( 0.4,0.4 , "e={:1.3e}".format(e), transform=axs[1][0].transAxes)
    if hasSig:axs[1][1].plot( etaV, eResolFunc(e,etaV) ,color='red')
    return fig,axs



@addToClass(Trainer)
def drawLayer(self, li):            
    Dense = keras.layers.core.Dense
    layers = [ l  for l in self.net.layers if isinstance(l,Dense) ]
    l = layers[li]
    w , b = l.get_weights()
    fig=plt.figure('others',constrained_layout=False,figsize=(10,7.8) )
    fig.clf()
    gs=fig.add_gridspec(nrows=1, ncols=4)
    ax0 = fig.add_subplot( gs[0,:-1] )
    ax1 = fig.add_subplot( gs[0,-1] )
    m=ax0.matshow(w.T, aspect='auto', cmap=matplotlib.cm.bwr)
    
    #ax0.colorbar(m)
    ax1.matshow(np.array([b] ).T ,aspect='auto', cmap=matplotlib.cm.bwr)
    fig.colorbar(m,ax=ax0,orientation='horizontal')
    for i in range(len(b)):
        ax1.text(0,i,'{:.2f}'.format(b[i]), ha='center',va='center')
    return m

featuresRanges = ConfigDict( e_var = (100000,5000000),
                             m_var = ( 10000,500000),
                             eta = (-4,4),
                             mu = (0, 60),
                             NPV = (0,40),
                             D2=(0,9),
                             groomMratio=(0,1),
                             effNConst=(0,5),
                             Qw=(5000, 100000),)
@addToClass(Trainer)
def histogramSelection(self, index):
    dataF = self.data[self.config.features]
    data = self.data

    if index is None:
        index =  slice(None,None,None)
    
    f=plt.figure("others",figsize=(10,7.8) )
    plt.clf()

    axs = f.subplots( 3,3)
    print(axs)
    refInput = self.refInput()
    for i, v in enumerate(self.config.features):
        if v=='e_var':
            v0 = self.config.e_transform.inverse(refInput[i])
            values = self.config.e_transform.inverse(dataF[v].iloc[index])
        elif v=='m_var':
            e = self.config.e_transform.inverse(dataF['e_var'].iloc[index])
            e0 = self.config.e_transform.inverse(refInput[0])
            v0 = self.config.m_transform.inverse1(refInput[i],e0)
            values = self.config.m_transform.inverse1(dataF['m_var'].iloc[index],e)
        else:
            v0=refInput[i]
            values = dataF[v].iloc[index]
        print(v,featuresRanges[v] , v0)
        h=axs.flat[i].hist(values , bins=100, range=featuresRanges[v] )
        axs.flat[i].plot( [v0]*2, [0,h[0].max()], '-r', linewidth=2)
        axs.flat[i].set_title(v)
    return h

@addToClass(Trainer)
def extrapolateSelection(self, index):
    dataF = self.data[self.config.features].iloc[index]
    data = self.data

    e_col = data.ni_e if self.config.isNIstep else data.true_e
    

    plt.figure("others",figsize=(10,7.8) )
    plt.clf()
    dstyle = dict( fillstyle='none',  )    
    #plt.scatter(e_col.iloc[index],data.r_e.iloc[index] , **dstyle)
    scaledInput = dataF
    for i,ind in enumerate(index):
        dstyle.pop('color',0)
        p=plt.plot(e_col.iloc[ind],data.r_e.iloc[ind], marker = 's', **dstyle)[0]
        dstyle.update(color=p.get_color())

        ypred = self.predict(scaledInput.iloc[i:i+1].to_numpy() )
        plt.plot(e_col.iloc[ind],ypred, marker='x', **dstyle )

        print( ypred, ' vs ', data.r_e.iloc[ind] )
        #getInputArray()
        x ,y, allinputFeaures = self.getPredictions('e', [150000.+50000.*e for e in range(0,100)], refInput = dataF.iloc[i] ) #.iloc[ind])
        plt.plot(x, y , marker=' ', **dstyle)
    #s=axs[0][0].scatter(e_col.iloc[randind],ypred , color='black',   marker='x')


@addToClass(Trainer)
def buildSelectedIndices(self, selname, vname, value=None, window=0 ):
    refFeatures = self.refInput()
    if vname=='e': vname = 'ni_e' if self.config.isNIstep else 'true_e'
    if vname=='m': vname = 'ni_m' if self.config.isNIstep else 'true_m'
    if value is None:
        i = self.config.features.index(vname)
        value = refFeatures[i]
    if isinstance(value,str):
        ind = np.flatnonzero(self.data.eval(value) )
    else:
        col = self.data[vname]    
        ind = np.flatnonzero(np.abs(col- value ) < window )
    self.selectedIndex[selname] = ind
    return ind

@addToClass(Trainer)
def prepareSelectedInd(self):
    if hasattr(self,'selectedIndex'):
        return 
    self.selectedIndex = ConfigDict()
    ind  = self.selectedIndex
    data = self.data

    self.buildSelectedIndices('eta0', 'eta', 0, 0.1 ) 
    self.buildSelectedIndices('eta2', 'eta', 2, 0.1 ) 

    self.buildSelectedIndices('e200', 'e', 200000, 20000 ) 
    self.buildSelectedIndices('e1000', 'e', 1000000, 50000 ) 

    
    self.buildSelectedIndices('NPV', 'NPV', None, 3 ) 
    self.buildSelectedIndices('mu', 'mu', None, 4 )
    features = self.config.features
    if 'effNConst' in features:
        self.buildSelectedIndices('effNConst', 'effNConst', None, 0.4 )
    if 'groomMratio' in features:
        self.buildSelectedIndices('groomMratio', 'groomMratio', None, 0.3 )
    if 'D2' in features:
        self.buildSelectedIndices('D2', 'D2', None, 1.5 )
    if 'Qw' in features:
        self.buildSelectedIndices('Qw', 'Qw', None, 3000 )


@addToClass(Trainer)
def selectionAtOld(self, **selDic):
    self.prepareSelectedInd()
    finalSel = []
    noRef=selDic.pop('noRef',False)    
    defSel = ['NPV', 'effNConst', 'mu', 'groomMratio',  'D2', 'Qw',]
    if noRef:
        defSel=[]
    for v, sel in selDic.items():
        if 'm'==v:
            m_col = self.data.ni_m if self.config.isNIstep else self.data.true_m
            finalSel.append(np.flatnonzero( np.abs(m_col -  sel)  <  sel/8 ) , )
        elif 'e'==v:
            e_col = self.data.ni_e if self.config.isNIstep else self.data.true_e
            if isinstance(v,str): finalSel.append(self.selectedIndex[sel])
            else:finalSel.append(np.flatnonzero( np.abs(e_col - sel)  <  sel/8 ) , )
        elif v=='eta':
            finalSel.append(self.selectedIndex[sel])
        elif v in defSel:
            defSel.remove(v)
            finalSel.append(self.selectedIndex[sel])
        else:
            finalSel.append(self.selectedIndex[sel])
    for sel in defSel:
        if sel in self.config.features:
            finalSel.append(self.selectedIndex[sel])
    print( 'xxxxxxxx', len(finalSel) )
    from sortednp import kway_intersect
    return kway_intersect(*finalSel)
        

@addToClass(Trainer)
def selectionAt(self, maxN=None, **selDic):
    N = self.gen.current_file.Nentries
    if selDic == {}:        
        return slice(None,N)
    import numexpr as ne
    finalSel = []
    self.gen.format_current_sample(reverse=True)
    
    for v, sel in selDic.items():
        if isinstance(sel, tuple):
            vmin,vmax=sel
        else:
            vmin,vmax= sel*(1-1./8), sel*(1+1./8)
        var = self.gen.chain.allVars[v]
        array = var.array
        finalSel.append( np.flatnonzero( ne.evaluate( "(v >{}) & (v<{})".format(vmin,vmax)  , local_dict=dict(v=array[slice(maxN)]) ) ) )
    from sortednp import kway_intersect
    selectedInd= kway_intersect(*finalSel)

    #if maxN>0 and len(selectedInd)>2000:selectedInd = np.random.choice(selectedInd, 2000, replace=False)
    print("selected = ",len(selectedInd) )
    if selectedInd[-1]==N:
        selectedInd=selectedInd[:-1]
    return selectedInd


@addToClass(Trainer)
def compareDist(self, distList, selDict={}, **histOpts):
    ii = self.selectionAt(**selDict)

    for v in distList:
        ax.hist( v.array[ii], label=v.title, **histOpts )

    ax.legend()
    return ax

@addToClass(Trainer)
def drawVar(self, expr , selDict={}, ploti=None, **histOpts):
    f=plt.figure("others",figsize=(10,7.8) )
    if ploti is None:
        f.clf()        
        ax = f.subplots()
    elif ploti=='same':
        ax=plt.gca()
    elif ploti <2:
        if len(f.axes)<2:
            f.subplots( 1,2)
        ax=f.axes[ploti]
        ax.cla()

    vars = dict(self.gen.chain.allVars)
    if self.last_predictions is not None:
        vars.update( ** self.last_predVars) 
    N = self.gen.current_file.Nentries


    sep =  ':' if ':' in expr else ','
    vL=[vars[v] for v in expr.split(',') ]
    nMin = min( len(v.array) for v in vL)

    ii = self.selectionAt(maxN=nMin, **selDict)
    
    if ':' in expr:
        v1, v2 = vL
        if len(v1.array) != len(v2.array):
            ii = slice(None, min( len(v1.array) , len(v2.array) ))
        ax.hist2d( v1.array[ii], v2.array[ii], **histOpts )
        ax.set(xlabel=histOpts.pop('xlabel', v1.title ), ylabel=histOpts.pop('ylabel', v2.title )) 
    else:
        for v in vL:
            ax.hist( v.array[ii], label=v.title, **histOpts )
    
    ax.legend()

Trainer.last_pred_slice = None
@addToClass(Trainer)
def acquirePredictions(self, limit=None, filei=0):
    if isinstance(limit,int):
        limit = np.s_[:limit]
    elif isinstance(limit,str):
        limit = eval(f'np.s_[{limit}]')
    else:
        limit = np.s_[:] # all

    if self.last_pred_slice == limit:
        return

    self.gen.loadFile(filei)
    x,y = self.gen.current_unfiltered_sample(formatted=True)
    
    pred= self.predict_formatted( x[limit], rescalePreds=True, batch_size=200000, verbose=1)
    x,y = self.gen.current_unfiltered_sample(formatted=False)

    self.last_predictions = pred
    self.last_pred_slice = limit
    self.last_predVars = ConfigDict()
    if isinstance(pred, tuple):
        pred_e, pred_m = np.split(pred[0], self.config.nPredictedParam,axis=1), np.split(pred[1], self.config.nPredictedParam,axis=1) 
    else:
        pred_e, pred_m = np.split(pred, self.config.nPredictedParam,axis=1), ()
    for i,p in enumerate(pred_e):        
        self.last_predVars[f'pred_e{i}'] = Variable(f"pred_e{i}",array=p.flatten(), h_range=(0,2))
    self.last_predVars.cal_r = Variable('cal_r',array=trainer.gen.chain.allVars.r_e.array[limit] * trainer.last_predVars.pred_e0.array)

    for i,p in enumerate(pred_m):        
        self.last_predVars[f'pred_m{i}'] = Variable(f"pred_m{i}",array=p.flatten(), h_range=(0,2))

        
@addToClass(Trainer)
def getScatter(self, etaRange=None, eRange=None, **otherSel):
    selectedInd = self.selectionAt( eta=etaRange, **otherSel)
    print( ' --> ',len(selectedInd) )

    axs = theFig.dfig.axes
    for a in axs.flat:
        a.cla()
    
    hasMass = 'r_m' in self.config.targets  or 'm_reco' in self.config.targets 
    hasE = 'r_e'  in self.config.targets 

    allVars = self.allVariables()
    e   = allVars.e_var.array
    r_e = allVars.r_e.array
    
    x,y = self.gen.current_unfiltered_sample(formatted=False)

    ypred = self.predict( x[selectedInd] )
    print(len(ypred)  )
    #r = 
    dstyle = dict( facecolor='none', edgecolors='grey',   marker='s' )

    respRange , resolRange = defaultRRanges(hasE, hasMass, None, None)    
    self.drawNN( 'e', [150.+100.*i for i in range(0,50)], otherVar=dict(m=massvsE,eta= [0.5*(etaRange[0]+etaRange[1])] ) ,axs=axs[0], respRange=respRange, resolRange=resolRange )
    
    s=axs[0][0].scatter(e[selectedInd], r_e[selectedInd] , **dstyle)
    #print( r_e[selectedInd])
    s=axs[0][0].scatter(e[selectedInd],ypred[:,0] , color='grey',   marker='x')
    #if hasE and hasMass:s=axs[0][1].scatter(e_col.iloc[randind]/1000.,data.r_m.iloc[randind] , **dstyle)

    ## ------
    print("eta plots")
    eRange = eRange or (400,500)
    selectedInd = self.selectionAt( e=eRange,  **otherSel)
    
    dstyle.update(edgecolors='grey')
    ypred = self.predict(x[selectedInd]) 

    eta= allVars.eta.array[selectedInd]

    self.drawNN('eta', [-3+i*0.1 for i in range(61)] , otherVar=dict(m=massvsE,e= [ 0.5*(eRange[0]+eRange[1])]  ) ,axs=axs[1], respRange=respRange, resolRange=resolRange )    
    s=axs[1][0].scatter(eta , r_e[selectedInd] , **dstyle)
    s=axs[1][0].scatter(eta, ypred[:,0] , color='grey',   marker='x')

    
    return s
    
    


@addToClass(Trainer)
def copyWeightsFrom(self, oTrainer):
    net = self.net
    onet = oTrainer.net

    Dense = keras.layers.core.Dense

    layers = [l for l in net.layers if isinstance(l,Dense)]
    olayers = [l for l in onet.layers if isinstance(l,Dense)]

    print(len(layers), len(olayers))
    print(layers, olayers)

    pairLayers = list(zip(layers,olayers))
    
    for l, ol in pairLayers[:-1]:
        l.set_weights(ol.get_weights())
    
    last, olast = pairLayers[-1]

    ow ,ob = olast.get_weights()
    if 'r_m' in self.config.targets:
        last.set_weights( [ow[:,:2] , ob[::2] ] )
    else:
        last.set_weights( [ow[:,:1] , ob[:1] ] )
    
    

@addToClass(Trainer)
def histogramMratio(self,  ):
    x, y = self.gen.current_unfiltered_sample(formatted=False)    
    r = self.gen.chain.allVars.m_var.array / self.gen.chain.allVars.m_true.array
    N = self.gen.current_file.Nentries
    f=plt.figure("others",figsize=(10,7.8) )
    plt.clf()
    ax = f.subplots()
    h=ax.hist(r[:N] , bins=200, range=(0,4) )
    return h


@addToClass(Trainer)
def setScaleFactors(self, vname, scaleFactor=None, offset=None):
    i , scaler = self.varIndexAndScaler(vname)

    # make sure we un-scale
    self.gen.unscale_current_sample()
    if scaleFactor != None: scaler.scaleFactor[i] = scaleFactor
    if offset != None: scaler.offset[i] = offset
    self.gen.scale_current_sample()

@addToClass(Trainer)
def showVariablesScaleFactors(self):
    for i,f in enumerate(self.config.features):
        print( "{}.setScaleParameters(sf={:.3f}, o={:.3f} ) ".format(f, self.inscaler.scaleFactor[i],self.inscaler.offset[i],) )
    print()
    for i,f in enumerate(self.config.originalTargets):
        print( "{}.setScaleParameters(sf={:.3f}, o={:.3f} ) ".format(f, self.outscaler.scaleFactor[i],self.outscaler.offset[i],) )

@addToClass(Trainer)
def simpleHist(self,v, sel=None, weights=None, formatted=False, range=None):
    """ """
    
    # just to make sure 'formatted' is applied.
    x, y = self.gen.current_unfiltered_sample(formatted, directY=True)
    
    allVars = self.gen.chain.allVars
    ardic = dict( (vn,v.array) for (vn,v) in allVars.items() )
    ardic['np']=np
    if sel:
        indices = eval(sel, ardic )
    else:
        indices=np.s_[:]

    if weights != None:
        weights = eval(weights, ardic)
        
    var = allVars[v]
    values = var.array[indices]
    if weights is not None:
        weights = weights[indices]

    range = range or var.h_range
    plt.cla()
    plt.hist( values, bins=300, range=range, weights=weights)
    
        
@addToClass(Trainer)
def histogramInputs(self, formatted=True , varList=None, fnum=-1, tanhOutput=True, useFilters=False):
    """ Draw several histograms of inputs or targets variable. 

    Histos values can be unformatted or formatted (that is : after transformations if any and scaled by trainer.inputscaler) 
    
    """
    if self.gen.current_file is None or self.gen.chain.current_file is None : fnum=0
    if fnum>-1: self.gen.loadFile(fnum)

    if useFilters:
        x, y = self.gen.current_sample(formatted, directY=True)
    else:
        x, y = self.gen.current_unfiltered_sample(formatted, directY=True)
        
    if isinstance(y,tuple):
        y = np.stack( y, 1 )
    
    f=plt.figure("others",figsize=(10,7.8) )
    plt.clf()
    f.set_tight_layout(True)

    hasW = self.gen.current_weight != None
    hOpts = dict( bins=150, range=[-1.2, 1.2] )
    if hasW: hOpts['weights'] = self.gen.chain.weights_var.array[:self.gen.current_file.Nentries]
    
    def getBinning(v):
        if formatted:
            if not tanhOutput and v.name.startswith('r_'):
                return dict( bins=150, range=[0, 2.1] )
            return hOpts
        
        if v.h_range is None:
            if hasW:
                print(" WARNING can't guess histo range for ",v.name)
                hOpts.update(bins=100, range=(0,1.))
            else:hOpts.update(bins='auto')
            return hOpts
        else:
            hOpts.update(bins=v.h_nbins, range=v.h_range )
            return hOpts

    # prepare plotted vars in the form [ (values, var_instance) ]
    plottedVars = []            
    acceptVar = (lambda v : True) if varList is None else (lambda v:v in varList)
    allVars = self.gen.chain.allVars
    print('uuuuu varList ', varList)
    for i,v in enumerate(self.config.features):
        if acceptVar(v):
            print('uuuuu ', v)
            plottedVars.append( (x[:,i], allVars[v] ) )
    for i,v in enumerate(self.config.originalTargets):
        if acceptVar(v):
            plottedVars.append( (y[:,i], allVars[v] ) )
        
    nplot = len(plottedVars)

    (i,j) = 1,1
    while i*j< nplot:
        if i<j: i+=1
        else: j+=1

    axs = f.subplots( i, j, squeeze=False)
    
    print(axs)
    for i, (values, var) in enumerate(plottedVars):
        h=axs.flat[i].hist(values , **getBinning(var) )
        prefix = 'TARGET ' if var.is_target else ''
        axs.flat[i].set(title=prefix+var.title,yscale='log')

    figManager = plt.get_current_fig_manager()
    figManager.window.showMaximized()

    
    return h


@addToClass(Trainer)
def savehistogramInnputs(self, formatted=True , ):
    from matplotlib.backends.backend_pdf import PdfPages
    nfile = len(trainer.gen.chain.file_helpers)
    with PdfPages("allInputs.pdf") as pdf:
        for i in range(nfile):
            self.histogramInputs( formatted, fnum=i)
            f=plt.figure("others")
            pdf.savefig(f)


def scaleOuputLayer(s):
    lastl=trainer.net.layessrs[-1]
    wL=lastl.get_weights()
    wL[0][:,0] *= s
    wL[1][0] *= s
    lastl.set_weights(wL)


def _evaluate2(model: Model, layer, x, y=None):
    #functor = K.function([model.input, K.learning_phase()], [layer.output])
    #functor = K.function([model.input, K.learning_phase()], [layer.output])
    functor = K.function([model.input ], [layer.output])
    return functor([x,1])
    

def _evaluate(model: Model, nodes_to_evaluate, x, y=None):
    if not model._is_compiled:
        if model.name in ['vgg16', 'vgg19', 'inception_v3', 'inception_resnet_v2', 'mobilenet_v2', 'mobilenetv2']:
            print('Transfer learning detected. Model will be compiled with ("categorical_crossentropy", "adam").')
            print('If you want to change the default behaviour, then do in python:')
            print('model.name = ""')
            print('Then compile your model with whatever loss you want: https://keras.io/models/model/#compile.')
            print('If you want to get rid of this message, add this line before calling keract:')
            print('model.compile(loss="categorical_crossentropy", optimizer="adam")')
            model.compile(loss='categorical_crossentropy', optimizer='adam')
        else:
            print('Please compile your model first! https://keras.io/models/model/#compile.')
            print('If you only care about the activations (outputs of the layers), '
                  'then just compile your model like that:')
            print('model.compile(loss="mse", optimizer="adam")')
            raise Exception('Compilation of the model required.')
    symb_inputs = (model._feed_inputs + model._feed_targets + model._feed_sample_weights)
    f = K.function(symb_inputs, nodes_to_evaluate)
    x_, y_, sample_weight_ = model._standardize_user_data(x, y)
    return f(x_ + y_ + sample_weight_)    
from collections import OrderedDict
def get_activations(model, x, layer_name=None):
    """
    Get output activations for all filters for each layer
    :param model: keras compiled model or one of ['vgg16', 'vgg19', 'inception_v3', 'inception_resnet_v2', 'mobilenet_v2', 'mobilenetv2']
    :param x: input for which activations are sought (can be a batch input)
    :param layer_name: if activations of a particular layer are sought
    :return: dict mapping layers to corresponding activations (batch_size, output_h, output_w, num_filters)
    """
    nodes = [layer.output for layer in model.layers if layer.name == layer_name or layer_name is None]
    # we process the placeholders later (Inputs node in Keras). Because there's a bug in Tensorflow.
    input_layer_outputs, layer_outputs = [], []
    [input_layer_outputs.append(node) if 'input_' in node.name else layer_outputs.append(node) for node in nodes]
    #activations = _evaluate(model, layer_outputs, x, y=None)
    print(x)
    layer = [l for l in trainer.net.layers if l.name == layer_name][0]    
    activations = _evaluate(model, layer, x, y=None)
    return activations[0]
    activations_dict = OrderedDict(zip([output.name for output in layer_outputs], activations))
    activations_inputs_dict = OrderedDict(zip([output.name for output in input_layer_outputs], x))
    result = activations_inputs_dict.copy()
    result.update(activations_dict)
    return result


def get_activation2(model, x, layer_name):
    tmpM= Model(inputs=model.input,
                outputs=model.get_layer(layer_name).output)
    return tmpM.predict(x)

def draw_activations(n=2000):
    #inputs = trainer.scaler.transform(trainer.X_train.iloc[0:n])
    inputs = trainer.inscaler.scale(trainer.X_train.iloc[0:n])
    acts=get_activations(trainer.net, inputs, layer_name='attention_vec')
    
    plt.figure("others")
    plt.clf()
    plt.bar( np.arange(len(trainer.config.features)), acts.sum(axis=0),
             tick_label=config.features,
    )
    plt.show()

def draw_activationsHist(n=2000):
    #inputs = trainer.inscaler.transform(trainer.X_train.iloc[0:n])
    inputs = trainer.inscaler.scale(trainer.X_train.iloc[0:n].to_numpy())
    acts=get_activations(trainer.net, inputs, layer_name='attention_vec')

    fig=plt.figure("others")
    plt.clf()

    nf = len(trainer.config.features)
    subplots = { 1:1 , 2:2, 3:(2,2),4:(2,2), 5:(3,2),6:(3,2), 7:(3,3), 8:(3,3), 9:(3,3)}[nf]
    axs=fig.subplots( *subplots)
    
    for i, ax in enumerate(axs.flat):
        if i==nf: break
        ax.hist( acts[:,i], bins='auto')
        ax.set(title=trainer.config.features[i])

def draw_activationsInCorr(v,n=20000):
    #inputs = trainer.inscaler.transform(trainer.X_train.iloc[0:n])
    inputs = trainer.inscaler.scale(trainer.X_train.iloc[0:n]).to_numpy()
    acts=get_activations(trainer.net, inputs, layer_name='attention_vec')

    fig=plt.figure("others")
    plt.clf()

    features =trainer.config.features
    i = features.index(v)
    
    ax=fig.subplots( 1 )
    ax.hist2d(trainer.X_train.iloc[:n,i] , acts[:,i], bins=60)    
    ax.set(title='activation vs '+features[i])

def drawLayerOutputs(inputSpec=100 , layer_name='dense_4', vsVarIndex=None, f=1.):

    ind = ()
    if isinstance(inputSpec,int):
        n=inputSpec
        raw_inputs = trainer.X_train.iloc[0:n].to_numpy()
        inputs = trainer.inscale.scale(raw_inputs)
        if vsVarIndex != None:
            ind = np.argsort(raw_inputs[:,vsVarIndex])
            if vsVarIndex==0: x = 10*np.exp(raw_inputs[:,0])[ind]
            else: x = raw_inputs[:,vsVarIndex]
        else:
            x = np.array(range(n))

    else:
        var, values = inputSpec
        x , inputs = getInputArray(var, values, otherVar=dict(eta=0))
        
        
    layer = [l for l in trainer.net.layers if l.name == layer_name][0]
    layerOuts= _evaluate2(trainer.net, layer, inputs)[0]
    #layerOuts = trainer.net.predict(inputs)
    #print(layerOuts)
           
           #return layerOuts
    #acts=_activations(trainer.net, inputs, layer_name='dense_4')
    fig=plt.figure("others")
    plt.clf()
    ax=fig.subplots( 1 )
    ax.grid()

    for i, out in enumerate(layerOuts.T):
        print(x, out[ind])
        ax.plot(x, f*out[ind]+i, label=str(i))
    #ax.legend()
    return layerOuts
trainer.redraw = True
#trainer_raw.checkNN(predictionsFunc=trainer_raw.getPredictionsNI, marker='', clear=False)

def scatterEta0(data, Y, evar):

    centralInd = np.where(np.abs(data.eta)<0.1)[0]
    randind = np.random.randint(len(centralInd), size=2000)
    centralInd = centralInd[randind]
    dataC = data.iloc[centralInd]
    YC = Y.iloc[centralInd]
    print (YC.shape, dataC.shape)
    plt.clf()
    plt.scatter(dataC[evar], YC)



def dataCorrelation(filter=None):
    if filter is None:
        index = slice(None,None,None)
    else:
        index = np.flatnonzero(trainer.data.eval(filter))
    fig=plt.figure("others")
    plt.clf()
    ax=fig.subplots( 1 )
    if filter: ax.set_title(filter)
    labels = trainer.config.features+['r_e','r_m']
    corrData = trainer.data[labels].iloc[index]
    corr = corrData.corr()
    print(len(labels), corr.shape)
    i=ax.matshow(corr)
    i.set_cmap(matplotlib.cm.bwr)
    i.set_clim(-1,1)
    n = np.arange(len(labels))
    ax.set_xticks( n ); ax.set_xticklabels( labels )
    ax.set_yticks( n ); ax.set_yticklabels( labels )
    #ax.set_yticklabels(labels)    
    return fig.colorbar(i)


class PrintToSr:
    def __init__(self):
        self.s=''
    def __call__(self, *args):
        print(args)
        self.s = self.s+' '.join(args)+'\n'
        

@addToClass(Trainer)
def savePDFsample(self,tag='',text=''):
    from matplotlib.backends.backend_pdf import PdfPages
    config = self.config
    tag = tag +'_'.join(config.originalTargets)
    from os.path import basename, join
    outdir = join('HistoAnalysis',basename(trainer.outName()).replace('.h5',''))
    os.makedirs( outdir, exist_ok=True)
    outname = join( outdir, 'nnPredictions_'+tag+'.pdf')
    with PdfPages(outname) as pdf:
        toS = PrintToSr()
        trainer.net.summary(print_fn=toS)
        toS(text)
        ofig,axs=theFig.get('ofig', shape=(1,))
        ofig.text( 0.1 , 0.1, toS.s, transform=ofig.transFigure, wrap=False, family='monospace'  )
        pdf.savefig(ofig)
        self.checkNN(otherVar=dict(), respRange=(0.7,1.))
        pdf.savefig(theFig.dfig,dpi=96)
        self.checkNN(otherVar=dict(m=40),respRange=(0.7,1.))
        pdf.savefig(theFig.dfig,dpi=128)
        self.checkNN(otherVar=dict(m=80),respRange=(0.7,1.))
        pdf.savefig(theFig.dfig,dpi=256)

        ofig.clf()
        mrange=[10.+5.*i for i in range(40)]
        trainer.drawNN('m', mrange  , otherVar=dict(eta=0, e=[200,500,1000,2000]) ,respRange=(0.8,1.3, 0.6,1.4),resolRange=(0.,0.2,0,0.6))
        pdf.savefig(ofig)
        trainer.drawNN('m', mrange  , otherVar=dict(eta=0.6, e=[200,500,1000,2000]) ,respRange=(0.8,1.3,0.6,1.4),resolRange=(0.,0.2,0,0.6))
        pdf.savefig(ofig)

        for v in self.config.features:
            if v in ('e_var','m_var', 'eta'): continue
            var = self.allVariables()[v]
            values = np.linspace(var.h_range[0], var.h_range[1], 60)#[:50]
            trainer.drawNN(v, values  , otherVar=dict(eta=0., e=[200,500,1000,2000]) ,respRange=(0.8,1.3,0.6,1.4),resolRange=(0.,0.2,0,0.6))
            pdf.savefig(ofig)
            trainer.drawNN(v, values  , otherVar=dict(eta=0.6, e=[200,500,1000,2000]) ,respRange=(0.8,1.3,0.6,1.4),resolRange=(0.,0.2,0,0.6))
            pdf.savefig(ofig)

        
        ofig, ax = theFig.get('ofig', (1,) )
        self.loglossnseen.draw(norm=False, plotMetrics=False,  marker='.',ls='', ax=ax[0])
        pdf.savefig(ofig)
        
        # npvs=[i for i in range(60)]
        # trainer.drawNN('mu', npvs  , otherVar=dict(eta=0., e=[200,500,1000], m=[40, 80]) ,respRange=(0.8,1.1,0.6,1.4), )
        # pdf.savefig(ofig)
        # print ("saved in ", outname)
    print("\nsaved as ",outname)



def atEta(eta):
    m=trainer.inscaler.mean.copy()
    m[0]=eta
    return trainer.inscaler.scale(m).reshape( (1,len(trainer.config.features) ) )


from JetCalibDNN.ModelDefinitions  import EtaAnnotBase

def testAnnotAll():
    x=np.stack([np.linspace(-4.3,4.3,200)]*14,1)
    plt.clf()
    f=plt.plot(x[:,0],get_activation2(trainer.net, x/EtaAnnotBase.eta_scale, "Eta_GausAnnot")  )
    return f

def testAnnot(i, lname="Eta_GausAnnot", clear=True):
    x=np.stack([np.linspace(-4,4,200)]*14,1)
    if clear: plt.clf()
    f=plt.plot(x[:,i],get_activation2(trainer.net, x/EtaAnnotBase.eta_scale, lname) [:,i], )
    return f

def testEAnnot(lname="E_GausAnnot",i=1):
    x=np.stack([np.linspace(0,300,200)]*14,1)
    plt.clf()
    a,b = trainer.inscaler.scale_parameters(-1)
    y = get_activation2(trainer.net, x*a+b, lname) [:,0]
    f=plt.plot(x[:,1],y )
    return f


def testEtaLinC(doScale=False):
    x=np.stack([np.linspace(-1.1,1.1,200)]*17,1)
    l=trainer.net.layers[-4]
    #exec(open('plotAndDebugDNN.py').read()) ; 
    plt.clf()
    if doScale:
        etaS = ModelDefinitions.LayerDeepTailEtaC.eta_scale
        yS = ModelDefinitions.LayerDeepTailEtaC.r_e_scale
        return plt.plot(etaS*x,l.test(x)[:,0])
    else:
        return plt.plot(x,l.test(x)[:,0])        

def testOutScaler( out , scaleF ):
    l = trainer.net.layers[-2]
    out = np.stack ([ out, np.zeros(len(out))] , 1 )
    #scaleF = scaleF*np.ones( len(out) )
    scaled = trainer.outscaler.unscale( l.test( out, scaleF ) )
    unscaled = trainer.outscaler.unscale(  out  )
    print('scaled ', scaled)
    return scaled / unscaled

def solveExpCoef(x0,y0,x1,y1):
    c = np.log( y0 /y1) / (x0-x1)
    a = -y0/np.exp(c*x0)
    return a,c


def my_model_plot(model,to_file,rankdir='TB', dpi=96, show_layer_names=False,  show_layer_class=False, only_dense=False, ):
    """Draw a graph of the model using the dot language. Inspired/recopied from keras.utils.plot_model """
    from tensorflow.python.keras.layers import wrappers
    from tensorflow.python.keras.engine import sequential
    #$from tensorflow.python.keras.engine import network
    from tensorflow.python.util import nest
    import pydot
    
    dot = pydot.Dot( )
    dot.set('rankdir', rankdir)
    dot.set('concentrate', True)
    dot.set('dpi', dpi)
    dot.set('ranksep', 0.04)
    #dot.set_node_defaults(shape='record')
    dot.set_node_defaults(shape='box')
  
    sub_n_first_node = {}
    sub_n_last_node = {}
    sub_w_first_node = {}
    sub_w_last_node = {}
  
    if isinstance(model, sequential.Sequential):
      if not model.built:
        model.build()
    #layers = model._layers
    layers = model.layers

    def add_edge(dot, src, dst):
        if not dot.get_edge(src, dst):
            #dot.add_edge(pydot.Edge(src, dst,style='invis'))
            dot.add_edge(pydot.Edge(src, dst))
            
    annot_layer = None
    setW = rankdir=='TB'
    displayedLayer = set()
    # Create graph nodes.
    for i, layer in enumerate(layers):
        layer_id = str(id(layer))
      
        # Append a wrapped layer's label to node's label, if it exists.
        layer_name = layer.name
        class_name = layer.__class__.__name__
    
        # Create node's label.
        if show_layer_names and show_layer_class:
            label = ['{}: {}'.format(layer_name, class_name)]
        elif show_layer_class:
            label = [class_name]
        elif show_layer_names:
            if 'dense' in layer_name:
                label=[]
            else:
                label = [layer_name]
        else:
            label=[]
                
        def layer_size(shape):
            if isinstance(shape,list):
                return shape[0][1]
            if len(shape)<2:
                return 0
            return shape[1]

        #     # label = '%s\n|{input:|output:}|{{%s}|{%s}}' % (label,
        #     #                                                inputlabels,
        #     #                                                outputlabels)
        #     label = '%s\n|{out : %s}' % (label,
        #                                outputlabels.replace('?','_'))

        print(layer_name, class_name,layer_size(layer.output_shape))

        # if only_dense and class_name not in ["InputLayer", "Dense"]:
        #     continue
        if class_name in ['Dropout', 'Activation', "Concatenate", "DuplicateEntry", "Multiply"]:
            continue
        displayedLayer.add(layer_id)
        
        
        ls = layer_size(layer.output_shape)
        w = 0.75+0.005*ls
        label = ' '.join(label+ [str(ls)] )
        args = dict(label=label)
        if setW: args['width']=w
        else : args.update(height=w, orientation=90, width=0.1)

        if i==0:
            args.update(color='blue', fontcolor='blue')

            
        node = pydot.Node(layer_id, **args)
        dot.add_node(node)

        if 'Annot' in class_name:
            print( 'AAAAAAAAAAa ',i, ls)
            annot_layer = node
        
    def firstDisplayed(layer):
        layer_id = str(id(layer))
        if layer_id in displayedLayer:
            return layer
        for node in layer._inbound_nodes:
            for inbound_layer in nest.flatten(node.inbound_layers):
                return firstDisplayed(inbound_layer)            

    # Connect nodes with edges.
    for layer in layers:
        layer_id = str(id(layer))
        if not layer_id in displayedLayer:
            continue
        for i, node in enumerate(layer._inbound_nodes):
            #node_key = layer.name + '_ib-' + str(i)
            for inbound_layer in nest.flatten(node.inbound_layers):
                inbound_layer = firstDisplayed(inbound_layer)
                inbound_layer_id = str(id(inbound_layer))
                assert dot.get_node(inbound_layer_id)
                assert dot.get_node(layer_id)
                add_edge(dot, inbound_layer_id, layer_id)

    # if annot_layer :
    #     n=pydot.Node(2,label='eta annotation', shape='plaintext', pos="300,300?")
    #     dot.add_node(n)
    #     dot.add_edge(pydot.Edge(n, annot_layer))

    from os.path import splitext
    _, extension = splitext(to_file)
    if not extension:
        extension = 'png'
        to_file=to_file+'.png'
    else:
        extension = extension[1:]                
    dot.write(to_file, format=extension)
    return dot


def saveHistos(v='e_reco'):
    import h5py
    var=trainer.allVariables()[v]
    allH =[]
    nf = len(trainer.gen.input_files)
    vindex, _ = trainer.varIndexAndScaler(v)
    for i in range(nf):
        trainer.gen.loadFile(i,noTransform=True)
        N = trainer.gen.current_file.Nentries
        print(i, ' fname = ',trainer.gen.current_file.fname)
        values = trainer.gen.x_slice_unfiltered()[:N,vindex]
        r,edge = np.histogram(values, 200, var.h_range )
        allH.append(r)

    f=h5py.File(v+'_histos.h5','w')
    f['edge']=edge
    for i in range(nf):
        f[f'h{i}'] = allH[i]
    f.close()

def loadAndPlotHistos(v='e_reco'):
    import h5py
    f=h5py.File(v+'_histos.h5')
    var=trainer.allVariables()[v]
    edges = f['edge']
    from JetCalibDNN.HistoAnalysis import BinnedArraysGraphics as bag 
    page = bag.buildPage('pdf', 1, f'{v}_histos.pdf')
    ax=page.figure.gca()
    
    for k in f.keys():
        if k=='edge': continue
        ax.hist( edges[:-1],edges, weights=f[k] )
        ax=page.nextAx()
    page.close()
    return page

def prepareDrawHist(isNIstep=False):

    N = trainer.gen.current_file.Nentries
    if trainer.last_predictions is None:
        pred=trainer.fullPredictions()
        trainer.gen.format_current_sample(reverse=True)
    pred = trainer.last_predictions
    V = trainer.gen.chain.allVars
    if not isNIstep:
        if 'm_true' in trainer.config.targets:
            V.m_ni=Variable('m_ni',array=pred[:,2])
        else:
            V.m_ni=Variable('m_ni',array=pred[:,2]*V.m_var.array[:N])
    else:
        if 'r_m' in trainer.config.targets:
            V.r_pred_e=Variable('r_pred_e',array=pred[:,0])
            V.r_pred_m=Variable('r_pred_m',array=pred[:,1])
            V.mse_e=Variable('mse_e',array=(pred[:,0]-V.r_e.array)**2)
    

def addCurvesToPlot( fname , ploti=0, **plotargs):
    cfile=np.load(fname)
    f=plt.figure("others",figsize=(10,7.8) )
    plotargs.setdefault('color' , 'black')
    for an, xy in cfile.items():
        f.axes[ploti].plot( xy[0], xy[1] , **plotargs)


def saveEMHisto( fname, bins, hrange):
    from Variables import Histo2D
    x,y=trainer.gen.current_unfiltered_sample(formatted=False)
    e=trainer.gen.chain.allVars.e_var.array
    m=trainer.gen.chain.allVars.m_var.array
    h = Histo2D( bins, hrange, x=e,y=m)
    #h.hcontent[1:-1,:] += h.hcontent[:-2,:]  +h.hcontent[2:,:]
    #h.hcontent[1:-1,:] /=3.
    # normalize on E :
    #h.hcontent /= h.hcontent.sum(1).reshape( (bins[0],1) )    
    #np.nan_to_num(h.hcontent, copy=False)

    h.hcontent *= bins[1]/(np.count_nonzero(h.hcontent,1).reshape( (bins[0],1) ) )
    h.hcontent *= bins[0]/(np.count_nonzero(h.hcontent,0) )
    np.nan_to_num(h.hcontent, copy=False)
    h.save(fname)
    
    return h

def saveEaMHisto( fname, bins, hrange):
    from Variables import Histo2D, Histo1D
    x,y=trainer.gen.current_unfiltered_sample(formatted=False)
    e=trainer.gen.chain.allVars.e_var.array
    m=trainer.gen.chain.allVars.m_var.array
    hE = Histo1D( bins[0], hrange[0], x=m, name='h1')
    hM = Histo1D( bins[1], hrange[1], x=e, w=1./hE.toHContent(m), name='h2')
    #h.hcontent[1:-1,:] += h.hcontent[:-2,:]  +h.hcontent[2:,:]
    #h.hcontent[1:-1,:] /=3.
    d = hE.arrayDict()
    d.update(hM.arrayDict() )
    np.savez(fname, **d )
    return hE,hM

def saveEHisto( fname, bins, hrange):
    from JetCalibDNN.HistoUtils import Histo1D
    x,y=trainer.gen.current_unfiltered_sample(formatted=False)
    e=trainer.gen.chain.allVars.e_true.array
    hE = Histo1D( hrange=(bins,hrange) , x=e, name='h1')
    hE.save(fname)
    return hE

# ex: h=saveEHisto('hE.npz', 500, (100, 6000) )

def savelogEHisto( fname, bins, hrange):
    from Variables import Histo2D, Histo1D
    x,y=trainer.gen.current_unfiltered_sample(formatted=False)
    e=np.log(trainer.gen.chain.allVars.e_var.array)
    hE = Histo1D( bins=bins, hrange=hrange,x=e,   name='h1')
    hE.save(fname)
    return hE

def savelogMoEHisto( fname='hLogMoE', bins=500, hrange=(-9,-0.2) ):
    from Variables import Histo2D, Histo1D
    x,y=trainer.gen.current_unfiltered_sample(formatted=False)
    v=trainer.gen.chain.allVars.LogMoE.array
    hE = Histo1D( bins=bins, hrange=hrange,x=v,   name='h1')
    hE.save(fname)
    return hE
# ex: h=saveEHisto('hLogMoE.npz', 500, (100, 6000) )




from types import SimpleNamespace
if 'mlayers' not in dir():
    mlayers = SimpleNamespace()

def getModelLayer(i):
    m = getattr(mlayers,"m"+str(i), None)
    if m is not None: return m
    m = keras.models.Model(inputs= trainer.net.layers[0].input, outputs=trainer.net.layers[i].output)
    
    setattr(mlayers, "m"+str(i), m)
    return m


def _drawActivations(i,vsVar, vvalues, otherVar={}, refInput=None, sortInd=None, plotType='matrix'):

    model = getModelLayer(i)

    
    xvalues , inputs = trainer.getInputArray(vsVar, vvalues, otherVar, refInput=refInput)

    inputs = trainer.gen.format_inputs( inputs )
    pred=model.predict( inputs ).T

    if sortInd is not None:
        pred = pred[ pred[:,sortInd].argsort() ]
    
    fig=plt.figure('others',constrained_layout=False,figsize=(10,7.8) )
    fig.clf()
    ax = fig.subplots()
    if plotType=='matrix':
        m=ax.matshow(pred  ,aspect='auto', cmap=matplotlib.cm.bwr)
        fig.colorbar(m,ax=ax)#,orientation='horizontal')
    elif plotType=='ranges':
        r = (pred.max(axis=1)-pred.min(axis=1))/pred.std()
        ax.plot(  r , ls='',marker='.')
        
    return pred

def drawActivations(i,vsVar, vvalues, otherVar={}, refInput=None, sortInd=None):
    return _drawActivations(i,vsVar, vvalues, otherVar, refInput, sortInd, plotType='matrix')

def drawActivationRanges(i,vsVar, vvalues, otherVar={}, refInput=None, sortInd=None):
    return _drawActivations(i,vsVar, vvalues, otherVar, refInput, sortInd, plotType='ranges')



    
def compareWeights(t1,t2, lname ):
    l1 = t1.net.get_layer(lname)
    l2 = t2.net.get_layer(lname)
    wL1 = l1.get_weights()
    wL2 = l2.get_weights()

    for a1,a2 in zip(wL1, wL2):
        print( 'differing : ', (a1!=a2).any() )
    return



def buildTransformLayer(trainer):
    all_vars = trainer.gen.all_vars
    features = trainer.config.features
    sourceVars = [ v for v in all_vars if v.from_src]
    featureVars = [ trainer.gen.varDict[f]  for f in features ]

    # transforms

    # filtering ???
    
    # scalers
    

def gradients(trainer, x, y):
    model = trainer.net
    y=tf.convert_to_tensor(y, dtype=tf.float32)
    with tf.GradientTape() as g:
        # here we use test data to calculate the gradients
        
        y_pred = model(x)  # forward-propagation
        loss = model.compiled_loss(y_true=y, y_pred=y_pred)  # calculate loss
        gradients = g.gradient(loss, model.trainable_weights)  # back-propagation

        return gradients
        # # In eager mode, grads does not have name, so we get names from model.trainable_weights
        # for weights, grads in zip(self.model.trainable_weights, gradients):
        #     tf.summary.histogram(
        #         weights.name.replace(':', '_') + '_grads', data=grads, step=epoch)

        # writer.flush()


