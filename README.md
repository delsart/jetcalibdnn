Calibration of jet E and mass with   DNN
===========================================================

This package contains python code to train DNN to perform a MC-based simultaneous Energy and mass calibration of jets.


It allows to use 2 approaches :  Generalized Numerical Inversion (GenNI)[ATL-PHYS-PUB-2020-001](https://cds.cern.ch/record/2706189) or "direct" calibration.


The principle of GenNI technique is  to train 2 NN : 

  * the 1st NN learns $r_{ni} = mode(X_{reco}/X_{true})$ as a function of ($X_{true}, \theta$) (where $X$ is E, mass or both and $\theta$ represent a set of other parameters). With this NN, a new input variale is built : $X_{ni} = r_{ni}X_{true}$ 
  * the 2nd NN learns $r_ni$ as a function of $(X_{ni},\theta)$. 
  * finally, the calibration is given by $X_{calib} = X_{reco} / NN2(X_{reco},\theta)$ :  the 2nd NN gives the prediction of the of the calibration factor. 
By using 2 steps, the derivation of the calibration avoids direct dependence on the distribution of the truth (MC) quantities.


The package also allows to derive a direct calibration, using only 1 NN in a much simpler approach :

  * NN learns $r_{reco} = mode(X_{reco}/X_{true})$ as a function of ($X_{reco}, \theta$)
  * The calibration is given by $X_{calib} = X_{reco} / NN(X_{reco},\theta)$ 

In practice, the derived corrections work better with the direct calibration, and the derived calibration does not seem to be biaised significantly by underlying MC distrubtutions

Dependencies
------------------

The code here uses tensorflow with the keras API. It depends on
```
tensorflow # version 2.2 or above
tensorflow_addons
numpy
h5py
uproot  # version 4.1.7 or above
numexpr
```

Setup
------------

A clean way of  using this package would be to follow this instructions :
```bash
git clone https://gitlab.cern.ch/delsart/jetcalibdnn.git JetCalibDNN

# create an independent directory to run the scripts  
mkdir run/
cd run/
# do this only once per run/ directory :
source ../JetCalibDNN/trainscripts/setupLinks.sh
# or source ../JetCalibDNN/trainscripts/clustertraining/setupLinks.sh

# do this on each login :
source setupPATH.sh
```

Inputs
--------

This package is set up to process many hundred millions of jets. As
the corresponding data may not be able to fit in memory, the approach
is to stream the data from the input files, loading ~10M at a time.

The data is provided to keras through a custom keras.Sequence.
The input files are expected to be flat root ntuples (1 entry per jet)
and are read in by a dedicated helper class
(`GeneratorFromRootFile.TreeChain`) using the uproot module.

In order to create the flat ntuples from regular ntuples (or even
DAOD), utilities scripts in inputs_utilities/ are available.


Code organisation
-----------------

`Variables.py` define the base `Variable` class which is used to gather information about an input variable. The information includes : name in input file, how to transform the variable (ex: take the log(E) or build a ratio from 2 other variables), how to filter events according to it, how to scale the variable before feeding to the NN, etc... The file contains the list of known `Variable` instance to be used.

`ROOTDataGenerator.py` contains code related to reading the input data and feeding it to the NN.  The `ROOTDataGenerator` class is a  custom keras.Sequence implementing the reading of the variables, performing filtering and preprocessing (according to the list of `Variable` it holds), and providing the training batches to tensorflow. In practice we use some derived classes dedicated to 1 (only JES calib) or 2 targets (JES and JMS calib) and enabling Multi-Threaded loading (eg `Generator2TargetMT`)

`ModelDefinitions.py` defines classes which builds keras.Model. The model builders are organized this way to ease the definition of many models, varying only some part of it (head, core, tail) and automatically providing identification tags for bookkeeping.  

`NNutils.py` defines customs loss functions, normalizers for inputs and targets, and custom keras.Layer such as the `GausAnnotation`.  

`ConfigUtils.py` defines the specialized dictionnary used to store all options `ConfigDict`. Also defines some default values for the configuration.

`Trainer.py` defines the `Trainer` class. This is the top-level object bringing all the above pieces together and from which we can prepare the data, run training, save or load model weights, create numerically inversed data or calibrated data.

### scripts

`trainscripts/jetCalibDNN.py` example of a main script  setting up a custom configuration, instantiating a `Trainer` object and defining function to run a training sequence. This is a rather generic script, and it expects it is invoked together with a second script dedicated to specifics of the current training (ex : input files and variables for training a specific jet collection)

`trainscripts/setupAntiKt4EMPFlowJets.py` example of a specific script to be invoked together with `jetCalibDNN.py` (see below for an example).

`plotAndDebugDNN.py` methods for drawing graphics and tests. Many
methods are defined as additions to the `Trainer` class.

Training
------
The package can be run both interactively (strongly recommended when testing/developping) or in batch systems.

First define a top-level python script. This script will  (a generic example is `jetCalibDNN.py`) :
  * define a configuration (typically by calling `conf=defaultConfig.clone( ... )`)
  * intantiate a `Trainer` object
  * define a function specifying a training sequence (that is, a sequence of call to `trainer.refit(..)` which is just a wrapper around  `keras.Model.fit( )` )


Then the starting point is the  `Trainer.train()` method. It is called as `trainer.train(config, defaultSequence, reload)` where
 * `config` is a configuration dict, the one defined in the top-level script.
 * `defaultSequence` is the function taking a `Trainer` as argument and implementing the training calls. (see `testSequence()` in  `jetCalibDNN.py` as a minimal example)
 * `reload` is False if we want to train a NN from scratch and True if we only want to reload an existing NN. In the later case, the NN is read from a file which name is built according to `config`.


One key configuration entry is `config.mode` which must be in the form 'XX:YY' where
 * XX describes the input type and is either 'true' or 'ni' or 'reco' (in practice we always use 'reco', since numerical inversion isn't working well with DNN)
 * YY decribes if we're going to train or build predictions for the full dataset, and is either 'train' or 'predict'

Then a typicall call looks like :

`trainer.train( conf.update( mode = 'reco:train'), defaultSequence=testSequence, reload=0)`

We use the `config.update()` call to update on the fly the config defined in the top-level script.

Here are some example in interactive mode, assuming the setup described above has been executed.


### Training with direct calib ###
This approach seems to give the best performance. 

We use 'reco' as the input mode and only perform 1 training :
```python
# Launch python with : python3 -i jetCalibDNN.py setupA10CSSKUFOsoftDrop.py 
#  (that is when python gives us the prompt it would have executed the 2 above scripts)

# start training from scratch : 

trainer.train(conf.update( nInputFiles=-1,  modelBuilder=ModelDefinitions.HeadEtaGETCore1Tail1(), mode='reco:train'  ), reload=0 , fitSequence=defaultSequence)

# Save the state of the model :
trainer.save_model()
```

  * `conf` is the default config defined in this script. In the above line it is passed to train() after being updated on some of its content. This allows to customize the training on-the-fly (i.e without updating the conf in the jetCalibDNN.py script)
  *  `modelBuilder` is set to a `keras.Model` building instance.
  *  `fitSequence` is a function defining several calls to trainer.refit() , see below for examples.

`trainer.refit()` is just a wrapper around `keras.Model.fit` which
allows to easily re-start the training with updated loss function or
alternate sampling weights or alternate input filtering.
Thus, if necessary, we can perform more training steps, calling refit again :

```python
# continue training for 2 epochs, setting the optimizer, the loss functions (here 1 for the energy, 1 for the mass), and the relative weights for each loss function
trainer.refit(batch_size=105000, optimizer=diffgrad(1e-5), nepoch=2, loss=[utils.truncmdnLoss(1.),utils.truncmdnLoss(1.)], partition=None,loss_weights=[1.,1.], metrics=[utils.lgkLoss(1e-3,1e-3)], sample_weight=None)

# then save again :
trainer.save_model()
```


Finally, it is possible to calculate the calibrated E and/or mass at each event and save them into outprut file  :
```python
# reload the NN (setting reload=1) :
trainer.train(conf.update( nInputFiles=-1,  modelBuilder=ModelDefinitions.HeadEtaGETCore1Tail1(), mode='reco:predict  ), reload=1 , fitSequence=None)
# build the calibrated quantities :
trainer.createCalibDataset()
```
Alternatively, instead of calling `trainer.createCalibDataset()` and
creating new ntuple of calibrated mass and e, we can choose to
directly produce response histograms in a binned phasespace (typically
by bins of E,m and eta) in order to compare with ATLAS standard calib
and draw the usual response & resolution curves. See below.


### Training with Numerical Inversion ###

```
### Training. 1st NN : learn the response distribution mode
 trainer.train(conf.update( targets=['r_e', 'r_m'   ],  outputDir='', nInputFiles=-1, modelBuilder=ModelDefinitions.HeadEtaGETCore1Tail1(), mode='true:train'  ) , fitSequence=defaultSequence)
# Save the state of the model :
trainer.save_model()
```


Once a proper loss is reach and the model saved, we can build the NI
input. Start a new session and reload the NN :
```python
### Build NI inputs
# reload : 
trainer.train(conf.update( targets=['r_e', 'r_m'   ],  outputDir='', nInputFiles=-1, modelBuilder=ModelDefinitions.HeadEtaGETCore1Tail1(), mode='true:predict'  ), reload=1, fitSequence=defaultSequence)
# then build the NI inverse e and m and save them in root files :
trainer.createNIDataSet()
```
The call to `Trainer.train` is identical as above, except
  1. 'reload' option is set to 1 (which evaluates to True). The name of the NN to load is build according to `conf` (which is obtained by calling `trainer.outName()`) )
  2. 'mode' is set to 'true:predict'

The result is the creation of new ROOT files containing the numerically
inversed quantities (typically : r_e_ni, e_ni, r_m_ni, m_ni).
The new ROOT files are named by prefixing the input files by 'ni_'+trainer.fullTag().


```python
#### Training. 2nd NN : learn again the mode(response) but now  as function of NI quantities.
trainer.train(conf.update(targets=['r_e', 'r_m'   ],  outputDir='', nInputFiles=-1, modelBuilder=ModelDefinitions.HeadEtaGETCore00Tail1(coverage='full'),mode='ni:train') , reload=0,fitSequence=defaultSequenceNI)
#### Create the calibrated quantities for closure tests:
trainer.save_model()
``` 
Again, same call as above, except
  1. 'reload' option is set to 0 : we're building a new NN
  2. 'mode' is set to 'ni:train' to indicate we're using the NI quantities as input.




## Plots and validation  ##


## Info on the model & predictions ##

From an interactive session :
```
exec(open('plotAndDebugDNN.py').read()) 
```

By executing this scripts we make new plotting functions availabble to the `trainer` instance.

Then :
```python
## Draw response & resolution predictions vs E or vs Eta
trainer.checkNN(otherVar=dict(fixedToMean=True, m=40) )

# The predictions will be drawn at mass fixed at m=40. One can fix any other variable by passing values to 'otherVar'
# like in otherVar=dict(fixedToMean=True, m=40, D2=0.5)
# 'fixedToMean=True' means all input variables not fixed by this call will be fixed to their mean values on all the sample.
# 'fixedToMean=Fale' means all input variables not fixed by this call will be set to their mean in the bin around the (e,m,eta) values being drawn. This works only if the relevant histos have been prepared beforehand


# dump the NN
trainer.net.summary()

# collect several plots in a pdf :
trainer.savePDFsample()

# print the maximum & minimum of the rows of the matrix of each dense
#  layer:
trainer.printLayerNorms()

# draw histograms of input features :
trainer.histogramInputs()
```

## Creating response histograms ##

The system can create a set of mass and E response histograms arranged
in a binned phase space identical to what is produced by the
`DeriveJetScale` package (but much faster !!).

From these histos, we can then draw the response and resolution
curves. 


```python
# first execute a helper scripts which setups the histogram building (including binned phase space and histogram containers):
exec(open('HistoAnalysis/buildBHfromTrainer.py').read()) 

# Then reload a trained NN in "predict" and build histograms.
# this can be done in 1 (big) command : 
trainer.runHistoFilling(conf.update(inputFiles=prefix+'CSSKUFO*root' ,  lossType='MDN', modelBuilder =ModelDefinitions.HeadEtaBlockCore1LDeepTA(N = 500,  last_activation='tanhp' ) ), \
              [mR_in_EMEta_dnn, eR_in_EMEta_dnn, ] , beginFileTask=predCalib, \
	      outName=trainer.outName(prefix).replace('.h5', '_respBH.h5') )
# - 1st argument is just the exact same config.update( ...)  arguments we have passed to trainer.train( )
# - 2nd is the list of histogram containers we want to build
# - beginFileTask is a function which is called for each input file : it just performs the DNN prediction for the content of the file.


# if we're building the histos for the uncalibrated and ATLAS calibration response, do not set 'beginFileTask'
#  but we may want to add inputFriends args to the config in case the ATLAS-calibrated quantities are in a different set of files :
# (and in that case the modelBuilder argument is irrelevant, we can put anything there)
trainer.runHistoFilling(conf.update(inputFiles=prefix+'CSSKUFO*root' ,  lossType='MDN', modelBuilder =ModelDefinitions.HeadEtaBlockCore1LDeepTA(N = 500,  last_activation='tanhp' ), inputFriends=prefix+'CalibJZ_*.root', ), \
              [eR_in_BINS_uncal,  eR_in_BINS_cal ] , beginFileTask= None\
	      outName="AtlasCalib_resp.h5" )

```

This will create the histograms and save then in an output h5 file.



## Analysing response histograms ##
The package provides utilities to analyse and plot the histograms produced at the previous step.

`BinnedArraysGraphics.py` provide plotting helpers. In particular the
`GraphicSession` class allows to read back and manipulate many
`BinnedHistos`.
There are examples in `histoscript/initRespAnalysis.py` and
`clustertraining/initRespAnalysisCL.py`. What the essentially do is
define a `GraphicSession` object with :
```python
from JetCalibDNN.HistoAnalysis import BinnedArraysGraphics as bag 
gs = bag.GraphicSession( )
```

and then read-in `BinnedHistos` from several .h5 files with
```python
gs.readBAFromFile('afilename.h5')
gs.readBAFromFile('aOtherfilename.h5')
...etc...
``` 

Then `gs` will hold all `BinnedHistos` from the read files and make
them available.

A commented example interactive session :
```python
# assume the session is started with python -i initRespAnalysisCL.py
# and the initRespAnalysisCL.py did properly call readBAFromFile()

# we can access the BinnedHistos :
>>> gs.eR_in_Eeta_uncal
<JetCalibDNN.HistoAnalysis.BinnedArrays.BinnedHistos object at 0x7f6bed937c10>

# by default only the definitions were loaded. The actual content
# needs to be loaded explicitly :
>>> gs.eR_in_Eeta_uncal.loadContent()

# now we can access histograms individually (the argument is a tuple : 
# the indices of the bin. Here, a 2D bin) :
>>> gs.eR_in_Eeta_uncal.histoAt( (3,1) )
<JetCalibDNN.HistoUtils.Histo1D object at 0x7f6bed82bb20>

# or draw it :
>>> gs.eR_in_Eeta_uncal.drawAt( (3,1) )
[<matplotlib.lines.Line2D object at 0x7f6bb42e1d60>]

# there are advanced functions like drawManyHistos, which draw histos
# from different BinnedHistos into 1 plot :
>>> gs.drawManyHistos( [gs.eR_in_Eeta_uncal, gs.eR_in_Eeta_cal ] , coords=np.s_[2,3], nplots=1)
# above we pass the list of BinnedHistos, the coordinates in numpy
# slicing format (hence the np.s_ ) and the number of subpot per
# widow.

# to draw 4 sub-plots at positons (2,3), (2,4) , (3,3), (3,4) do :
>>> gs.drawManyHistos( [gs.eR_in_Eeta_uncal, gs.eR_in_Eeta_cal ] , coords=np.s_[2:4,3:5], nplots=4)

# to save this exact graph in a pdf, do :
>>> gs.drawManyHistos( [gs.eR_in_Eeta_uncal, gs.eR_in_Eeta_cal ] ,coords=np.s_[2:4,3:5], nplots=4, outname='outHisto.pdf')

# To draw ALL the histos with 4 subplots per page into a multi page pdf, just
# ignore the coord argument : 
>>> gs.drawManyHistos( [gs.eR_in_Eeta_uncal, gs.eR_in_Eeta_cal ] ,nplots=4, outname='outHisto.pdf')

```