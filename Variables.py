"""
Define utilities to represent and manipulate variables to be used as inputs or targets.

"""
import numpy as np
import numexpr as ne
import tensorflow as tf

from .ConfigUtils import ConfigDict
from . import NNUtils as utils
from .HistoUtils import Histo1D, Histo2D


class Variable:
    """ Helper class to describe a variable used as an input or a target
    
    This class holds the variable name, description and a set of functions used to filter or transform the data (ex: m --> log(m))
    It also holds a slice to the numpy array containing the data (in self.array)

    """
    
    array = None
    interpretation = None
    referenceVars = ConfigDict()
    aliases = []
    from_src=True
    update_order = None
    #filterFunc = None
    is_sampleW = False

    dependencies = [] # the list of Variable instance name, this instance requires.
    chain = None

    pre_filter = []
    post_filter = []


    # scaling factor and offset to be applied once the variable has been scaled by NNutils.VariableScaler
    # The goal is to adjust  the scaling so the final scaled variable fit well within [-1,1]
    #  Precisely if this variable has index i in the variable list :
    #        - scaleAdjust is set  to inscaler.scaleFactor[i]
    #        - offsetAdjust is set to inscaler.offset[i]
    # scaleAdjust = 3. 
    # offsetAdjust = 0.

    scaleFactor = 1
    offset = 0
    
    # configuation when plotting histo with this variable
    h_nbins = 150
    h_range= None

    is_target = False # set to True automatically by Trainer    



    isElike = False
    
    #src_indices = None # array containing indices of all sources, == np.arange(len(src_array))
    def __init__(self,name, src_name=None, array_transfo=[],array_revtransfo=[], array_filter=None, title=None, desc=None, irrev_array_transfo=[], **args ):
        self.name=name        
        if callable(src_name): src_name = src_name(name)
        else: src_name = src_name or name
        self.src_name = src_name 
        self.array_transfo = array_transfo
        self.array_revtransfo = array_revtransfo
        self.array_filter = array_filter
        
        self.irrev_array_transfo = irrev_array_transfo # some vars need a single irrev transfo at load time. we use this list to store such transfo
        self.default_filter = array_filter
        self.title = title or name 
        self.desc  = desc or name
        
        for k,v in args.items():
            setattr(self,k,v)
        self.update_after = args.pop('update_after', [] )
        
        Variable.referenceVars[name] = self

    def clone(self):
        from copy import copy
        return copy(self)
    
    def setup(self, chain, array ):
        self.chain =chain
        self.array = array
    

    def update_array(self, reverse=False):
        a = self.array
        ldict = dict(a=a, **numExprConst)
        transformations = self.array_revtransfo if reverse else self.array_transfo
        for tf in transformations:
            ne.evaluate( tf , local_dict=ldict,out=a)
        #print ("updated ",self.name, "reverse=",reverse, a[:4])

    def irrev_update_array(self, ):
        ldict = dict(a=self.array, **numExprConst)
        for tf in self.irrev_array_transfo:
            array=ne.evaluate( tf , local_dict=ldict, out=self.array)

        
    def transformed_array(self, reverse=False , array=None):
        #print ("updating ",self.name)
        array = array if array is not None else  self.array
        transformations = self.array_revtransfo if reverse else self.array_transfo
        ldict = dict(a=array, **numExprConst)
        for tf in transformations:
            #print "tf"
            ldict['a'] = array
            array=ne.evaluate( tf , local_dict=ldict)
        return array

    
    def untransformed_array(self):
        return self.transformed_array(reverse=True)
    


    def build_pre_maskers(self):
        """Build the list of masker functions corresponding to the pre_filters """
        utils.debug('build maskers ',self.name, self.pre_filter)
        return self.build_maskers(self.pre_filter)
    def build_post_maskers(self):
        """Build the list of masker functions corresponding to the post_filters """
        return self.build_maskers(self.post_filter)


    def build_single_masker(self, filterFunc ):
        """Builds a masker function from filterFunc : the returned function perfoms the operation 'masked_entries &= filterFunc(self.array)'  """
        def _f(masked_entries):
            masked_entries &= filterFunc( self.array )
            print ("filtered ", self.name , " : ",np.count_nonzero(masked_entries), self.array[:4] ) 
        return _f

    def build_maskers(self, filters):
        """return a list of masker functions for self.array from the input filter functions (can be a either a list of func or a func)  """
        if callable(filters): return [ self.build_single_masker(filters) ]
        return [ self.build_single_masker(f) for f in filters ]
        
        
    def reset_filter(self):
        self.array_filter = self.default_filter

    def calculate_update_order(self):
        if self.update_order is not None:
            return
        if self.update_after==[]:
            self.update_order =0
            return
        others = []
        for vname in self.update_after:
            v=self.chain.allVars[vname]
            v.calculate_update_order()
            others.append( v.update_order)
        self.update_order = min(others)-1



    def overwrite_scaler(self, trainer):
        pass

    def setScaleFactors(self, scaleFactor=None, offset = None):
        if scaleFactor!=None : self.scaleAdjust = scaleFactor
        if offset !=None     : self.offsetAdjust = offset

    def setScaleParameters(self, sf=None, o=None):
        if sf!=None : self.scaleFactor = sf
        if o!=None: self.offset = o
        
    @staticmethod
    def add_alias( vname, alias):
        Variable.aliases += [ (vname, alias) ]
        Variable.referenceVars[alias] = Variable.referenceVars[vname]
        


    def asDict(self):
        d = dict( name=self.name, from_src =self.from_src,
                  array_transfo = self.array_transfo,              
                  #offsetAdjust = self.offsetAdjust,
                 )

        
        return d
        
    
class PseudoVar(Variable):
    from_src=False

    

class CopyVar(Variable):
    from_src = False
    def __init__(self,name, src_name,  **args ):
        args.setdefault('dependencies', [src_name ])
        Variable.__init__(self, name, src_name, **args)
    def setup( self, chain, array ):
        self.chain = chain
        self.array = array
        self.src_var = chain.allVars[self.src_name]
        # make sure the system will call update_array before numerator and denominator are updated. 
        self.update_after = [self.src_var.name]

    def update_array(self,reverse=False):
        if reverse:
            return
        self.array[:] = self.src_var.array[:]
        

class VarSuppress(Variable):
    """ """
    from_src=False
    useOwnScale = False
    def __init__(self,name, filteredName, threshold, above=True,  **args ):
        args.setdefault('dependencies', [filteredName])
        self.filteredName = filteredName
        self.threshold = threshold
        self.above = above
        Variable.__init__(self, name, **args)

    def setup( self, chain, array ):
        self.chain = chain
        self.filteredV = chain.allVars[self.filteredName]
        self.array = array

        # make sure the system will call update_array before numerator and denominator are updated. 
        self.filteredV.update_after += [self.name]
        #self.update_after += [.name ]
        return self.name, None

    def update_array(self,reverse=False):
        if reverse:
            return
        ldict = dict(v=self.filteredV.array, t=np.float32(self.threshold) )
        if self.above:
            ne.evaluate("where(v>t,t, v)", local_dict=ldict, out=self.array )
            print('XXXXXXXXXXX Supress', self.array[:10])
        else:
            #ne.evaluate("where(v<t,0, v-t)", local_dict=ldict, out=self.array )
            ne.evaluate("where(v<t,t, v)", local_dict=ldict, out=self.array )
            print('XXXXXXXXXXX Supress', self.array[:10])


    # def overwrite_scaler(self, trainer):
    #     """Makes sure we use exactly the same scaling as the original variable.
    #     This is called from Trainer.setupScalersFromData().
    #     """
    #     if self.useOwnScale:
    #         # do not follow original variable, just use our own parameter to scale ourselves
    #         return
    #     i, scaler = trainer.varIndexAndScaler(self.name)

    #     i_f, scaler_f = trainer.varIndexAndScaler(self.filteredName)

    #     if i is None or i_f is None : return
    #     scaler.mean[i] = scaler_f.mean[i_f]
    #     scaler.std[i] = scaler_f.std[i_f]
    #     scaler.scaleFactor[i] = scaler_f.scaleFactor[i_f]
    #     scaler.offset[i] = scaler_f.offset[i_f]
        


            
class VarAbsValue(Variable):
    """Alows to define a variable being the absloute value of a source variable."""
    from_src=False
    def __init__(self,name,  srcvar ,**args):
        Variable.__init__(self, name, dependencies=[srcvar], srcvar=srcvar, **args)
        
    def setup( self, chain, array ):
        self.chain = chain
        self.srcvar = chain.allVars[self.srcvar]
        self.array = array

        # make sure the system will call update_array before numerator and denominator are updated. 
        self.update_after += [self.srcvar.name  ]
        return self.name, None
        

    def update_array(self,reverse=False):
        if reverse: return
        ne.evaluate("abs(v)", local_dict=dict(v=self.srcvar.array), out = self.array)
        print( 'VarAbsValue ', self.srcvar.array[:10], self.array[:10] )

class VarRatio(Variable):
    """Alows to define a variable being the ratio of 2 source variables. """
    from_src=False
    def __init__(self,name, reverse_update=False, **args ):
        args.setdefault('dependencies', [args['numerator'], args['denom'] ])
        Variable.__init__(self, name, reverse_update=reverse_update, **args)
        
    def setup( self, chain, array ):
        self.chain = chain
        self.numerator = chain.allVars[self.numerator]
        self.denom = chain.allVars[self.denom]
        self.array = array
        # make sure the system will call update_array before numerator and denominator are updated. 
        if self.reverse_update:
            self.numerator.update_after += [ self.name ]
            self.denom.update_after += [ self.name ]
        else:
            self.update_after += [self.numerator.name, self.denom.name ]
        return self.name, None

    def reset_depenencies(self):
        self.dependencies = [self.numerator, self.denom]

    def update_array(self,reverse=False):
        if reverse:
            return
        #imax = len(src_indices)
        print ("updating ",self.name, self.numerator.array[:4] , self.denom.array[:4] )
        ldict = dict(n=self.numerator.array, d=self.denom.array, **numExprConst)

        if hasattr(self, 'mindenom'):
            ldict['minD']= self.mindenom 
            ne.evaluate("n/where(d==0, minD, d)", local_dict=ldict, out = self.array)
        else:
            ne.evaluate("n/d", local_dict=ldict, out = self.array)            
        #self.array[:] = self.numerator.array / self.denom.array
        
class VarLogRatio(VarRatio):
    def update_array(self,reverse=False):
        if reverse:
            return
        #imax = len(src_indices)
        print ("updating ",self.name, self.numerator.array[:4] , self.denom.array[:4] )
        ldict = dict(n=self.numerator.array, d=self.denom.array, **numExprConst)

        if hasattr(self, 'mindenom'):
            ldict['minD']= self.mindenom 
            ne.evaluate("log(n/where(d==0, minD, d))", local_dict=ldict, out = self.array)
        else:
            ne.evaluate("log(n/d)", local_dict=ldict, out = self.array)            


class VarGenExpr2(Variable):
    from_src=False
    def __init__(self,name, expr, reverse_update=False, **args ):
        args.setdefault('dependencies', [args['v1'], args['v2'] ])
        Variable.__init__(self, name, expr=expr, reverse_update=reverse_update, **args)
        
    def setup( self, chain, array ):
        self.chain = chain
        self.v1Var = chain.allVars[self.v1]
        self.v2Var = chain.allVars[self.v2]
        self.array = array
        # make sure the system will call update_array before numerator and denominator are updated.
        if self.reverse_update:
            self.v1Var.update_after += [ self.name ]
            self.v2Var.update_after += [ self.name ]
        else:
            self.update_after += [self.v1Var.name, self.v2Var.name ]
        return self.name, None
        

    def update_array(self,reverse=False):
        if reverse:
            return
        ldict = dict(v1=self.v1Var.array, v2=self.v2Var.array, **numExprConst)
        ne.evaluate( self.expr , local_dict=ldict, out = self.array)
        print("rrrrrrrrrrrr update ",self.name, self.v1Var.array[:6] , self.array[:6])


class WeightVar(Variable):
    is_sampleW = True
    normalize = True
    
    def update_array(self,reverse=False):
        Variable.update_array(self,reverse)
        if not reverse:
            if self.chain.last_loaded_file and self.normalize:
                self.normalize_array()

    def normalize_array(self):        
        self.array /=self.array[:self.chain.last_loaded_file.Nentries].sum()
        self.array *= self.array.shape[0]

class VarGenW(Variable):
    """Implements weights build from a generic expression of input variables
       Base class !! Concrete subclasses must implement local_dict() and setup()
    """
    from_src=False
    is_sampleW = True
    def __init__(self,name,  expr   ,**args):
        Variable.__init__(self, name, src_name=expr, **args)
    
    def update_array(self,reverse=False):
        if reverse:
            return
        d = self.local_dict()
        d.update( **numExprConst ) # add the pre-defined constants
        ne.evaluate(self.src_name, local_dict=d, out=self.array )
        
        
class VarEEtaW(VarGenW):
    """Implements weights build from e and eta, and a given math expresion."""

    def setup( self, chain ,array ):
        self.chain = chain
        self.e = chain.allVars["e_var"]
        self.eta = chain.allVars["eta"]
        self.array = array
        self.update_after = [self.e.name, self.eta.name]
    def local_dict(self):
        return dict(eta=self.eta.array,e=self.e.array,)

class VarEMW(VarGenW):
    """Implements weights build from e and m """
    def setup( self, chain ,array ):
        self.chain = chain
        self.e = chain.allVars["e_var"]
        self.m = chain.allVars["m_var"]
        self.array = array
        self.update_after =[self.e.name, self.m.name]

    def local_dict(self):
        return dict(m=self.m.array,e=self.e.array,)

class VarpTW(VarGenW):
    """Implements weights build from pT """
    def setup( self, chain ,array ):
        self.chain = chain
        self.pt = chain.allVars["pt_reco"]
        self.array = array
        self.update_after =[self.pt.name]

    def local_dict(self):
        return dict(pt=self.pt.array,)
        

def genVarW(varList,expr, vname=None):
    class VarWClass( VarGenW ):
        def setup(self, chain , array):
            self.chain = chain
            self.array = array
            self.ldict = dict( (v,chain.allVars[v].array) for v in varList )
            self.update_after = [  chain.allVars[v].name for v in varList ]

        def local_dict(self):
            return self.ldict

    vname = vname or  '_'.join(varList)+'_gen'
    return VarWClass(vname, expr)
        
class VarEtaW(VarGenW):
    """Implements weights build from e and eta """

    def setup( self, chain ,array ):
        self.chain = chain
        self.eta = chain.allVars["eta"]
        self.array = array
        self.update_after = [ self.eta.name]
    def local_dict(self):
        return dict(eta=self.eta.array)
        

class VarFlatEW(Variable):
    """Implements weights    """
    from_src=False
    is_sampleW = True
    
    def setup( self, chain ,array ):
        self.chain = chain
        self.e = chain.allVars["e_var"]
        self.array = array
        self.update_after =[self.e.name, ]
        self.evalind = "(a-{min})/({max}-{min})*{nbins}".format( min=self.min, max=self.max, nbins=self.nbins) 
        self.ii = np.ones_like(array,dtype=int)
        
    def update_array(self,reverse=False):
        if reverse:
            return
        ne.evaluate(self.eval, local_dict=dict(m=self.m.array,e=self.e.array,), out=self.ii )
        np.clip(ii, 0, self.nbins-1, out=self.ii)
        self.array[:]=self.hist[self.ii]
        

class WeightFromHBase(WeightVar):
    from_src=False
    apply_before_xtransfo = True
    apply_before_ytransfo = True

    Nvar = 1
    
    def setup(self, chain, array ):
        Variable.setup(self, chain, array)
        allVars =chain.allVars
        self.xvar = allVars[self.xvname]
        #self.yvar = allVars[self.yvname]

        if self.apply_before_xtransfo: self.update_after += [self.xvar.name ]
        else: self.xvar.update_after += [self.name ]            
        if self.Nvar==2:
            self.yvar = chain.allVars[self.yvname]
            if self.apply_before_ytransfo: self.update_after += [self.yvar.name ]
            else: self.yvar.update_after += [self.name ]            
            
        
        self.loadHistos(self.fname)

        self.xindices = np.zeros_like(array, dtype=int)

class WeightFromH1D(WeightFromHBase):
    normalize = False
    def loadHistos(self,fname):
        h1 = Histo1D(name='h1')
        h1.load(fname)
        h1.hcontent = 1./np.where(h1.hcontent==0., 1., h1.hcontent)
        #h1.hcontent /=h1.hcontent.mean()
        self.h1 = h1
        
    def update_array(self,reverse=False):
        if reverse:
            return
        self.h1.binContentAt(self.xvar.array, 
                           outC= self.array,
                           _x_i = self.xindices, )
        if self.chain.last_loaded_file and self.normalize:
            self.normalize_array()

        print('aaaaaaa', self.xvar.array[-4:], self.xindices[-4:], self.xindices.max(), self)
        
        
class WeightFrom2H(WeightFromHBase):
        
    def loadHistos(self,fname):
        h1 = Histo1D(name='h1')
        h1.load(fname)
        h1.hcontent = 1./np.where(h1.hcontent==0., 1., h1.hcontent)
        h1.hcontent /=h1.hcontent.mean()

        h2 = Histo1D(name='h2')
        h2.load(fname)
        h2.hcontent = 1./np.where(h2.hcontent==0., 1., h2.hcontent)
        h2.hcontent /=h2.hcontent.mean()
        
        self.h1 = h1
        self.h2 = h2
        
            
    def update_array(self,reverse=False):
        if reverse:
            return
        self.h1.binContentAt(self.xvar.array, 
                           outC= self.array,
                           _x_i = self.xindices, )
        print('aaaaaaa', self.xvar.array[-4:], self.xindices[-4:], self.xindices.max(), self)
        self.h2.binContentAt(self.yvar.array, 
                           outC= self.array,
                           _x_i = self.xindices, scaleOutC=True)
        if self.chain.last_loaded_file and self.normalize:
            self.normalize_array()

        print('aaaaaaa', self.xindices[-4:], self.xindices.max())

class WeightFromH2D(Variable):
    from_src=False
    
    def setup(self, chain, array ):
        Variable.setup(self, chain, array)
        self.yindices = np.zeros_like(array, dtype=int)

    def loadHistos(self,fname):
        
        h2 = Histo2D()
        h2.load(self.fname)
        
        self.h2 = h2
        self.reverseContent()
        
    def update_array(self,reverse=False):
        if reverse:
            return
        self.h2.binContentAt(self.xvar.array, self.yvar.array, 
                           outC= self.array,
                           _x_i = self.xindices, 
                           _y_i = self.yindices, )
        if self.chain.last_loaded_file and self.normalize:
            self.normalize_array()


    def reverseContent(self):
        h2 = self.h2
        h2.hcontent = np.where(h2.hcontent==0., 1., 1./h2.hcontent)
        
        
def eresp_in_range(r):
    return tf.logical_and(r>0.3, r<3)    
def mresp_in_range(r):
    return tf.logical_and(r>0.3, r<3)    

# needed to workaround automatic casts from literals to float64 in numexpr
numExprConst=dict(
    c_0 = np.float32(0.),
    c_01 = np.float32(0.1),
    c_001 = np.float32(0.01),
    c_0e5 = np.float32(1e-5),
    c_1 = np.float32(1.),
    c_10 = np.float32(1.),
    c_100 = np.float32(100.),
    #c_1000 = np.float32(100.),
)


_evarArgs = dict( pre_filter= lambda e : e>80, h_range=(100,6300),
                  array_transfo=[ "log(a)"  ], array_revtransfo=["exp(a)"], isElike=True,
                  #scaleAdjust=1.9, # if no transformation.
)
_mvarArgs = dict( h_range=(0,300), 
                  array_transfo=["where(a<=0.1,c_01,a)","log(a)"] , array_revtransfo=["exp(a)"], isElike=True,
                  # offsetAdjust=-0.75, # if no transformation.
)


varList = [

    Variable("e_true" ,    title="E true",  ),
    Variable("e_reco" , "e", title='E reco',  **_evarArgs,),
    Variable("e_ni" ,  "e_ni" ,  title='e NI', **_evarArgs,),
    Variable("e_dnn" ,  "e_dnn" ,  title='e DNN', **_evarArgs,),
    Variable("e_cal" ,  "e_calib" ,  title='e Atlas', **_evarArgs,),

    Variable("e_jes" ,  "e_jes" ,  title='e Atlas', **_evarArgs,),
    
    # CopyVar("e_reco_nolog" , "e_reco", title='E reco',  post_filter= lambda e : e>80, h_range=(100,6300),),
    # CopyVar("m_reco_nolog" , "m_reco", title='M reco',  ),

    Variable("e_reco_nolog" , "e_reco", title='E reco', pre_filter= lambda e : e>80, h_range=(100,6300),),
    Variable("m_reco_nolog" , "m_reco", title='M reco' , hrange=(0,300) ),
    

    
    Variable("m_true" ,     title='M true', **_mvarArgs),
    Variable("m_reco" ,  "m" ,  title='M reco',**_mvarArgs),
    Variable("m_ni" ,  "m_ni" , title='M NI',**_mvarArgs),
    Variable("m_dnn" ,  "m_dnn" ,  title='M DNN', **_evarArgs,),
    Variable("m_cal" ,  "m_calib" ,  title='M Atlas', **_evarArgs,),
    Variable("m_jes" ,  "m_jes" ,  title='M Atlas', **_evarArgs,),
    
    Variable("pt_true" ,    title="pT true",  **_evarArgs,),
    Variable("pt_true_lows" , "pt_true", title="pT true",  hrange=(0,4000), isElike=True),# intended to be scaled such only lower pT are in [-1,1]
    Variable("pt_reco" ,    title="pT reco",  isElike=True),
    Variable("pt_reco_lows" , "pt_reco",    title="pT reco",  hrange=(0,4000), isElike=True ), # intended to be scaled such only lower pT are in [-1,1]

    VarSuppress("pt_reco_low" , "pt_reco", threshold=80,    title="pT reco", useOwnScale=True , isElike=True),
    

    Variable("jesR" , "jesF", h_range=(0,2),  pre_filter=lambda v : np.isfinite(v), array_transfo=["1/a","1/a"] ), # jesF=e_jes/e_reco maybe nan because e_jes maybe nan
    
    Variable("NPV" , h_range=(0,50), bins=45, ),
    Variable("mu", h_range=(0,110),  ),
    #WeightVar("eventWeight" ,),
    WeightVar("eventWeightXS", normalize=False),

    Variable("PID", h_range=(0,23), ),
    
    Variable("DetectorEta" , title='eta',h_range=(-5.,5.),),
    Variable("eta_det" ,  "eta_det",h_range=(-5.,5.),),
    Variable("eta", "DetectorEta" ,h_range=(-5.,5.),  ),
    Variable("eta_true", "True Eta" ,h_range=(-5.,5.),  ),
    VarGenExpr2("Deta_det", "v2-v1", v1='eta_det', v2='eta', h_range=(-0.5,0.5)),

    
    Variable("EMFrac", h_range=(0,1), array_transfo=["where(a!=a,c_0,a)"] ),    
    Variable("D2", array_transfo=["where(a!=a,-c_01,a)"] , h_range=(-0.001,12),  ), # "a!=a" a trick to avoid nan values (isnan not supported by numexpr2)
    Variable("C2", array_transfo=["where(a!=a,-c_01,a)"] , h_range=(-0.001,12),  ),
    Variable("Tau32",    array_transfo=["where(a<=0,-c_01,a)"], h_range=(-0.001,1.4) ),
    Variable("Tau21",    array_transfo=["where(a<=0,-c_01,a)"], h_range=(-0.001,1.4) ),
    Variable("Qw" , array_transfo=["where(a<=0,c_1,a)"], h_range=(0,250) ,  ), ## Ntuple is wrongly in MeV !!!        
    Variable("EffNConsts" , h_range=(0,40) , ),
    Variable("EffNTracks" , h_range=(0,18) , array_transfo=["where(a!=a,-c_01,a)"]),

    Variable("groomMratio" , h_range=(0,1), array_transfo=["where(a!=a,c_0,a)"] ),
    Variable("neutralFrac" , h_range=(0,1) , ),
    Variable("sumPtTrkFrac" , h_range=(0,1.2) , ),
    Variable("EM3Frac" , h_range=(0,0.5) , array_transfo=["where(a!=a,c_0,a)"] ),
    Variable("Tile0Frac" , h_range=(0,0.7), array_transfo=["where(a!=a,c_0,a)"] ),
    Variable("sumMassTrkFrac", h_range=(0,1), ),

    Variable("fracPtLeadJet" , h_range=(0,1.),  ),
    
    VarAbsValue('abseta', 'eta' , hrange=(0,5) ),

    
    VarRatio("r_raw_e",   numerator="e_reco", denom="e_true",  post_filter=lambda r : np.logical_and(r>0.1, r<3),  title='E response', h_range=(0,3) , ),
    VarRatio("r_raw_m",   numerator="m_reco", denom="m_true" , post_filter=lambda r : np.logical_and(r>0.1, r<5) , mindenom=np.float32(1.), title='M response' ,h_range=(0,4),),

    VarRatio("r_raw_pt",   numerator="pt_reco", denom="pt_true",  post_filter=lambda r : np.logical_and(r>0.1, r<3),  title='p_T response', h_range=(0,3) , ),
    

    VarRatio("r_cal_e",   numerator="e_cal", denom="e_true",  post_filter=lambda r : np.logical_and(r>0.3, r<3),  title='E response', h_range=(0,3) , ),
    VarRatio("r_cal_m",   numerator="m_cal", denom="m_true" , post_filter=lambda r : np.logical_and(r>0.1, r<5) , mindenom=np.float32(1.), title='M response' ,h_range=(0,4),),
    VarRatio("r_dnn_e",   numerator="e_dnn", denom="e_true",  post_filter=lambda r : np.logical_and(r>0.1, r<3),  title='E response', h_range=(0,3) , ),
    VarRatio("r_dnn_m",   numerator="m_dnn", denom="m_true" , post_filter=lambda r : np.logical_and(r>0.1, r<5) , mindenom=np.float32(1.), title='M response' ,h_range=(0,4),),

    VarRatio("r_jes_e",   numerator="e_jes", denom="e_true",  post_filter=lambda r : np.logical_and(r>0.1, r<3),  title='E response', h_range=(0,3) , ),
    VarRatio("r_jes_m",   numerator="m_jes", denom="m_true" , post_filter=lambda r : np.logical_and(r>0.1, r<5) , mindenom=np.float32(1.), title='M response' ,h_range=(0,4),),
    
    VarRatio("r_e_ratio", numerator="r_dnn_e", denom="r_cal_e", h_range=(0,2.)),
    Variable("r_m_ratio", numerator="r_dnn_m", denom="r_cal_m", h_range=(0,2.)),

    VarEEtaW("eetaW", "cosh(eta)*150/e+1"),
    VarEEtaW("highEW", "exp( 5*(e/4000 -1) )+1"),
    VarEEtaW("highEW2", "exp( 6*(e/3000 -1) )+1"),
    #VarEtaW("etaW"),
    VarEMW("emW","2*exp(-m/(80*where(e<500,1,1+(e-500)/500)))"),
    VarEMW("MoEW","where((m/e<0.02) & (e>2000), 5*c_1,  c_1)"),

    VarEMW("WZmassW","where( (m<75) | (m>100) , 5*c_1,  c_1)"),

    VarEEtaW("highEW", "tanh((e-2000)/1000)*5 +6" ),
    VarEEtaW("highEtaW", "tanh(abs(eta)-1)*5 +6" ),

    VarEEtaW("highEetaW", "(tanh((e-2000)/1000)+2)*(tanh(abs(eta)-1)+2)"),

    VarpTW("lowpTW", "where(pt<500, c_1, c_0)"),

    Variable("r_ni_e" , "r_e_ni", title='E NI respone',h_range=(0,3), ),
    Variable("r_ni_m" , "r_m_ni", title='M NI respone',h_range=(0,4), ),

    PseudoVar("r_e_sig", title="E Resolution"),
    PseudoVar("r_m_sig", title="M Resolution"),    
    PseudoVar("m_reco_sig", title="M Resolution"),    

    PseudoVar("r_e_sig2", title="E Resolution (right)"),
    PseudoVar("r_m_sig2", title="M Resolution (right)"),    
    
    VarRatio("MoE",   numerator="m_var", denom="e_var",   title='M/E'),
    VarLogRatio("LogMoE",   numerator="m_var", denom="e_var",   title='Log(M/E)',mindenom=np.float32(1.),h_range=[-8,0]),
    VarGenExpr2("LowMoES", "log(c_1+exp( 3*c_10*(4*c_001-v1/v2)) )", v1='m_var', v2='e_var', h_range=[0,1]),

    VarGenExpr2("deltaEta", "abs(v1-v2)", v1='eta', v2='eta_true', ),


    VarSuppress("HighE", "e_var",threshold=np.log(2500.), above=False),
    VarSuppress("LowMoE", "MoE", threshold=0.03, above=True, useOwnScale=False, h_range=[0,1]), # MoE=0.03 --> log(MoE)=-3.506
    
    WeightFrom2H("EMweights", xvname='m_true', yvname='e_true', fname='hEM.npz',
    ),
    WeightFromH1D("Eweights", xvname='e_true',  fname='hE.npz',
                  adjustScaleOffset = dict( e_var = (2.,-0.1),
                                           ),
                  ),

    WeightFromH1D("LogMoEweights", xvname='LogMoE',  fname='hLogMoE.npz',
                  apply_before_xtransfo=False,
                  ),


    WeightFromH1D("LogEweights", xvname='e_true',  fname='hLogEtrue.npz',
                  apply_before_xtransfo=False,
    ),


    # Small R variables :
    Variable("area" ,  "area", h_range=(0, 1) ),
    Variable("jet_Wtrk1000" ,  "jet_Wtrk1000", h_range=(0,1)),
    Variable("jet_Ntrk1000" ,  "jet_Ntrk1000", h_range=(0,50) ),
    Variable("jet_ChargedFraction" ,  "jet_ChargedFraction"),
    Variable("jet_nMuSeg" ,  "jet_nMuSeg"),
    Variable("rho" ,  "rho"),
    Variable("jet_ChargedFraction_SumPtTrkPt500" ,  "jet_ChargedFraction_SumPtTrkPt500"),
    Variable("jet_ChargedFraction_charged" ,  "jet_ChargedFraction_charged"),
    Variable("jet_ChargedFraction_combined" ,  "jet_ChargedFraction_combined"),
    Variable("jet_ChargedFraction_charged_tracks" ,  "jet_ChargedFraction_charged_tracks"),
    Variable("jet_ChargedFraction_tracks" ,  "jet_ChargedFraction_tracks"),
    Variable("jet_eEM3" ,  "jet_eEM3"),
    Variable("jet_eTile0" ,  "jet_eTile0"),


    VarRatio("fracTile0",   numerator="jet_eTile0", denom="e_reco",    title='Tile0 frac', h_range=(-0.1,1.1) , ),
    VarRatio("fracEM3",   numerator="jet_eEM3", denom="e_reco",    title='EM3 frac', h_range=(-0.1,1.1) , ),
    

    Variable("Width", title='Width', h_range=(0.,1.0)),
    Variable("Split12", title='Split12', h_range=(0,500)),
    Variable("Split23", title='Split23', h_range=(0,200)),

]

# When using Numerical Inversion what actual variable is used for e or m (or the ratios) depends
# on the num inv step (or input mode).
# The dictionnary below is used to map the generic 'e_var', 'm_var',etc.. to the actual Variable name
# according to the input mode.

_defA = lambda t,n,r : ConfigDict( true=t,ni=n,reco=r)
Variable.aliasConfig = ConfigDict(
    e_var = _defA('e_true', 'e_ni','e_reco'),
    m_var = _defA('m_true', 'm_ni','m_reco'),
    r_e   = _defA('r_raw_e','r_ni_e','r_raw_e'),
    r_m   = _defA('r_raw_m','r_ni_m','r_raw_m'),
    #eta = _defA('eta','eta','eta'),
    pt_var_lows = _defA('pt_true_lows', 'pt_ni_lows','pt_reco_lows'),
)




# ****************************************************
# Define known partitions of the input data.
#  each partition is defined as a set of filters on 1 or more input variables.
#  There is 2 kind of filters : those applied before the variable transformation, those after.
# a partition is thus define as 2 dictionnaries :
#
#   part_name = ( dict_preFilters, dict_postFilters) ,
# where
#  dict_preFilters = dict( var_name1 = filter_function, var_name2 = filter_function2, ...)
# 
dataPartitions = ConfigDict( )
dataPartitions[None] = ({}, {} )
