import numexpr as ne
import numpy as np

import types

import sys
if sys.version_info[0] < 3:
    print('Not working with python2 !!!')
else:
    #import uproot_methods.classes.TH1
    import uproot
import h5py


from .. import HistoUtils




def binarySearch(bins, x, indices=None, validity=None):
    """Calculate the bin index of 'x' values corresponding to bins defined by 'bins'
    returns the array of these indices and the array of their validity (array of bool)
    If given, indices and validity arrays are filled and returned.
    
    (This function always create temporary indices )
    """
    if indices is not None:
        indices[:] = np.searchsorted(bins,x, 'right')
    else:
        indices = np.searchsorted(bins,x, 'right')
    if validity is None:
        validity= np.zeros_like(indices, dtype=np.bool)


    indices -=1
    ne.evaluate('(indices>-1) & (indices<{}) '.format(len(bins)-1),out=validity,casting='unsafe')
    #print(validity)    
    return indices, validity

    
    




AxisSpec=HistoUtils.AxisSpec
        
class BinnedPhaseSpace:
    """Represents a grid of contiguous bins in a N dimensional phase space. 
    Each dimension is given by a AxisSpec.
    """

    class Indices:
        """A helper class to conveniently obtain a block of index coordinates in a given BinnedPhaseSpace bps.
        
        usage is as in : coords = bps.indices[:2,:,4] 
        then coords is an array of (N, ndim). coords contains all possible index where 
          * dim 0 varies from 0 to 2
          * dim 1 varies on all possible index in  bps.axis[1]
          * dim 2 contains only index 4 
        """
        def __init__(self, bps):
            self.bps = bps

        def __getitem__(self, sl ):
            if sl==() or sl is None:
                sl=[ slice(None) for s in self.bps.axis ]
            if isinstance(sl, slice):
                sl = (sl,)
            indices = [ np.arange(a.nbins)[s]  for (a,s) in zip(self.bps.axis, sl) ]
            #print(indices)
            # trick to build a cartesian product of 'indices' :
            t = np.array(np.meshgrid( *indices ,indexing='ij') ).T.reshape(-1,self.bps.nDim())
            return t
        
    
    def __init__(self, name,  *axis):
        self.axis = axis
        self.name = name
        for i,a in enumerate(axis):
            setattr(self,'a'+str(i),a)
        self.tagBase = '_'.join( ['{:d}'] * len(axis) )
        self.nbins = 1
        for a in self.axis: self.nbins *= a.nbins
        self.indices = BinnedPhaseSpace.Indices(self)
        
    def shape(self):
        return tuple( a.nbins for a in self.axis)
        
    def nDim(self):
        return len(self.axis)
            
        
    def iter(self):
        # simple iterator over all possible indices.
        it =np.ndindex( tuple(a.nbins for a in self.axis)[::-1] )
        for i in it:
            yield i[::-1]
        
        
    def findBins(self, coords, indices=None,validity=None):
        """Find the multi-dim indexes of points represented by coords.
        coords is in the form [array([x0,x1,x2,...,xN]), array([y1,y2,...,yN])...]
        
        if given, indices and validity are filled. They are returned 
                
        """
        if indices is None:
            indices = np.zeros_like( coords, dtype=int)
        validityI = np.zeros_like( coords, dtype=bool)

        # for each dimension find the index of the coordinates and if they are valid.
        #  put the results in each entry of indices and validityI
        for i,co in enumerate(coords):
            self.axis[i].binIndexAndValidity(co, indices[i], validityI[i] )

        # evaluate the validity for each point by conbining with AND the validity of each dimension.
        #   - build the expression in the form "i1&i2&i3..."
        nL = ['i'+str(i) for i in range(self.nDim()) ]
        expr='&'.join(nL)
        #print('findBins expr=',expr, '-->', validityI)
        #   - evaluate the expressions on all points corresponding to coords
        ne.evaluate( expr, local_dict=dict( (n,validityI[i]) for i,n in enumerate(nL) ),out=validity)
        return indices, validity

    def findValidBins(self, coords, indices=None):
        """ given a list of coordinates 'coords' (an array of shape (ndim, N) , return the valid bin indices as  2 objects :
        
          * a tuple of ndim arrays where array i has indices in dimension i. Each array has size validN where validN is the number of valid entries in 'coords' (i.e within the binspace).
          * an array of indices corresponding to valid entries  in 'coords', thus of size validN.
        
        """
        if indices is None:
            indices = np.zeros_like( coords, dtype=int)
        validity = np.zeros( (coords[0].shape[0],)   , dtype =bool )
        self.findBins(coords, indices, validity )
        print('inputs ', coords[0][:5])
        print('validity', validity[:5])
        # get indices that are non-zero (thus True) in validity 
        validCoordI = np.flatnonzero( validity)
        #print('validCoordI', validCoordI[:5])
        validN = len(validCoordI)
        for i in range(self.nDim()):
            # take only the valid indices for dim i , and put them back into indices 
            np.take(indices[i], validCoordI, out=indices[i][:validN])

        #print("vals = ", )
        tupleI = tuple(i[:validN] for i in indices)
        for t in tupleI:
            print('__',t[:5])

            
        return tupleI, validCoordI
        

    def describeBin(self, b ):
        if isinstance(b,int):
            b = self.iteratorAtIndex(b)
        else:
            bin = b
        return [ a.describeBin(bin[i]) for (i,a) in enumerate(self.axis)]

    def showStruct(self):
        print("BPS",self.name, self.nbins," bins :")
        info='{:>20} : {:d} bins '
        t=np.get_printoptions()['threshold']
        np.set_printoptions(threshold=7) 
        for ax in self.axis:
            print( info.format(ax.name, ax.nbins), ax.edges )
        np.set_printoptions(threshold=t)


    def tagAt(self, coords):
        return self.tagBase.format(*coords)


    def buildNCoordinates(self,N):
        return np.zeros( (self.nDim(), N)  , dtype =int )


    def axisTag(self):
        return ''.join(a.name for a in self.axis)

    def subspace(self, retainDims=(), removeDim=() ):
        """Returns a new BinnedPhaseSpace representing a subspace of self. 
        Can be called either by passing the removed dimensions or the retained dimensions
        """
        if removeDim!= () and retainDims != ():
            print ("BinnedPhaseSpace.subspace() ERROR : specify either removeDim either retainDims, not both")
            raise
        if removeDim != ():
            if isinstance(removeDim,int): removeDim=(removeDim,)
            retainDims = tuple( i for i in range(self.nDim()) if i not in removeDim)

        newaxis = [self.axis[i] for i in retainDims]
        return BinnedPhaseSpace(self.name+'_'.join([str(i) for i in retainDims]), *newaxis)
    


    
    
class BinnedArrays:
    """Base class to store numpy arrays in a multidimensionnal phase space
    The phase space is represented by a BinnedPhaseSpace  (self.bps attribute)
    Concrete class will implement the actual content. Typically by implementing a numpy array 
    of dimension self.bps.nDim()+1 where the last dimension represent the arrays in each bin.
    """

    drawStyle = {} # option to be passed to matplotlib graphs.
    isLoadedFromInput = False
    persistNonArrays = [] # list of non-array attribute we want to save.
    inputFile = None
    def __init__(self, name, bps, initArrays=True, label=None):
        self.name = name
        self.bps = bps

        if initArrays:
            self.initArrays()

        self.label = label or name
        self.friendNames=[]

    def setup(self):
        if self.drawStyle == {} : self.drawStyle={}
        self.drawStyle.setdefault('label', self.name)

        if self.persistNonArrays == [] : self.persistNonArrays = []

    
    def nameAt(self, coords):
        return self.name+'_'+self.bps.tagAt(coords)

    def addFriend(self, name, shape=(), dtype=np.float32, array=None, ):
        a = array if array is not None else np.zeros( self.bps.shape()+shape, dtype=dtype)
        self.friendNames.append(name)
        setattr(self, name, a)
        return a
        
    def persName(self):
        return '_BH_'+self.persPrefix+'_'+self.name         

    def saveInH5(self, fname=None, mode='a', ):

        fname = fname or self.inputFile
        if isinstance(fname, str):
            f = h5py.File(fname, mode)
        else: f = fname
        gname = self.persName()
        g = f.require_group(gname)
        from pickle import dumps
        bps_s=dumps(self.bps)
        
        if 'bps' in g:del(g['bps'])
        g['bps'] =np.bytes_(bps_s)


        if 'friendNames' not in self.persistNonArrays: self.persistNonArrays.append('friendNames')
        if 'persistNonArrays' in g: del(g['persistNonArrays'])
        g['persistNonArrays'] = np.bytes_( dumps( dict( (n,getattr(self,n))  for n in self.persistNonArrays) ) )
        
        for k in self.persistentAttr+self.persistentAttrHeavy+self.friendNames:
            v = getattr(self,k)
            if k in g : del(g[k])
            g[k]=v

        if isinstance(fname, str):
            f.close()
        
        
    def loadFromH5(self, fname, loadContent=True, encoding=None):
        if isinstance(fname, str): f = h5py.File(fname, 'r')
        else: f=fname
        gname = self.persName()
        #print( gname , ' .. ', f)
        g = f[gname]
        self.inputFile = f.filename
        
        from pickle import loads
        opt={} if encoding is None else dict(encoding=encoding)
        #print(opt, encoding,  g['bps'][()].tostring())
        self.bps = loads( g['bps'][()].tostring()  , **opt)
        self.bps.indices = BinnedPhaseSpace.Indices(self.bps)
        if 'persistNonArrays' in g:
            d = loads( g['persistNonArrays'][()].tostring()  , **opt)
            self.persistNonArrays = []
            for k,v in d.items():
                setattr(self,k,v)
                self.persistNonArrays+=[k]

        dontLoadYet = self.persistentAttrHeavy+self.friendNames+['persistNonArrays']
        for k,v in g.items():
            if k in dontLoadYet or k=='bps' : continue
            v = v[()]
            if isinstance(v,bytes): v=v.decode()            
            setattr(self,k,v)
            
        if loadContent : self.loadContent(f)
        if isinstance(fname, str) : f.close()

    def loadContent(self,f=None):
        """Load the heavy arrays for this BinnedArrays from file (if f==None, default to self.inputFile) """
        if self.isLoadedFromInput:
            return

        f = self.inputFile if f is None else f
        if isinstance(f,str): f =  h5py.File(f, 'r')            
        gname = self.persName()
        g = f[gname]
        for k in self.persistentAttrHeavy+self.friendNames:
            setattr(self,k, g[k][()])
        self.isLoadedFromInput = True
        if isinstance(f,str) : f.close()


    def applyFunc(self, func, rangei=() ):
        """Apply the function func on some bin range (default : all bins in the phase space)
        Function is called for wach bin as : func( coords, self) 
        """
        for c in self.bps.indices[rangei]:
            func( tuple(c), self)
        



    def reduceToBinnedLine(self, lineDim, reduceFunc,name=None, withErr=True, ylabel=None):
        """Build a new BinnedLine on a subspace of self.bps with dimension lineDim removed. 

        Typically if this space space has dim (X,Y,Z) then reduceToBinnedLine(2, f ) will return a 
        phase space (X,Y) with each bin containing a graph (Z,f(Z)). 

        The graphs/lines in the new BinnedLine are of the form (X,Y) where 
          * X and Y are arrays of same size as dimension lineDim 
          * X is given by the center of the bins of  of bh.bps.axis[lineDim] (by default)
          * Y is calculated by calling 'reduceFunc' on each bin of the new phase space.
         

        For each bin coordinates c of the new phase space, 'reduceFunc()' is called as 

          reduceFunc(self, coordinates_at_c, X) and is expected to return ( X,Y,Y_err )


        coordinates_at_c is the slice corresponding to all indices of the top phase space (self.bps)  
        which correspond to c in the subspace being created. It's a tuple of int arrays (== a numpy slice)
        
        if c=(c_0,...,c_n) then coordinates_at_c=(c_0,...,:,..,c_n) where ':' is at pos lineDim

        Ex : if we have dimensions (3,5,6)  and reducing lineDim=1 then coordinates_at_c will correspond to 5 indices each time.

        the result (X,Y,Y_err) may have a size < (nbins in linedDim)
        """

        # create a new BinnedLine 
        name = name or self.name+'_lines_'+'_'.join([str(i) for i in range(self.bps.nDim()) if i!=lineDim])
        newbps = self.bps.subspace(removeDim=lineDim)

        bL = BinnedLine(name, newbps, maxN = self.bps.axis[lineDim].nbins, withErr=withErr,
                        xlabel = self.bps.axis[lineDim].title,
                        ylabel = ylabel or reduceFunc.__name__
        )
        bL.drawStyle = dict(self.drawStyle)
        bL.parent = self
        # --

        # a shortcut for a slice spanning a full dimension
        all = slice(None)

        # shortcut to our index utility
        topindices = self.bps.indices

        # create the default X of the graphs/lines to be created
        edges = self.bps.axis[lineDim].edges
        x0 = 0.5*(edges[:-1]+edges[1:])

        # loop over new binned space and calculate the lines :
        for c in newbps.indices[()]:
            #print(c)
            # we build all the indices in our space corresponding to c :
            topslice=tuple(c[:lineDim])+(all,)+tuple(c[lineDim:])
            c_top = tuple(topindices[topslice].T)

            # call the reduceFunc :
            x,y,y_err=reduceFunc(self, c_top, x0)

            # assign results to the new BinnedLine 
            c=tuple(c)
            n=x.shape[0] # it is possible x !=x0 and some 'invalid' points have been ignored. We honour this.
            bL.array_x[c][:n]=x
            bL.array_y[c][:n]=y
            bL.npoints[c]=n
            if withErr: bL.array_yerr[c][:n]=y_err

        return bL
        
            

    knownClasses= dict(  )
    @staticmethod
    def readBAFromFile(fname, store=None, loadContent=False, encoding=None, exclude='', tag=''):
        """read all instances of BinnedArrays in a h5 file and assign them to a 'store' (by default a simple namespace).
        The content of the BinnedArrays is not read by default.

        """
        if isinstance(fname, str): f = h5py.File(fname, 'r')
        else: f=fname

        store = store or types.SimpleNamespace()
        allBA = []

        print( tag, '  ---- Reading from ', fname)
        for k in f.keys():
            if exclude!='' and exclude in k: continue
            print(k)
            if k =='trainingSummary':
                from pickle import loads
                setattr(store, 'trainingSummary'+tag,  loads( f[k][()].tostring()  ) )
            
            if k.startswith('_BH_'):
                persPrefix = k[4:7]
                name = k[8:]
                klass = BinnedArrays.knownClasses[ persPrefix ]
                o = klass( name , bps=None , initArrays=False)
                o.loadFromH5(f, loadContent=loadContent, encoding=encoding)
                o.label = tag
                o.setup()
                setattr(store, name+tag, o)
                allBA.append(o)
        f.close()
        return store, allBA

    @staticmethod
    def toKnownClasses(cls):
        BinnedArrays.knownClasses[cls.persPrefix] = cls
        return cls




@BinnedArrays.toKnownClasses
class BinnedScalar(BinnedArrays):
    # what members will be persistified
    persistentAttr = ['nentries']
    persistentAttrHeavy = [ 'array', 'counts']
    persPrefix = 'BSc'

    array = None
    def __init__(self, name, bps,  initArrays=True, ):
        """ bps: the BinnedPhaseSpace representing the bins of the phase space in which the scalar values are kept
        """
        BinnedArrays.__init__(self,name, bps,  initArrays,)
        if bps: self.setup()
    

    def initArrays(self):
        shape = [a.nbins for a in self.bps.axis]        
        self.array = np.zeros( shape , dtype='float32')
        self.counts = np.zeros( shape , dtype='int')
        self.nentries = 0
        

    def accumulate(self, coords, values):
        """Fill histos in nbin given by coords with values.
           coords = array( (ndim, nvalues), dtype=float32)
           values = array( (nvalues,), dtype=float32) 
        """
        # allocate arrays for indices        
        indices = np.zeros( (self.bps.nDim()+1, values.shape[0])  , dtype =int )

        # find indices of coords falling in valid nbin :
        tupleI,validCoordI = self.bps.findValidBins(coords,indices[:-1,:] )
        # validCoordI is the indices of values corresponding to valid bins

        np.add.at(self.array,  tupleI , values[validCoordI] )
        np.add.at(self.counts,  tupleI , 1 )
        self.nentries += len(validCoordI)    



    def sumAlong(self, dim):
        bs = BinnedScalar( self.name+f'_sum{self.bps.axis[dim].name}',
                           self.bps.subspace(removeDim=(dim,) ),initArrays=False )
        bs.nentries = self.nentries
        bs.array = self.array.sum(dim)
        bs.count = self.counts.sum(dim)
        
        

    @staticmethod
    def concat(listBS, name=None):
        """create a new BinnedScalar by stacking the BinnedScalar in listBS. 
        All BPS in listBS must be the same (bps0), then the new BinnedScalar has a new BPS being bps0+1 dim
        """
        bs0 = listBS[0]
        bps = BinnedPhaseSpace( bs0.bps.name+"_list", *(bs0.bps.axis+(AxisSpec('n', len(listBS) , range=(0,len(listBS)) ),)  ) )
        name = name or  'concat_'+bs0.name
        newBS = BinnedScalar( name,bps)
        for i,bs in enumerate(listBS):
            newBS.array[...,i] = bs.array
            newBS.counts[...,i] = bs.counts
        return newBS
        
Histo1 = HistoUtils.Histo1D
        
@BinnedArrays.toKnownClasses
class BinnedHistos(BinnedArrays):
    """Represents a set of histograms arranged in a multi-dimension binned phase space"""

    # what members will be persistified
    persistentAttr = ['x_'+a for a in AxisSpec.save_att]
    persistentAttrHeavy = [ 'array', 'arrayw2', 'entriesPerH']
    persPrefix = 'BH1'

    array = None
    xaxis = None
    def __init__(self, name, bps, hspec=None, initArrays=True, ):
        """ bps: the BinnedPhaseSpace representing the bins of the phase space in which the histos are kept.
            hspec:  in the form (histo_nbins, (histo_min, histo_max) ) or (histo_nbins, (histo_min, histo_max), "name:title" )"""
        if bps and hspec is not None:
            if len(hspec)==2 : hspec=tuple(hspec)+('xaxis:xaxis',)
            nbins, range, hname = hspec
            hname = hname.split(':')
            self.nbins = nbins
            self.range = range
            self.xaxis = AxisSpec( hname[0], nbins=nbins, range=range,title=hname[-1])
        else:
            self.xaxis = AxisSpec('xaxis')
        BinnedArrays.__init__(self,name, bps,  initArrays,)
        if bps: self.setup()
                    

    def setup(self):
        BinnedArrays.setup(self)
        #if hasattr(self, 'bins'): self.nbins = self.bins # TEMPORARY to read back older version
        if self.xaxis :
            range = self.xaxis.range
            self.binwidth = (range[1]-range[0])/self.xaxis.nbins

    def initArrays(self):
        shape = [a.nbins for a in self.bps.axis]+[self.xaxis.nbins+2] # +2 : overflow and underflow nbin 
        self.array = np.zeros( shape , dtype='float32')
        self.arrayw2 = np.zeros( shape, dtype='float32' )
        self.entriesPerH = np.zeros( shape[:-1] ,dtype=int)


    def loadFromH5(self, fname, loadContent=True, encoding=None):
        BinnedArrays.loadFromH5(self, fname, loadContent, encoding)
        d ={}
        for a in AxisSpec.save_att:
            d[a] = getattr(self, 'x_'+a, '' )
        self.xaxis.loadFrom(d, prefix='')

    def saveInH5(self, fname=None, mode='a', ):
        for a in AxisSpec.save_att:
            setattr(self, 'x_'+a,getattr(self.xaxis,a) )
        BinnedArrays.saveInH5(self, fname, mode)
        

    def projectOnDim(self, i):
        newBh = BinnedHistos( self.name+str(i), hspec=(self.xaxis.nbins, self.xaxis.range) ,bps = self.bps.subspace(removeDim=i) ,initArrays=False)
        newBh.array = self.array.sum(axis=i)
        newBh.arrayw2 = self.arrayw2.sum(axis=i)
        newBh.entriesPerH = self.entriesPerH.sum(axis=i)
        newBh.isLoadedFromInput =True
        return newBh
    
    ## ********************************
    # Filling methods
    def fill(self, coords, values, weights=1):
        """Fill histos in nbin given by coords with values.
           coords = array( (ndim, nvalues), dtype=float32)
           values = array( (nvalues,), dtype=float32) 
        """
        # allocate arrays for indices        
        indices = np.zeros( (self.bps.nDim()+1, values.shape[0])  , dtype =int )
        
        # find indices of coords falling in valid nbin :
        tupleI,validCoordI = self.bps.findValidBins(coords,indices[:-1,:] )
        # validCoordI is the indices of values corresponding to valid bins

        # prepare weights corresponding to valid bins 
        if isinstance(weights, np.ndarray):
            weights = weights[validCoordI]

        # finalize fill operations (this will compute histo indices and actually append values to histos)
        self.fillAtIndices( values,  validCoordI, tupleI,weights, weights*weights, indices[-1] )
            


    def fillAtIndices(self,values, validValueIndices,  validTupleIndices,  validW=1, validW2=1, hIndices=None):
        """Fill values into our histos, using a given pre-calculated list of valid indices (validValueIndices) and a corresponding list valid of bins (validTupleIndices, the bins index coordinates).         

        Optionnally fill with weights and squared weights.

        The input arrays must be like :
        validValueIndices = array( (validN,) )
        validTupleIndices = tuple( array(validN),  .. , array(validN) ) 
        value, validW and validW2 = array( (N,) ) where N>= validN
        
        validValueIndices,  validTupleIndices are typically obtain from 'bps.findValidBins( some_coords )'

        hIndices can be given to avoid allocation here. IT must be array( len(values), dtype=int) 
        """
        if hIndices is None:
            hIndices = np.zeros_like( values, dtype=int)

        validN =len(validValueIndices)
        # map the values to their bin index into hIndices
        self.xaxis.binIndex( values, hIndices)

        # select only the histo entries corresponding to valid bins. 
        np.take(hIndices, validValueIndices, out=hIndices[:validN])

        tupleI = validTupleIndices+(hIndices[:validN],)
        
        np.add.at(self.array,  tupleI , validW )
        np.add.at(self.arrayw2, tupleI , validW2 )
        np.add.at(self.entriesPerH, tupleI[:-1], 1) 
        

    ## ********************************

    def histoAt(self, coords):
        #h = self.hModel
        hcontent = self.array[coords]
        hname=self.nameAt( coords )
        h = Histo1(name=hname, axis=self.xaxis,
                   #edges=self.edges,
                   hcontent = hcontent,
                   hw2 = self.arrayw2[coords],
                   #_fEntries = self.entriesPerH[coords] ,
                   #_fMaximum = hcontent.max(),
                   title = self.label,
                   )
        return h
        
    def drawAt(self, coords, ax=None, **opts):
        from matplotlib import pyplot as plt
        if ax is None: ax=plt.gca()
        if self.array is None:self.loadContent()
        h = self.histoAt(coords)
        return h.draw(ax=ax,**opts)

        

    def saveAsROOT(self, fname):
        if isinstance(fname,str):
            f = uproot.recreate(fname)
        else: f= fname
        
        for index in np.ndindex( self.array.shape[:-1] ):
            #print(index)
            h = self.histoAt( index )
            f[h.name] = h
        f.close()

    def normalize(self, perHEntries=False):

        
        s = self.array.sum(self.bps.nDim(), keepdims=True)
        if perHEntries: s /= self.entriesPerH.reshape( self.bps.shape()+(1,) )
        try:
            test = self.sum_array 
            print(self.name, "already normalized")
        except:
            print( self.name , 'normalize')
            self.sum_array = s
        
        #if s==0: return
        self.array /= s
        self.arrayw2 /= s*s

    def renormalize(self,):

        self.array *= self.sum_array
        self.arrayw2 *= self.sum_array*self.sum_array
        
        




@BinnedArrays.toKnownClasses
class BinnedLine(BinnedArrays):
    """Represents a set of 1D lines arranged in a multi-dimension binned phase space"""

    persistentAttr = ['maxN', 'xlabel', 'ylabel']
    persistentAttrHeavy = [ 'array_x', 'array_y', 'array_yerr', 'npoints']
    persPrefix = 'BL1'

    array_x = None
    def __init__(self, name, bps, maxN=None, withErr = True, initArrays=True, xlabel='x', ylabel='y' ):
        """ bps: the BinnedPhaseSpace representing the bins of the phase space in which the histos are kept.
            hspec:  in the form (histo_nbins, (histo_min, histo_max) """
        self.maxN = maxN
        self.withErr = withErr
        self.xlabel=xlabel
        self.ylabel = ylabel
        BinnedArrays.__init__(self,name, bps,  initArrays,)

        
    def initArrays(self):                         
        shape = [a.nbins for a in self.bps.axis]+[self.maxN] # +2 : overflow and underflow bins 
        self.array_x = np.zeros( shape , dtype='float32')
        self.array_y = np.zeros( shape , dtype='float32')
        if self.withErr:
            self.array_yerr = np.zeros( shape , dtype='float32')
        self.npoints = np.ones( shape[:-1] ,dtype=int)*self.maxN

    def drawAt(self, coords, ax=None, err=True, **opts):
        from matplotlib import pyplot as plt
        if ax is None: ax=plt.gca()
        if self.array_x is None:self.loadContent()
        n=self.npoints[coords]
        x = self.array_x[coords][:n]
        y = self.array_y[coords][:n]        
        if self.withErr and err :            
            y_err = self.array_yerr[coords][:n]
            return ax.errorbar(x,y,y_err,**opts)
        else:
            return ax.plot(x,y,**opts)
                
