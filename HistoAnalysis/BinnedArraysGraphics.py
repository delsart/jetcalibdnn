from .BinnedArrays import BinnedHistos, BinnedArrays, BinnedPhaseSpace
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os
from copy import copy

#matplotlib.rcParams['axes.titlelocation']='right'
import imp
try:
    imp.find_module('mplcairo')
    hasMplCairo = True
except ImportError:
    hasMplCairo = False


class Page(object):
    """Base class to draw and save series of pages containing 1 or several plots each.
    This class is not to be instantiated alone.
    It is expected to be be inherited by a concrete class in the form
    class ConcretePage(Page,FormatClass) : ...
    where FormatClass is one of the class below and is used to specify how the multiple pages
    will be saved (1 single pdf file or multiple pdf/svg/png/root files).

    The buildPage() function below does that automatically to instantiate a concrete page object.    
    """
    nplots=1
    pname = ""
    curpad=0
    savedImg = []

    currentPageNum = 0
    
    graphObjects = [] # a list to keep temporary graphic objects 

    dir = ''
    
    def __init__(self, nplots=1):
        self.nplots = nplots
        self.savedImg = []
        self.graphObjects = []

    def ax(self):
        return self.figure.axes[self.curpad]
    
    def nextAx(self):
        """Change current active page to next ax within the TFigure  """
        self.curpad += 1
        self.saveIfFull()
        p=self.figure.axes[self.curpad]
        self.figure.sca(p)
        #print " moving to ",p, self.curpad 
        return p

    def resetFigure(self):
        #print ' resetting figure !!'
        # for a in self.figure.axes:
        #     a.cla()
        self.curpad=0
        self.figure.clf()
        self.ax_array=self.figure.subplots( *self.plotshape , squeeze=False)
        
        p=self.figure.axes[self.curpad]
        for a in self.figure.axes:
            a.clear()
        self.figure.sca(p)
        self.graphObjects = []        
    
    def saveIfFull(self, n=None):
        """ """
        if self.curpad == self.nplots :
            self.save(n)
            self.resetFigure()

    def forceSave(self, n = None):
        self.save(n)
        self.resetFigure()


    def setNplots(self, i):
        print( "Setting nplots ",i)
        self.figure.clear()
        if isinstance(i,int):
            self.nplots = i
            grid = [ (1,) , (2,) , (2,2), (2,2) , (2,3), (2,3) ] + [(3,3) ]*6
            i = grid[i-1]
        else:
            self.nplots = i[0]*i[1]
        self.plotshape = i
        self.resetFigure()

    def currentPad(self):
        return self.figure.axes[self.curpad]

    def setup_alt_figure(self):
        pass

    def allFigures(self):
        return [self.figure, ]
    
    def allAxes(self):
        return self.figure.axes
    
class ImgSaver(object):
    appendImgNum = True
    suffix = '.png'
    def open(self,n):
        self.dir , self.pname = os.path.split(n)
        if self.pname=='': self.pname = 'img'
        if self.dir and  not os.path.exists(self.dir):            
            os.makedirs(self.dir)
        
        
    def save(self, pname=None):
        pname = pname or self.pname
        if self.appendImgNum:
            pname = pname + '_'+str(self.currentPageNum)
        sn = os.path.join(self.dir, pname)+self.suffix
        #sn = self.dir+pname+self.suffix
        
        self.figure.savefig( sn  )
        self.savedImg.append(sn)
        self.currentPageNum += 1
        
    def close(self):
        pass


class QtSVGSaver(object):
    appendImgNum = True
    suffix = '.svg'
    def open(self,n):
        self.dir , self.pname = os.path.split(n)
        if self.pname=='': self.pname = 'img'
        if self.dir and  not os.path.exists(self.dir):            
            os.makedirs(self.dir)
        from PyQt5 import QtSvg, QtGui
        
    def save(self, pname=None):
        from PyQt5 import QtSvg, QtGui
        pname = pname or self.pname
        if self.appendImgNum:
            pname = pname + '_'+str(self.currentPageNum)
        sn = os.path.join(self.dir, pname)+self.suffix

        self.generator = QtSvg.QSvgGenerator()
        self.generator.setFileName(sn)
        
        self.painter = QtGui.QPainter(self.generator)
        # #self.painter.begin()            
        self.figure.canvas.render(self.painter)
        self.painter.end()
        # self.painter.eraseRect( self.figure.canvas.rect() )

        self.savedImg.append(sn)
        self.currentPageNum += 1
        
    def close(self):
        pass
    
class PdfSaver(object):
    suffix = '.pdf'
    figure_2 = None

    def open(self,name):

        self.name = name            
        #from matplotlib.backends.backend_pdf import PdfPages
        #self.pdfpage = PdfPages(name)
        if hasMplCairo:
            from mplcairo.multipage import MultiPage
            self.pdfpage = MultiPage( name )
        else:
            from matplotlib.backends.backend_pdf import PdfPages
            self.pdfpage = PdfPages(name)
            
        self.nPagePrinted=0

        from concurrent.futures import ThreadPoolExecutor    
        self.pool = ThreadPoolExecutor(max_workers=1)

        class PSeudoFutures:
            def result(self):
                pass
        self.future = PSeudoFutures()
        from copy import copy

    def setup_alt_figure(self):
        self.figure_2 = plt.figure( "pager_2", self.figure.get_size_inches() )
        d_prop =dict(self.figure.properties())
        d_prop2 =dict( (k,d_prop[k]) for k in ['agg_filter', 'alpha', 'animated',  'clip_on',  'constrained_layout', 'dpi', 'edgecolor', 'facecolor',  'in_layout', 'label', 'path_effects',  'snap', 'tight_layout', ])
            
        self.figure_2.set( **d_prop2)
        self.ax_array_2 = self.figure_2.subplots( *self.ax_array.shape)

    def allFigures(self):
        return [self.figure, self.figure_2]
        
    def allAxes(self):
        return self.figure.axes+self.figure_2.axes
        
        
    def save(self, pname):
        if self.figure_2 is None:
            self.setup_alt_figure()
        waitForPrevious = self.future.result()
        print( ' saving ',pname , self.figure)
        self.figure, self.figure_2 = self.figure_2, self.figure
        #self.pdfpage.savefig(self.figure)
        #pname = str(self.nPagePrinted) if pname is None else pname
        # self.figure.Print(self.name, "Title:"+ pname)
        #self.figure.title(pname)
        self.nPagePrinted += 1
        self.future = self.pool.submit( self._saver, "" )
        #

    def _saver( self, sn ):
        #print('__ ', id(self.figure_2) )
        self.pdfpage.savefig(self.figure_2)

    def close(self):
        waitForPrevious = self.future.result()
        
        self.pdfpage.close()

        print( "closing ",self.name)

class NoSaver(ImgSaver):
    def open(self,*l, **args):
        pass
    def saveIfFull(self):
        if self.curpad>=self.nplots:
            #     self.resetFigure()
            self.curpad =0
        pass
    def save(self, pname=None):
        pass
    
class SvgSaver(ImgSaver):
    suffix = '.svg'

class PdfImgSaver(ImgSaver):
    suffix = '.pdf'

class EpsImgSaver(ImgSaver):
    suffix = '.eps'

class RootMacroSaver(ImgSaver):
    suffix = '.root'

def buildPage(format, nplots, name, figure=None):
    print ('buildPage ', format, nplots, name)
    if format is None or format == "":
        format = NoSaver
    if format =='png':
        format = ImgSaver
    if format =='pdf':
        format = PdfSaver
        if not name.endswith('.pdf'): name = name+'.pdf'        
    if format =='svg':
        format = SvgSaver
    if format =='qtsvg':
        format = QtSVGSaver
        
    if figure is None:
        figure = plt.figure('pager', figsize=(12,9), ) #tight_layout=True) 
    # Generate a new type 'Pager' combining Page and the output format :
    Pager = type("Pager",(format,Page,), {}) 
    page = Pager()
    page.figure = figure
    figure.set_tight_layout(True)
    
    page.setNplots(nplots)
    page.resetFigure()
    page.open( name )
    if format == PdfSaver:
        page.setup_alt_figure()
    
    return page



def setupMPL(usePdf, interactive):
    bckend = matplotlib.get_backend()
    if usePdf:
        if hasMplCairo:
            matplotlib.use("module://mplcairo.base")
            matplotlib.rcParams['agg.path.chunksize'] = 1000
        plt.ioff()        
    elif not interactive:
        matplotlib.use("cairo")
        plt.ioff()
    else:
        plt.ion()
    return bckend



##**************************************
##**************************************

class BinnedArtist:
    """A base class to help to automatize  the drawing of BinnedArrays """
    def __init__(self):
        self.axes = []
        self.bps = None

    def setup(self, bps, baList, figs):
        self.bps = bps
        self.baList = baList # list of BinnedArray on which to operate
        for i,bh in enumerate(baList):
            bh.draw_i = i # just set the drawing order of this BinnedArrays so BinnedArtist can use it.
        try:
            figs = iter(figs)
        except:
            figs=[figs]
        for fig in figs:
            self.add_figure(fig)
        
    def add_figure(self, fig):
        self.axes += fig.axes
        
    def draw(self, bh, coord, ax, ):
        """Implement the draw operation for the  content of BinnedArray 'bh' at coordinates 'coord' and onto axes 'ax'
        This functions will be called for each bh to be drawn on a same ax .
        """
        return []

    def drawAtBin(self,  coord, ax, plotOpts={}):
        """Implement draw operations to be done only once per coordinates 'coord' on axes 'ax' 
        ex: draw text describing the bin at 'coord'
        """
        return []
    
class BAHistoDraw(BinnedArtist):
    """Draws histograms in each bin (assuming it is used on BinnedHistos)"""
    def setup(self, bps, baList, fig):
        self.label = getattr(baList[0], 'hdesc' ,'')
        
    def drawAtBin(self,  coord, ax, plotOpts={}):
        ax.set_xlabel(self.label)
        
    def draw(self, ba, coord, ax, ):
        self.curve=ba.drawAt( coord, same=True,  ax=ax, **ba.drawStyle)
        return self.curve
    

class BABinDesc(BinnedArtist):
    fontsize_def = "xx-large"
    def __init__(self, x0=0.63,y0=0.75,dy=0.06, fontsize=None,addBinN=False):
        super().__init__()
        self.x0=x0
        self.y0=y0
        self.fontsize=fontsize or self.fontsize_def
        self.addBinN  = addBinN
        
    def add_figure(self,  fig):
        super().add_figure(fig)
                
    def drawAtBin(self,  coord, ax, plotOpts={}):
        descs = self.bps.describeBin( coord )
        if self.addBinN: descs.append( str(coord) )
        fullStr='\n'.join(descs)
        ax.text(self.x0,self.y0,fullStr,transform=ax.transAxes,fontsize=self.fontsize)
        return []

class BALine(BinnedArtist):
    """Base class of drawing simple lines """
    def __init__(self,  ):
        super().__init__()
        self.lines = []
        self.tmpl = np.array( [[0,1],[0,1]] )
        
    def add_figure(self,  fig):
        super().add_figure(fig)


class BAGraphXY(BinnedArtist):
    """Draws the 2D graphs in each bin (assuming it is used on BinnedLine objects)"""
    def draw(self, ba, coord, ax, ):        
        self.curve=ba.drawAt( coord, ax=ax, **ba.drawStyle )
        return self.curve
        
class BARespLine(BALine):
    """Draws a vertical line at x = bh.respname[coord] """
    def __init__(self, respname='resp' ):
        super().__init__()
        self.respname = respname

    def setup(self, bps, baList, fig):
        super().setup(bps,baList,fig)
        self.label = getattr(baList[0], 'hdesc' ,'')
        #self.x_array =[ getattr(ba,self.respname) for ba in baList ]
        
    def draw(self, bh, coord, ax, plotOpts={}):
        #i = ax.figure.axes.index(ax)
        #x = self.x_array[bh.draw_i][coord]
        x=getattr(bh, self.respname)[coord]
        #l = self.lines[i][bh.draw_i]
        #print( l , id(l)  , x )
        #l.set_data([x,x],[0,ax.get_ylim()[1]] )
        ax.plot( [x,x],[0, ax.get_ylim()[1] ] , **bh.drawStyle)
        #ax.add_line(l)
        return []
        
class BALinesAt1(BinnedArtist):
    def __init__(self, y0=1, dy=0.01 ):
        super().__init__()
        self.y0=y0
        self.dy=dy
        self.lines = dict()
        self.setupLines()
        
    def setup(self, *l):
        pass
    
    def setupLines(self):
        L = matplotlib.lines.Line2D
        y0, dy = self.y0, self.dy
        self.lines=[L([0,1],[y0,y0],color='grey', linewidth=1, ) ,
                           L([0,1],[y0-dy,y0-dy],color='grey', ls='--',linewidth=0.5,) ,
                           L([0,1],[y0+dy,y0+dy],color='grey', ls='--',linewidth=0.5,),
                    ]
        
        
    def drawAtBin(self,  coord, ax, plotOpts={}):
        xlim = ax.get_xlim()
        for l in [copy(_l) for _l in self.lines]:
            l.set_xdata( xlim )
            ax.add_line(l)
        return []

    def drawOnAx(self,ax):
        self.drawAtBin(None, ax)

class BALinesAtX(BinnedArtist):
    def __init__(self, x=1. ):
        super().__init__()
        self.x=x
        self.line = None
        self.setupLines()
        
    
    def setupLines(self):
        L = matplotlib.lines.Line2D
        x=self.x
        self.line =L([x,x],[0,1],color='grey', linewidth=1,ls='--' ) 
        
        
    def drawAtBin(self,  coord, ax, plotOpts={}):
        ylim = ax.get_ylim()
        l = copy(self.line)
        l.set_ydata(ylim)
        ax.add_line(l)
        
        return []

    def drawOnAx(self,ax):
        self.drawAtBin(None, ax)
        
    


from os import path
class GraphicSession:
    """ Helper class to deal with multiple BinnedArrays : 
      - read them from file
      - methods to produce multiple plots containing multiple lines/graphs/histos...
    """

    inputDir = ''
    plotDir = ''


    colorStock = {'black', 'red', 'blue', 'green', 'purple', 'peru', 'gray', 'cyan', 'maroon'}
    
    # will be a list containing all BinnedArrays in the session
    allBA = None
    
    def __init__(self, **args):
        self.allBA = []
        for k,v in args.items():
            setattr(self,k,v)


    def outFile(self, outname):
        if self.plotDir: os.makedirs(self.plotDir, exist_ok=True)
        return path.join(self.plotDir, outname)



    def readBAFromFile(self,fname, loadContent=False, encoding=None, exclude='', tag='', **drawStyle):
        """Reads all BinnedArrays from file fname and sets then as members of self.
        Also assign BinnedArrays graphic style as given by **drawStyle options. 
          - if drawStyle is void a color is picked from self.colorStock
          - if drawStyle contains styleFunc : this function is used to generate the dict of styles.
        ex : 
         graphSess.readBAFromFile(fname, tag='myTag', color='blue', ls='--')
         graphSess.readBAFromFile(fname, styleFunc = lambda ba : dict(color='r', label=ba.name+'AA' ) )
        """
        _, allBA = BinnedArrays.readBAFromFile(fname,loadContent=loadContent,store=self,exclude=exclude, tag=tag)
        self.allBA += allBA

        styleFunc = drawStyle.pop('styleFunc', None)
        
        if styleFunc is None:
            styleFunc = lambda ba:drawStyle

        for ba in allBA:
            ba.tag = tag
            ba.drawStyle = styleFunc(ba)
            self.assertStyleHasColor(ba.drawStyle)
            ba.drawStyle.setdefault('label', tag)

    def assertStyleHasColor(self, drawStyle):
        c = drawStyle.get('color',None)
        if c is None:
            drawStyle['color'] = self.colorStock.pop()
        else:
            self.colorStock.discard(c)
        
        
    
    def drawBins(self, bhList, artists, outname='', coords=None,  nplots=4, page= None,  suffix='', **axOpts):
        """Generic function to draw content of several BinnedArrays in several plots, each plot corresponding to 1 coordinate of the phase space. 
        Typically, this is used to dump many plots in a pdf file or as a series of images.
        See drawManyGraphs and drawManyHistos for concrete examples. """
        #nplots=4 if coords is None or len(coords)>1 else 1
        bps= bhList[0].bps

        # guess the suffix if needed :
        if suffix=='':  suffix = outname.split('.')[-1] if '.' in outname else ''

        bckend = setupMPL( suffix=='pdf',  coords is not None)
        bckend = matplotlib.get_backend()

        
        # setup the Page which contain the matplotlib figure&axes
        if page is None:
            page = buildPage(suffix, nplots, self.outFile(outname) )
        else:
            page.setNplots( nplots )


        if not isinstance(nplots, int): nplots = nplots[0]*nplots[1]

        a0 = page.figure.axes[0]

        nBin0 = bps.a0.nbins
        if coords is None : coords = bps.iter()
        elif isinstance(coords, tuple):
            coords=bps.indices[coords]

        # prepare the artists ------------
        for a in artists:
            a.setup(bps, bhList, page.allFigures())


        count = 0
        grid = axOpts.pop('grid',None)        
        for c in coords:
            c = tuple(c)
            #print( c , repr(page.figure.gca()))
            ax = page.figure.gca()
            ax.cla()

            print('draw at ', c)
            for bh in bhList:
                for a in artists:
                    a.draw( bh,c, ax)
            for a in artists:
                a.drawAtBin( c, ax )
            ax.set( **axOpts)
            if grid: ax.grid(**grid)
            
            if (count%nplots)==0: ax.legend()

            if c[0]==nBin0:
                # restart from a blank page after we plotted a full pt slice
                page.forceSave()
                count=0
            else:
                page.nextAx()
                count +=1 
            #if count==100:break
        if suffix : page.close()
        self.page = page
        matplotlib.use(bckend)
        plt.ion()
        return page


    def drawManyHistos(self, bhList,  coords=None,  outname='', nplots=4, drawResp=False, addBinN=False, otherArtists=[], suffix='', **plotOpts):
        """Draw multiple plots of several histos from a list of BinnedHistos.
         * Loop on coordinates defined by 'coords' (an array of bin coordinates as in bps.indices[ something] )
         * For each coordinate  draw 1 plot containing the histos taken from 'bhList' at that coordinate.
         * if saving on file (when outname ends with '.pdf', '.png', '.svg') several pages or images maybe saved, with 'nplots' per page/image.

        ex : drawManyHistos( [mR_in_EMEta_uncal, mR_in_EMEta_dnn], outname='massResp.pdf', )
            -> draw ALL the histos in the 2 input BinnedHistos, with 4 plots per page in the PDF file 'massResp.pdf'

        ex : drawManyHistos( [mR_in_EMEta_uncal, mR_in_EMEta_dnn], coords=np.s_[4,:2,12], nplots=2)
           -> Draw 2 plots 1 for coordinate (4,0,12), 1 for (4,1,12) containing each the corresponding histos from the input BinnedHistos. No output file saved, only shown on an interactive window.
        """
        artists = [ BAHistoDraw(), BABinDesc(addBinN=addBinN), ]
        if drawResp:
            artists += [ BARespLine(drawResp)]
        artists += otherArtists
        return self.drawBins(bhList, artists, outname, coords,  nplots, suffix=suffix, **plotOpts)


    def drawManyGraphs(self, bhList, outname=None, coords=None,  nplots=4, drawLineAtY=(1,0.01) , addBinN=False, otherArtists=[], suffix='',**plotOpts):
        """Same as drawManyHistos but for Lines/Graphs """
        artists = [  BABinDesc(addBinN=addBinN), BAGraphXY(),  ]
        if drawLineAtY:
            y,dy = drawLineAtY
            artists +=[  BALinesAt1(y, dy) ]
        artists += otherArtists        
        plotOpts.setdefault('xlabel', bhList[0].xlabel)
        plotOpts.setdefault('ylabel', bhList[0].ylabel)
        return self.drawBins(bhList, artists, outname, coords, nplots, suffix=suffix, **plotOpts)


def asGraphicSessionMethod(func):
    setattr(GraphicSession, func.__name__, func)
