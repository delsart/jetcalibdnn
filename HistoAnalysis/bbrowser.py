# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'bbrowser.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication        
import sys

import matplotlib.pyplot as plt
plt.ion()


from .bbrowserUI import *
from . import BinnedArraysGraphics as bag
from . BinnedArrays import BinnedArrays, BinnedHistos, BinnedLine

qApp = QtWidgets.QApplication(sys.argv)

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

class MyMplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        fig.set_tight_layout(True)
        self.axes = fig.add_subplot(111)

        #self.compute_initial_figure()

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def compute_initial_figure(self):
        pass

    
    
class BinnedBrowserBase:

    def __init__(self, baList=[], axOpts={}):
        self.baList = baList
        self.phaseSpace = self.baList[0].bps

        self.diag= QtWidgets.QDialog()
        ui = Ui_Dialog()
        self.ui = ui
        ui.setupUi(self.diag)

        self.figCanvas = MyMplCanvas()
        self.figCanvas.setSizePolicy( 7, 7 )


        self.navtoolbar = NavigationToolbar(self.figCanvas, self.diag)

        ui.vLayout2.addWidget( self.navtoolbar)
        ui.vLayout2.addWidget( self.figCanvas )
        #ui.horizontalLayout.addWidget( self.figCanvas )
        
        self.ax0 = self.figCanvas.figure.axes[0]
        self.spinBoxes = [ui.spinBox_1, ui.spinBox_2, ui.spinBox_3, ui.spinBox_4]
        for sb, a in zip(self.spinBoxes,self.phaseSpace.axis):
            sb.setWrapping(True)
            sb.setMaximum( a.nbins+1 )
            sb.setMinimum( -1 )
            
        self.connexions()
        
        self.resetingBins = False
        self.currentBin = (0,0,0,0)
        self.savedBins = []
        self.prefix = ''
        for bh in self.baList:
            bh.loadContent()
        self.diag.show()

        self.axOpts = dict(**axOpts)
        
    def plt(self):
        self.ax0.plot( range(4), range(4) )
        self.figCanvas.draw()


    def connexions(self):
        ui = self.ui
        ui.drawButton.clicked.connect(self.drawCurrent)

        for b in self.spinBoxes:
            b.valueChanged.connect( self.dimChanged )

            
        
    def drawCurrent(self):
        self.draw(self.currentBin) # expecting to call  BPSPlots.draw
        self.figCanvas.draw()
        
            
        
    def dimChanged(self, *l):
        if not self.resetingBins: self.changeBin()

    def changeBin(self,):
        self.resetingBins =True
        bin = [b.value() for b in self.spinBoxes ]
        #print( ' changing  to ', bin)
        axis = self.phaseSpace.axis
        def validateBin( bin ):            
            for i, ax in enumerate(self.phaseSpace.axis):
                if bin[i]==-1:
                    bin[i+1] = max(bin[i+1]-1,0)
                    bin[i] = ax.nbins-1
                    return validateBin(bin)
                if bin[i] == ax.nbins :
                    bin[i]=0
                    bin[i+1] +=1
            if bin[-1] >= axis[-1].nbins:
                bin[-1]=max(0,axis[-1].nbins-1)
            return bin
        bin = validateBin(bin)
        #print("   --> reset to ", bin)
        for i in (0,1,2,3):
            self.spinBoxes[i].setValue(bin[i])
            
        self.currentBin = tuple(bin)[:self.phaseSpace.nDim()]
        self.drawCurrent()
        self.resetingBins =False        


    def saveBin(self, base, suffix='.svg',bin=None):
        if bin is not None:
            self.currentBin=bin
            self.drawCurrent()
            
        self.figCanvas.figure.savefig(base+self.phaseSpace.tagAt(self.currentBin)+suffix)

class BinnedBrowser(BinnedBrowserBase):
    artists = [] # set automatically in ctor below

    def __init__(self, baList=[], axOpts={}, moreArtists=[],fontsize='large', legpos='best'):
        super().__init__(baList, axOpts)
        ba0 = baList[0]
        if isinstance(ba0, BinnedHistos):
            self.artists = [bag.BAHistoDraw() , bag.BABinDesc(addBinN=True, y0=0.75,fontsize=fontsize) ]
        elif isinstance(ba0, BinnedLine):
            self.artists = [bag.BAGraphXY() , bag.BALinesAt1(), bag.BABinDesc(addBinN=True, y0=0.5,fontsize=fontsize) ]
        self.artists += moreArtists
        for a in self.artists:
            a.setup(self.phaseSpace, baList, self.figCanvas.figure)
        self.fontsize=fontsize
        self.legpos = legpos
        
    def add_artist(self, a):
        a.setup(self.phaseSpace, self.baList, self.figCanvas.figure)
        self.artists.append(a)
        
    def draw(self, bin ):
        #print( bin )
        self.ax0.cla()
        a0= self.artists[0]
        leglines=[]
        for bh in self.baList:
            leglines += [ a0.draw(bh, coord= bin, ax=self.ax0 ) ]
        #self.ax0.legend(fontsize=self.fontsize)
        self.ax0.legend(handles=leglines,fontsize=self.fontsize, loc=self.legpos)
        for a in self.artists:
            a.drawAtBin(bin, ax=self.ax0)
        self.ax0.set( **self.axOpts)

