from tensorflow.keras.optimizers import SGD
from tensorflow.keras.legacy import interfaces
from tensorflow.keras import backend as K
class PerturbSGD(SGD):
    def __init__(self, randScale=0.05, **args):
        self.randScale=K.variable(randScale, name='randScale')
        SGD.__init__(self,**args)
    @interfaces.legacy_get_updates_support
    def get_updates(self, loss, params):
        grads = self.get_gradients(loss, params)
        self.updates = [K.update_add(self.iterations, 1)]

        lr = self.lr
        if self.initial_decay > 0:
            lr = lr * (1. / (1. + self.decay * K.cast(self.iterations,
                                                      K.dtype(self.decay))))
        # momentum
        shapes = [K.int_shape(p) for p in params]
        moments = [K.zeros(shape) for shape in shapes]
        self.weights = [self.iterations] + moments
        for p, g, m in zip(params, grads, moments):
            v = self.momentum * m - lr * g  # velocity
            self.updates.append(K.update(m, v))

            r = K.stop_gradient(K.random_normal(K.shape(p),stddev=self.randScale*K.mean(K.abs(v)) ))
            #r = K.print_tensor(r, 'rnd')
            #v = K.print_tensor(v, 'vvv')
            if self.nesterov:
                new_p = p + self.momentum * v - lr * g +r
            else:
                new_p = p + v +r

            # Apply constraints.
            if getattr(p, 'constraint', None) is not None:
                new_p = p.constraint(new_p)

            self.updates.append(K.update(p, new_p))
        return self.updates
    
