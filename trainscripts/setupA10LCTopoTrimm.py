# this file is meant to be included from the main jetCalibDNN.py script 

conf.update(
    nnName = "AntiKt10LCTopo" ,
    inputFiles = 'AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets_*.root',
    treeName = "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets" ,
    features = ['eta', 'e_var','LogMoE','NPV', "effNConst", "D2", "EMFrac", "Qw"],
    
)    
Variable.add_alias("EffNConsts", 'effNConst' )
Variable.referenceVars.EffNConsts.src_name = 'effNConst'
