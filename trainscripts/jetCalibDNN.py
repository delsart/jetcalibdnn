## ######################################################333
## Top level scripts.

## defines a Trainer object, a configuration and training sequences.
##
## typically used interactively from python3 -i jetCalibDNN.py
##
##  PLEASE SEE README FOR USAGE EXAMPLES
##
import sys
import numpy as np
import h5py

# Import Keras (either standalone, either as embedded in tensorflow )
from JetCalibDNN.ImportKeras import *
import JetCalibDNN.NNUtils as utils




from JetCalibDNN.ConfigUtils import defaultConfig
from JetCalibDNN  import ModelDefinitions
from JetCalibDNN.GeneratorFromRootFile import ROOTDataGeneratorMT, Generator1TargetMT, Generator2TargetMT
from JetCalibDNN.Variables import Variable, dataPartitions, genVarW
from JetCalibDNN import NNUtils


from JetCalibDNN.Trainer import Trainer

## ***********************************************************************
## Configuration
## ***********************************************************************

conf=defaultConfig.clone(

    # **********************
    # the options below are necessary : we set them in a dedicated file to allow easy switching from
    # jet one collection to another. See variableFile below
    #nnName = "" ,     SET in variableFile 
    #inputFiles = '',  SET in variableFile
    #treeName = "" ,   SET in variableFile
    #features = [],     SET in variableFile

    # **********************
    
    inputDir='data/',
    outputDir='',

    # define the input reading classes for 1 or 2 targets : 
    inputClassList = [ Generator1TargetMT, Generator2TargetMT ],
    # ------------    
    
    # ------------
    targets=['r_e', 'r_m'],
    # ------------
    useWeights = False,

    # ------------
    lossT = -1.5, # obsolete

    
    #lossType = 'LGK', # Trainer will use this to set conf.nPredictedParam
    lossType = 'MDN', # Trainer will use this to set conf.nPredictedParam 
    
    #nPredictedParam = True, # this is set according to lossType in Trainer.setupConfig. When true then, the model MUST output 2 or 4 variables. Forced to false when isNIstep is on.

    
    
    # -------------------

    #modelTag = '', set automatically by the object passed as modelBuilder
    optimTag = '',
    
    callbacks = [
        #keras.callbacks.EarlyStopping(monitor='loss', patience=2, min_delta=0.01),
        keras.callbacks.ModelCheckpoint('tmp.hdf5', monitor='loss', mode='min',save_best_only=True,verbose=1),
        #keras.callbacks.TensorBoard(log_dir='tensorboard/', histogram_freq=1, write_graph=True,update_freq='batch',write_images=True),
        #CyclicLR(0.0001, 0.05, 0, mode='triangular'),
        #cyclAdam,
        #cycLR1C,#.set(max_lr=0.06,base_lr=0.00001,batch_size=150000),
    ],

    # -------------------
    lr = None,  # unused (???)
    # -------------------
    isNIstep = False, # set automatically to true when inputFriendsPrefix starts with 'ni'

    # ----------------

    # ----------------
    doLookahead = True,

    # -----------------
    ngpu = 1,

    # --------------------
    debug=False,

    #sampleWeights='EMweights',
    #sampleWeights='Eweights',    
    specialTag='',
    
    #modelBuilder = ModelDefinitions.modelDefinition1,
)

NNUtils.switch_debugging( conf.debug)


if 'variableFile' not in dir():
    if sys.argv[-1]!='jetCalibDNN.py':
        variableFile = sys.argv[-1]
    else:
        # variableFile = 'setupA10CSSKUFOsoftDrop.py'
        # variableFile = 'setupAntiKt4EMPFlowJets.py'
        variableFile = 'setupCluster.py'

print(" Including ", variableFile)
    
exec(open(variableFile).read(), )

#****************************************
    

if conf.sampleWeights=='EMweights':
    conf.specialTag = 'EMw'
if conf.sampleWeights=='Eweights':
    conf.specialTag = 'Ew'
if conf.sampleWeights=='LogEweights':
    conf.specialTag = 'LogEw'
if conf.sampleWeights=='eventWeight':
    conf.specialTag = 'evw'


    
trainer = Trainer()

prefix =''


## ******************************************************************
## define the data partitions we may use (see Variables.py)
## format is
#   part_name = ( dict_preFilters, dict_postFilters) ,
# where
#  dict_preFilters = dict( var_name1 = filter_function, var_name2 = filter_function2, ...)
## ******************************************************************
dataPartitions.update(
    central = ( dict(
        eta = lambda eta : np.fabs(eta)<0.8 ,
    ), {} ) ,
    #forward0 = ( dict( eta = lambda eta :  np.logical_and( np.fabs(eta)>0.8 , np.fabs(eta)<1.2)  ), {} ),
    crack1 = ( dict( eta = lambda eta :  np.logical_and( np.fabs(eta)>0.85 , np.fabs(eta)<1.15)  ), {} ),
    crack2 = ( dict( eta = lambda eta :  np.logical_and( np.fabs(eta)>1.2 , np.fabs(eta)<1.7)  ), {} ),
    forward2 = ( dict( eta = lambda eta :  np.fabs(eta)>1.7 ) , {} ),
    forward3 = ( dict( eta = lambda eta :  np.fabs(eta)>3 ) , {} ),
    lowE   = ( dict(e_var = lambda e : np.logical_and( e>100 , e<500)  ), {} ) ,
    noHighE   = ( dict(e_var = lambda e : e<3000  ), {} ) ,
    
    lowMoE = ( {}, dict( MoE = lambda MoE : MoE<0.04  )  ),
    highE   = ( dict(e_var = lambda e : e>1500  ), {} ) ,
    VhighE   = ( dict(e_var = lambda e : e>2500  ), {} ) ,

    highPT = ( dict(pt_true = lambda pt : pt>2000) ,{}),

    verlowE = ( dict(e_reco = lambda e : e < 30), {}   ),
    verlowEtrue = ( dict(e_true = lambda e : e < 30), {}   ),
    #lowMoE = ( {}, dict( MoE = lambda MoE : MoE<0.04  )  ),        
)


def partEtaRange(etamin, etamax):
    """generates a partition. Use it as trainer.refit( ..., partition=partEtaRange(1.4,2.0),...)  """
    k = f"etaRange{etamin}_{etamax}"
    import numexpr as ne
    if not k in dataPartitions:
        dataPartitions[k] = ( dict(eta=lambda eta : ne.evaluate( f"(eta>{etamin})&(eta<{etamax})", local_dict=dict(eta=eta)) ), {} )
    return k

def partAbsEtaRange(etamin, etamax):
    """generates a partition. Use it as trainer.refit( ..., partition=partEtaRange(1.4,2.0),...)  """
    k = f"absetaRange{etamin}_{etamax}"
    import numexpr as ne
    if not k in dataPartitions:
        dataPartitions[k] = ( dict(eta=lambda eta : ne.evaluate( f"(abs(eta)>{etamin})&(abs(eta)<{etamax})", local_dict=dict(eta=eta)) ), {} )
    return k

def partLowEcentral(emax, etamax):
    """generates a partition. Use it as trainer.refit( ..., partition=partEtaRange(1.4,2.0),...)  """
    k = f"lowecentr{emax}_{etamax}"
    import numexpr as ne
    if not k in dataPartitions:
        dataPartitions[k] = ( dict(eta=lambda eta : ne.evaluate( f"(abs(eta)<{etamax})", local_dict=dict(eta=eta)),
                                   e_true=lambda e : ne.evaluate( f"e<{emax}")), {} )
    return k


def partHighPt(ptmin):
    """generates a partition. Use it as trainer.refit( ..., partition=partHighPt(3000),...)  """
    k = f"highPT{ptmin}"
    if not k in dataPartitions:
        dataPartitions[k] = ( dict(pt_true=lambda pt:pt>ptmin), {} )
    return k

def partLowPt(ptmax):
    """generates a partition. Use it as trainer.refit( ..., partition=partHighPt(3000),...)  """
    k = f"lowPT{ptmax}"
    if not k in dataPartitions:
        dataPartitions[k] = ( dict(pt_true=lambda pt:pt<ptmax), {} )
    return k


#exec(open("specifics.py").read() )


## ******************************************************************
## fit sequence definitions :
## ******************************************************************

class MyRectifiedAdam(tfa.optimizers.RectifiedAdam):
    def _resource_apply_dense(self, grad, var):
        var_dtype = var.dtype.base_dtype
        lr_t = self._decayed_lr(var_dtype)
        #tf.print('fffffffffff ',lr_t, self.iterations , )

        return super(MyRectifiedAdam,self)._resource_apply_dense(grad,var)



def sgd(lr,momentum=0.9):
    return keras.optimizers.SGD(lr=lr,momentum=momentum)
def nada(lr):
    return keras.optimizers.Nadam(lr=lr)
    
def rada(lr):
    return tfa.optimizers.RectifiedAdam(lr=lr)
radaOpt = rada

def myradaOpt(lr, decay=0.1):
    return MyRectifiedAdam(lr=lr, decay=decay)

def adaBel(lr, eps=1e-8):
    from JetCalibDNN.AdaBelief import RadaBelief
    return RadaBelief(learning_rate=lr, epsilon=eps, warmup_steps=500)

def radaOptW(minlr, maxlr=None, w=0.05):
    maxlr = maxlr or minlr*5.
    return lambda trainer : tfa.optimizers.RectifiedAdam(lr=maxlr, total_steps=len(trainer.gen), warmup_proportion=w, min_lr=minlr)   

def myradaOptW(minlr, maxlr=None, w=0.2):
    maxlr = maxlr or minlr*5.
    #return lambda trainer : MyRectifiedAdam(lr=maxlr, total_steps=len(trainer.gen), warmup_proportion=w, min_lr=minlr)
    return lambda trainer : MyRectifiedAdam(lr=maxlr, total_steps=10, warmup_proportion=w, min_lr=minlr)   

def diffgrad(lr):
    from JetCalibDNN.DiffGradOptimizer import DiffGrad
    return DiffGrad(lr)

def profSequence(trainer):
    trainer.refit( batch_size =500,
          epochs=1,
          steps_per_epoch=200,
          callbacks = [utils.tensorBoardProfile()] )


def testSequence(trainer):
    trainer.loglossnseen.ninterval=3
    if trainer.config.lossType=="MDNA":
        lF = [utils.amdnLoss, utils.amdnLoss]
        if trainer.config.optimTag=='directSF':
            lF = [utils.amdnLossSF, utils.amdnLossSF]
    elif trainer.config.lossType=="LGK":
        lF = [utils.lgk1Loss(1e-3,1e-3) , utils.lgk1Loss(1e-3,1e-3) ]
    else:
        lF = utils.mdnLoss        
    trainer.refit( batch_size = 1000, epochs=3 , steps_per_epoch=100, optimizer=radaOpt(lr=0.001), loss=lF)

def testSequenceLKG(trainer):
    trainer.loglossnseen.ninterval=100
    if trainer.config.lossType=="MDNA":
        lF = [utils.mdnLoss, utils.amdnLoss]
    else:
        lF = utils.mdnLoss        
    trainer.refit( batch_size = 1000, epochs=3 , steps_per_epoch=10, optimizer=myradaOpt(lr=0.001), loss=lF)
    
def defaultSequence(trainer):
    args = trainer.config.refitParams
    
    #trainer.config.callbacks+=[ utils.earlyStopLoss , keras.callbacks.TerminateOnNaN()]
    #if not trainer.gen.singleTarget: utils.earlyStopLoss.lossT = -5.5
    print("aaaaa0")

    if trainer.config.lossType=="LGK":
        loss = utils.lgkLoss(1e-3, 1e-3)
    elif trainer.config.lossType=="MDN":
        loss = utils.mdnLoss
    elif trainer.config.lossType=="MDNA":
        loss = [utils.amdnLoss, utils.amdnLoss]
        if trainer.config.optimTag=='directSF':
            loss = [utils.amdnLossSF, utils.amdnLossSF]
        #loss = utils.mdnLoss if trainer.config.nPredictedParam else utils.lgkLoss(1e-3, 1e-3)
    trainer.refit( batch_size = 1000, epochs=1 , steps_per_epoch=30000, optimizer='rada', loss=loss, **args)
    if trainer.net.stop_training:
        print("ERROR")
        return
    print("aaaaa1")
    # T = 2 if trainer.gen.singleTarget else 10
    # if trainer.lossHistory[-1][0] > T:
    #     trainer.refit( batch_size = 3000, epochs=1 , steps_per_epoch=30000, optimizer='rada', loss=trainer.config.loss)
    #print("FAILURE  000 ")
        #return
    trainer.save_model()
    #return
    if trainer.config.nPredictedParam>1:
        refitSequenceMDN(trainer)
    else:
        refitSequenceLGK(trainer)

    # Freeze E related weights : 
    #trainer.freezeAncestorLayers("outputE")

def refitSequenceLGK(trainer):
    args = trainer.config.refitParams
    lossFuncs = [utils.lgk1Loss(1e-3,1e-3), utils.lgk1Loss(1e-3,1e-3)]
    trainer.refit( batch_size =15000,  optimizer=radaOpt(1e-3), epochs=3, loss=lossFuncs, **args )
    trainer.save_model()
    trainer.refit( batch_size =35000, optimizer=radaOpt(1e-4), epochs=3, loss=lossFuncs, **args )
    trainer.save_model()
    trainer.refit( batch_size =95000,  optimizer=radaOpt(1e-4), nepoch=3 , loss=lossFuncs  , **args)
    #trainer.refit( batch_size =5000,  optimizer=radaOpt(1e-4), nepoch=3 ,  loss=lossFuncs  , **args)
    trainer.save_model()                                                                              
    trainer.refit( batch_size =15000,  optimizer=radaOpt(1e-4), nepoch=2 ,  loss=lossFuncs, **args )
    trainer.refit( batch_size =95000,  optimizer=radaOpt(1e-4), nepoch=2 , loss=lossFuncs, **args )
    trainer.save_model()
    print("aaaaa4")

def refitSequenceMDN(trainer):
    args = trainer.config.refitParams

    if trainer.config.lossType=="MDNA":
        lossFunc = lambda t: utils.amdnLoss
        if trainer.config.optimTag=='directSF':
            loss = [utils.amdnLossSF, utils.amdnLossSF]        
    elif trainer.config.lossType=="MDN":
        lossFunc = lambda t: utils.truncmdnLoss(t)
    trainer.refit( batch_size =15000,  optimizer=radaOpt(1e-3), epochs=2, loss=[lossFunc(None), lossFunc(None)] , **args)
    print("aaaaa2")
    trainer.save_model()
    trainer.reweightAnomalousRow()
    trainer.refit( batch_size =25000, optimizer=radaOpt(1e-4), epochs=2, loss=[lossFunc(None ), lossFunc(None)], **args )
    print("aaaaa3")
    trainer.save_model()
    trainer.reweightAnomalousRow()
    trainer.refit( batch_size =35000,  optimizer=radaOpt(1e-4), nepoch=2 , loss=[lossFunc(3), lossFunc(3) ]  ,  metrics=[utils.lgkLoss(1e-3,1e-3)], **args  )
    trainer.save_model()
    trainer.reweightAnomalousRow()
    trainer.refit( batch_size =15000,  optimizer=radaOpt(1e-4), nepoch=2 , loss=[lossFunc(1.5), lossFunc(1.5) ]  , metrics=[utils.lgkLoss(1e-3,1e-3)], **args  )
    trainer.save_model()
    print("aaaaa4")

    return 



    

def defaultSequenceNI(trainer):
    trainer.config.callbacks+=[  keras.callbacks.TerminateOnNaN()]
    trainer.refit( batch_size = 1000, epochs=1 , optimizer='rada',steps_per_epoch=10000, loss='mse')
    if trainer.lossHistory[-1] > 2:
        print("FAILURE  000 ")
        return
    trainer.save_model()
    refitSequence0NI(trainer)
    
def refitSequence0NI(trainer):
    trainer.refit_noMP( batch_size =5000,  optimizer='rada', epochs=3, loss='mse')
    if trainer.net.stop_training: return
    trainer.save_model()
    trainer.refit( batch_size =25000, optimizer='rada1e4', epochs=3, loss='mse')
    if trainer.net.stop_training: return
    trainer.save_model()

    if trainer.lossHistory[-1] > 0.01:
        print (" FAILURE 333")
        return
    trainer.save_model()
    trainer.reweightAnomalousRow()
    trainer.refit( batch_size =15000,  optimizer='rada1e4', epochs=5, loss='mse' )
    if trainer.net.stop_training: return
    trainer.refit( batch_size =45000,  optimizer='rada1e4', epochs=3, loss='mse' )
    if trainer.net.stop_training: return
    trainer.refit( batch_size =5000,  optimizer='rada1e4', epochs=3, loss='mse' )
    if trainer.net.stop_training: return
    trainer.reweightAnomalousRow()
    trainer.save_model()


    




    

## ******************************************************************
## helpers and debugging functions
## ******************************************************************

def get_weight_grad(model, inputs, outputs ,w):
    """ Gets gradient of model for given inputs and outputs for all weights"""
    grads = model.optimizer.get_gradients(model.total_loss, model.trainable_weights)
    symb_inputs = (model._feed_inputs + model._feed_targets + model._feed_sample_weights)
    f = K.function(symb_inputs, grads)
    x, y, sample_weight = model._standardize_user_data(inputs, outputs,w)
    output_grad = f(x + y + sample_weight)
    return output_grad

def testInputValidity(trainer):
    x,y=trainer.gen.current_unfiltered_sample(formatted=False)
    N=trainer.gen.current_file.Nentries
    if np.any(np.isnan(x[:N])):
        print("nan x error at ",x[:N]) ; return
    if np.any(np.isinf(x)):
        print("inf x error at ",x[:N]) ; return
    if np.any(np.isnan(y[0][:N])):
        print("nan y0 error at ",y[0][:N]) ; return
    if np.any(np.isinf(y[0][:N])):
        print("inf y0 error at ",y[0][:N]) ; return
    if np.any(np.isnan(y[1][:N])):
        print("nan y1 error at ",y[1][:N]) ; return
    if np.any(np.isinf(y[1][:N])):
        print("inf y1 error at ",y[1][:N]) ; return
    

def testXValidity(trainer):
    N=trainer.gen.current_file.Nentries    
    for f in trainer.config.features:
        vname = Variable.referenceVars[f].name
        v = trainer.gen.varDict[vname]
        if np.any(np.isnan(v.array[:N])):
            print("nan error for ",f, v.array[:N]) ;


def testTrapez():
    a=np.array( [[ -0.6, 0,],
               [-0.4, 0.1],
               [0.05, 0.02] ])
                
    l = utils.TrapezoidAnnotation( 0, a )
    l.build( (5,1) )
    inputs = np.array([ [ -1, -0.5, -0.1, 0.012, 0.05, 0.4] ], dtype=np.float32).T
    return inputs, l 

def testAnnot():
    x=np.stack([np.linspace(-1,1,200)]*17,1)
    l=trainer.net.layers[1]
    exec(open('plotAndDebugDNN.py').read()) ; 
    plt.clf()
    return plt.plot(x,l.test(x))









# trainer.train(conf.update(inputFiles='AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets_*root' , outputDir='' , nInputFiles = -1 , modelBuilder =ModelDefinitions.HeadEtaBlockCoreT0Tail1(N = 750, coverage= 'full' ), sampleWeights = None, mode= 'reco:predict' ), reload = 1, fitSequence = defaultSequence)

# Adjust offset and scale factors. Check the normalized histo with :
# exec(open('plotAndDebugDNN.py').read())
# trainer.histogramInputs(formatted=True)


def removeMassRespFilter(trainer, lastMlayer="outputM"):
    trainer.freezeAncestorLayers(lastMlayer)

    try:
        trainer.gen.chain_workers[0].allVars.r_raw_m.post_filter = []
        trainer.gen.chain_workers[1].allVars.r_raw_m.post_filter = []
    except:
        trainer.gen.chain.allVars.r_raw_m.post_filter = []
        

def mdnTolgk(trainer, lastLayers=None):

    def parentOf(lname):
        return trainer.net.get_layer(lname).inbound_nodes[0].inbound_layers        

    if lastLayers is None:
        lastLayers =[ parentOf('outputE').name,
                      parentOf('outputM').name
                     ]

    
    trainer2 = trainer.copy(lossType='LGK', targets=['r_e', 'r_m'],
                            # do not copy weights from the last layers.
                            layerFilter = lambda l : l.name in  lastLayers
                            #layerFilter = ff,
                            )

    def copyRespWeight(lname):
        # l = trainer.net.get_layer(lname).inbound_nodes[0].inbound_layers        
        # l2 = trainer2.net.get_layer(lname).inbound_nodes[0].inbound_layers
        l = trainer.net.get_layer(lname)
        l2 = trainer2.net.get_layer(lname)
        w , b = l.get_weights()
        
        l2.set_weights( (w[:,:1], b[:1]) )

    # copyRespWeight( 'outputE' )
    # copyRespWeight( 'outputM' )
    for lname in lastLayers:
        copyRespWeight(lname)
    
    # assume trainer was trained well enough
    utils.reset_regularizer(trainer2.net, 1e-5 )

    return trainer2


def freezeSigmaEval(trainer):

    trainer.config.modelBuilder.forceNPredValues = 1
    netMu = trainer.config.modelBuilder(trainer)

    trainer.config.modelBuilder.forceNPredValues = 2
    netSigma = trainer.config.modelBuilder(trainer)
    netSigma.trainable = False

    def parentOf(lname):
        return trainer.net.get_layer(lname).inbound_nodes[0].inbound_layers        


    lastLayers =[ parentOf('outputE').name,                 ]
    if len(trainer.config.originalTargets)==2:
        lastLayers+=[ parentOf('outputM').name ]
        
    utils.copyWeights( trainer.net, netMu, nLayer=-1,
                       layerFilter = lambda l : l.name in  lastLayers
                      )
    utils.copyWeights( trainer.net, netSigma, nLayer=-1,
                       layerFilter = lambda l : l.name in  lastLayers
                      )
        
    def copyLastWeight(src, dst, lname, resp=True):
        # l = trainer.net.get_layer(lname).inbound_nodes[0].inbound_layers        
        # l2 = trainer2.net.get_layer(lname).inbound_nodes[0].inbound_layers
        ls = src.get_layer(lname)
        ld = dst.get_layer(lname)
        w , b = ls.get_weights()
        
        if resp :
            ld.set_weights( (w[:,:1], b[:1]) )
        else:
            ld.set_weights( (w[:,1:], b[1:]) )

    for lname in lastLayers:
        copyLastWeight(trainer.net, netMu,lname,True)
        copyLastWeight(trainer.net, netSigma,lname,False)

    outMu = netMu(netMu.inputs)
    outSig = netSigma(netMu.inputs)
    
    out= layers.concatenate( [outMu, outSig ] )
    
    net = Model(inputs=netMu.inputs, outputs=out)

    trainer.net_o = trainer.net
    trainer.net = net
    utils.reset_regularizer( net, 0.)
    return net
    
def reweightLastEDense(trainer, respF, lname):
    l = trainer.net.get_layer(lname)
    w,b = l.get_weights()

    f = respF*trainer.outscaler.full_scale_factor()
    l.set_weights( (w*f, b*f) )

def trainOnlySplitLayer(index, tag='E', base='dense_split'):
    if isinstance(index,int): index = [index]
    trainer.setTrainableStatus(False) # freeze all
    # unfreeze splited layers
    for i in index:
        l = trainer.net.get_layer(f'{base}{tag}{i}')        
        l.trainable = True
        try:
            l = trainer.net.get_layer(f'output{tag}{i}')        
            l.trainable = True
        except:
            pass
        

        
def highPredFraction(trainer,i=0,force=False):
    if trainer.last_predictions is  None or  force:
        trainer.gen.loadFile(0)
        trainer.fullPredictions()
    return np.sum(trainer.last_predictions[:,i]>1.9)/len(trainer.last_predictions[:,i])
