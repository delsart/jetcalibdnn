#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
#echo $SCRIPT_DIR
TOP_DIR=${SCRIPT_DIR%/*}
TOP_DIR=${TOP_DIR%/*}

#echo $TOP_DIR

ln -s $TOP_DIR/trainscripts/jetCalibDNN.py .
ln -s $TOP_DIR/histoscripts/plotAndDebugDNN.py .
ln -s $TOP_DIR/trainscripts/clustertraining/buildBHfromTrainerCL.py .



echo "Links to main JetCalibDNN scripts done"
echo "You probably want to copy one of these scripts to specialize your trainings : "
ls  $TOP_DIR/trainscripts/clustertraining/setupCluster.py 


PPATH=${TOP_DIR%/*}
cat <<EOF > setupPATH.sh
export PYTHONPATH=$PYTHONPATH:$PPATH
EOF


echo "now do 'source setupPATH.sh' to setup your PYTHONPATH"
