# this file is meant to be included from the main jetCalibDNN.py script 

conf.update(
    nnName = "CSSKUFOSoftDrop" ,
    inputFiles = 'CSSKUFOSoftDrop_*.root',
    #inputFiles = 'CSSKUFOSoftDrop_1.root,',
    treeName = "IsoJetTree" ,

    #features = ['eta', 'e_var','m_var', "EffNConsts", "EffNTracks", "D2", "EMFrac", "Qw", 'sumPtTrkFrac', "Tile0Frac", "groomMratio", "neutralFrac", "EM3Frac", "mu", 'NPV', ]#"Tau32", "pt_reco"],    # !!!
    #features = ['eta', 'e_var','m_var', "EffNConsts", "EffNTracks", "D2", "EMFrac", "Qw", 'sumPtTrkFrac', "Tile0Frac", "groomMratio", "neutralFrac", "EM3Frac", "mu", 'NPV', "Tau32", "pt_reco"],    # !!!
    #features = ['eta', 'e_var','m_var', "EffNConsts", "EffNTracks", "D2", "EMFrac", "Qw", 'sumPtTrkFrac', "Tile0Frac", "groomMratio", "neutralFrac", "EM3Frac", "mu", 'NPV',  'HighE'],    # !!!
    #features = ['eta', 'e_var','m_var', "EffNConsts", "EffNTracks", "D2", "EMFrac", "Qw", 'sumPtTrkFrac', "Tile0Frac", "groomMratio", "neutralFrac", "EM3Frac", "mu", 'NPV', 'e_reco_nolog'],    # !!!
    features = ['eta', 'e_var', 'm_var', 'Width', 'EMFrac', 'EM3Frac', 'Tile0Frac', 'EffNConsts', 'groomMratio', 'neutralFrac', 'sumPtTrkFrac', 'sumMassTrkFrac', 'Split12', 'Split23', 'C2', 'D2', 'Tau32', 'Tau21', 'Qw', 'mu', 'NPV', ]
)


refV=Variable.referenceVars    
    
# adapt the variable src_name to the source name in the input file :
for v in [ 'Width', 'EMFrac', 'EM3Frac', 'Tile0Frac', 'EffNConsts', 'groomMratio', 'neutralFrac', 'sumPtTrkFrac', 'sumMassTrkFrac', 'Split12', 'Split23', 'C2', 'D2', 'Qw' ]:
    refV[ v ].src_name = 'jet_'+v

refV.Tau32.src_name = 'jet_Tau32_wta'
refV.Tau21.src_name = 'jet_Tau21_wta'

refV.e_true.src_name = 'jet_true_E'
refV.pt_true.src_name = 'jet_true_pt'

refV.m_true.src_name = 'jet_true_mass'
refV.e_reco.src_name = 'jet_E'
refV.pt_reco.src_name = 'jet_pt'
refV.m_reco.src_name = 'jet_mass'



refV.eta.src_name = 'jet_eta'
refV.eta_true.src_name = 'jet_true_eta'
refV.mu.src_name = 'actualMu'
refV.NPV.src_name = 'NPV'
refV.PID.src_name = 'jet_PartonTruthLabelID'
refV.eventWeightXS.src_name = 'weight'


# Below we adapt the reference variables to our sample
#  - adjust the scale factors and offsets of variable normalizations

# Adjust offset and scale factors. Check the normalized histo with :
# exec(open('plotAndDebugDNN.py').read())
# trainer.histogramInputs(formatted=True)
"""
refV.eta.setScaleFactors(scaleFactor=3.600, offset=0.000 ) 
refV.e_reco.setScaleFactors(scaleFactor=2.00, offset=0.100 ) 
refV.m_reco.setScaleFactors(scaleFactor=3.000, offset=0.100 ) 
refV.EffNConsts.setScaleFactors(scaleFactor=3.000, offset=-0.500 ) 
refV.D2.setScaleFactors(scaleFactor=3.000, offset=-0.400 ) 
refV.C2.setScaleFactors(scaleFactor=3.000, offset=-0.400 ) 
refV.EMFrac.setScaleFactors(scaleFactor=3.000, offset=0.000 ) 
refV.Qw.setScaleFactors(scaleFactor=1.500, offset=-0.500 ) 
refV.sumPtTrkFrac.setScaleFactors(scaleFactor=2.800, offset=-0.200 ) 
refV.Tile0Frac.setScaleFactors(scaleFactor=3.400, offset=-0.500 ) 
refV.groomMratio.setScaleFactors(scaleFactor=1.200, offset=0.100 ) 
refV.neutralFrac.setScaleFactors(scaleFactor=3.800, offset=-1. ) 
refV.EM3Frac.setScaleFactors(scaleFactor=8.000, offset=-0.500 ) 
refV.mu.setScaleFactors(scaleFactor=2.000, offset=0.000 ) 
refV.NPV.setScaleFactors(scaleFactor=3.200, offset=-0.200 ) 
if 'LogMoE' in conf.features: refV.LogMoE.setScaleFactors(scaleFactor=3.000, offset=0.400 ) 
if 'e_reco_nolog' in conf.features: refV.e_reco_nolog.setScaleFactors(scaleFactor=3.000, offset=-0.900 ) 
if 'LowMoE' in conf.features: refV.LowMoE.setScaleFactors(scaleFactor=2.2, offset=-0.95 ) 
if 'LowMoES' in conf.features: refV.LowMoE.setScaleFactors(scaleFactor=2.5, offset=0.6 ) 
if 'HighE' in conf.features: refV.LowMoE.setScaleFactors(scaleFactor=2., offset=2.6 ) 
refV.pt_reco.setScaleFactors(scaleFactor=3.7, offset=-0.9 )
refV.Tau32.setScaleFactors(scaleFactor=4.4, offset=.5 )
refV.Tau21.setScaleFactors(scaleFactor=4.4, offset=.5 )

refV.r_raw_e.setScaleFactors(scaleFactor=4.500, offset=0.000 ) 
refV.r_raw_m.setScaleFactors(scaleFactor=4.00, offset=-0.400 ) 

noTanh = True
if noTanh :
    refV.r_raw_e.setScaleFactors(scaleFactor=10, offset=0.6 ) 
    refV.r_raw_m.setScaleFactors(scaleFactor=7.00, offset=0.3 ) 
    
"""

refV.eta.setScaleParameters(2.23e-01,1.00e-05)
refV.e_reco.setScaleParameters(sf=0.390, o=-2.430 ) 
refV.m_reco.setScaleParameters(1.95e-01,-.53e+00)
refV.Width.setScaleParameters(2.8e+00,-1e+00)
refV.EMFrac.setScaleParameters(1.2e+00,-0.7e+00)
refV.EM3Frac.setScaleParameters(1.7e+00,-0.65e-00)
refV.Tile0Frac.setScaleParameters(1.2e+00,-0.55e-00)
refV.EffNConsts.setScaleParameters(1.5e-01,-1.1e+00)
refV.groomMratio.setScaleParameters(1.98e-00,-0.99e+00)
refV.neutralFrac.setScaleParameters(1.98e-00,-0.99e+00)
refV.sumPtTrkFrac.setScaleParameters(1.8e-00,-1.0e-00)
refV.sumMassTrkFrac.setScaleParameters(1.99e+00,-9.92e-01)
refV.Split12.setScaleParameters(9.2e-07,-1.0e+00)
refV.Split23.setScaleParameters(2.e-06,-1.0e+00)
refV.C2.setScaleParameters(3.2e-00,-1.00e+00)
refV.D2.setScaleParameters(3.65e-03,-1.00e+00)
refV.Tau32.setScaleParameters(2.05e-00,-1.0e-00)
refV.Tau21.setScaleParameters(2.1e-00,-1.0e-00)
refV.Qw.setScaleParameters(1.75e-03,-1.0e+00)
refV.mu.setScaleParameters(2.0e-02,-1.0e+00)
refV.NPV.setScaleParameters(2.75e-02,-1.03e+00)
refV.r_raw_e.setScaleParameters(7.5e-01,-1.03e-00)
refV.r_raw_m.setScaleParameters(2.0e-03,-.0e-00)
refV.pt_reco.setScaleParameters(3.92e-04,-1.05e+00)

noTanh = True


    
    
