#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
#echo $SCRIPT_DIR
TOP_DIR=${SCRIPT_DIR%/*}
#echo $TOP_DIR

ln -s $TOP_DIR/histoscripts/buildBHfromTrainer.py .
ln -s $TOP_DIR/trainscripts/jetCalibDNN.py .
ln -s $TOP_DIR/histoscripts/plotAndDebugDNN.py .


echo "Links to main JetCalibDNN scripts done"
echo "You probably want to copy one of these scripts to specialize your trainings : "
ls $TOP_DIR/trainscripts/setupA10CSSKUFOsoftDrop.py 
ls $TOP_DIR/trainscripts/setupA10LCTopoTrimm.py 
ls $TOP_DIR/trainscripts/setupAntiKt4EMPFlowJets.py 


echo "It's safe to rm unecessary setupXYZ.py files"

PPATH=${TOP_DIR%/*}
cat <<EOF > setupPATH.sh
export PYTHONPATH=$PYTHONPATH:$PPATH
EOF


echo "now do 'source setupPATH.sh' to setup your PYTHONPATH"
