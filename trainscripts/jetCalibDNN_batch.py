if 'conf' not in dir():
    import tensorflow as tf
    print("TF version = ", tf.__version__)
    print("TF path = ", tf.__path__)
    import tensorflow_addons as tfa
    print("TFA version =", tfa.__version__)


    exec(open('jetCalibDNN.py').read())

    conf.inputDir = ''
    conf.fitVerbosity = 2

def batchSequence(trainer):
    opts = dict( metrics=[utils.lgkLoss(1e-3,1e-3)], )
    for i in range(1):
        trainer.refit(batch_size=35000, optimizer=radaOpt(1e-6), nepoch=3, loss=[utils.truncmdnLoss(1.),utils.truncmdnLoss(1.)], partition=None, **opts)
        trainer.refit(batch_size=15000,  optimizer=radaOpt(1e-6),  nepoch=2, loss=[utils.truncmdnLoss(1.),utils.truncmdnLoss(1.)], partition=None, **opts)    
        trainer.refit(batch_size=95000, optimizer=radaOpt(1e-6), nepoch=3, loss=[utils.truncmdnLoss(1.),utils.truncmdnLoss(1.)], partition=None, **opts)
    trainer.save_model()
    for i in range(1):
        utils.reset_regularizer(trainer.net, 0. )
        trainer.refit(batch_size=95000, optimizer=radaOpt(1e-6), nepoch=3, loss=[utils.truncmdnLoss(.5),utils.truncmdnLoss(0.6)], partition=None, **opts)
    trainer.save_model()

def batchSequenceB(trainer, freezeBN=False, weights=None):

    opts = trainer.config.refitParams
    opts.update( metrics=[utils.gkLoss(1e-2)],
                 sample_weight=weights)
    if trainer.config.lossType=="MDNA":
        lossFunc = lambda t: utils.amdnLoss
        if trainer.config.optimTag=='directSF':
            lossFunc = lambda t: utils.amdnLossSF
    elif trainer.config.lossType=="MDN":
        lossFunc = lambda t: utils.truncmdnLoss(t)

    for i in range(1):
        #trainer.refit(batch_size=35000, optimizer=adaBel(1e-6), nepoch=3, loss=[lossFunc(1.),lossFunc(1.)],  **opts)
        #trainer.refit(batch_size=15000,  optimizer=adaBel(1e-6),  nepoch=2, loss=[lossFunc(1.),lossFunc(1.)],  **opts)    
        if freezeBN: trainer.freezeBatchNorm()
        trainer.reweightAnomalousRow()
        trainer.refit(batch_size=95000, optimizer=diffgrad(1e-6), nepoch=3, loss=[lossFunc(1.),lossFunc(1.)],  **opts)
    trainer.save_model()
    for i in range(1):
        utils.reset_regularizer(trainer.net, 0. )
        trainer.refit(batch_size=95000, optimizer=diffgrad(1e-4), nepoch=2, loss=[lossFunc(.5),lossFunc(0.6)],  **opts)
        trainer.reweightAnomalousRow()
        trainer.refit(batch_size=95000, optimizer=diffgrad(1e-6), nepoch=2, loss=[lossFunc(.5),lossFunc(0.6)],  **opts)
        trainer.refit(batch_size=15000, optimizer=diffgrad(1e-6), nepoch=2, loss=[lossFunc(1.),lossFunc(1.)],  **opts)
        trainer.refit(batch_size=105000, optimizer=diffgrad(1e-6), nepoch=2, loss=[lossFunc(.5),lossFunc(0.6)],  **opts)

    trainer.save_model()

def batchSequenceC(trainer):
    if trainer.config.lossType=="MDNA":
        lossFunc = lambda t: utils.amdnLoss
        if trainer.config.optimTag=='directSF':
            lossFunc = lambda t: utils.amdnLossSF
    elif trainer.config.lossType=="MDN":
        lossFunc = lambda t: utils.truncmdnLoss(t)

    utils.reset_regularizer(trainer.net, 1e-5 )
    trainer.refit(batch_size=105000, optimizer=diffgrad(1e-3), nepoch=2, loss=[lossFunc(1.),lossFunc(0.8)], partition=None,loss_weights=[1.,1.], metrics=[utils.gkLoss(1e-2)], sample_weight=1 )
    trainer.refit(batch_size=15000, optimizer=diffgrad(1e-3), nepoch=1, loss=[lossFunc(1.),lossFunc(0.8)], partition=None,loss_weights=[1.,1.], metrics=[utils.gkLoss(1e-2)], sample_weight=1 )
    utils.reset_regularizer(trainer.net, 0)
    trainer.refit(batch_size=150000, optimizer=diffgrad(1e-3), nepoch=2, loss=[lossFunc(.8),lossFunc(0.8)], partition=None,loss_weights=[1.,1.], metrics=[utils.gkLoss(1e-2)], sample_weight=1 )
    print("\n END batchSequenceC !!")
    trainer.save_model()

def batchSequenceAMDN(trainer):
    if trainer.config.lossType!="MDNA":
        print("DO NOT use batchSequenceAMDN if lossType != AMDN")
        return

    lossFunc = lambda t: utils.truncamdnLoss(t)
    if trainer.config.optimTag=='directSF':
        lossFunc = lambda t: utils.truncamdnLossSF(t)
        
    trainer.refit(batch_size=95000, optimizer=diffgrad(1e-3), nepoch=3, loss=[lossFunc(None),lossFunc(None)], partition=None,loss_weights=[1.,1.], metrics=[], sample_weight=1 )
    utils.reset_regularizer(trainer.net, 1e-5 )
    trainer.refit(batch_size=95000, optimizer=diffgrad(1e-3), nepoch=2, loss=[lossFunc(3.5),lossFunc(3.5)], partition=None,loss_weights=[1.,1.], metrics=[], sample_weight=1 )
    trainer.refit(batch_size=125000, optimizer=diffgrad(1e-3), nepoch=2, loss=[lossFunc(3.5),lossFunc(3.5)], partition=None,loss_weights=[1.,1.], metrics=[], sample_weight=1 )
    trainer.save_model()
    utils.reset_regularizer(trainer.net, 0. )
    trainer.refit(batch_size=125000, optimizer=diffgrad(1e-3), nepoch=2, loss=[lossFunc(3.5),lossFunc(3.5)], partition=None,loss_weights=[1.,1.], metrics=[], sample_weight=1 )
    trainer.save_model()
    trainer.refit(batch_size=155000, optimizer=diffgrad(5e-4), nepoch=4, loss=[lossFunc(3.2),lossFunc(3.2)], partition=None,loss_weights=[1.,1.], metrics=[], sample_weight=1 )
    trainer.save_model()
    

    
def batchSequenceBLGK(trainer, ):
    gkL = utils.gk1Loss(1e-3)
    trainer.lossColl.accumulate(trainer)
    for i in range(1):
        trainer.refit(batch_size=35000, optimizer=diffgrad(1e-4), nepoch=3, loss=[gkL,gkL],  **opts)
        trainer.refit(batch_size=150000,  optimizer=diffgrad(1e-4),  nepoch=2, loss=[gkL,gkL],  **opts)    
    trainer.save_model()
    trainer.lossColl.accumulate(trainer)
    for i in range(1):
        utils.reset_regularizer(trainer.net, 0. )
        trainer.refit(batch_size=105000, optimizer=diffgrad(1e-5), nepoch=2, loss=[gkL, gkL],  **opts)
    trainer.save_model()



def retrainSequenceE(trainer):
    removeMassRespFilter(trainer,'outputM')
    trainer.setTrainableStatus(True, ancestorsOf='outputE')
    try:
        trainer.gen.shuffle_files()
    except:
        pass

    for i in range(2):
        trainer.refit(batch_size=150000, optimizer=diffgrad(1e-3), nepoch=2, loss=[utils.amdnLoss,utils.nullLoss], partition=None,loss_weights=[1.,1.], metrics=[utils.gkLoss(1e-2)], sample_weight=1 )
        trainer.save_model()
        trainer.refit(batch_size=50000, optimizer=diffgrad(1e-3), nepoch=2, loss=[utils.amdnLoss,utils.nullLoss], partition=None,loss_weights=[1.,1.], metrics=[utils.gkLoss(1e-2)], sample_weight=1 )
        trainer.save_model()
        print( "UUUUUUUU" , trainer)
    for i in range(2):
        trainer.refit(batch_size=150000, optimizer=diffgrad(1e-4), nepoch=4, loss=[utils.amdnLoss,utils.nullLoss], partition=None,loss_weights=[1.,1.], metrics=[utils.gkLoss(1e-2)], sample_weight=1 )
    trainer.save_model()
        
