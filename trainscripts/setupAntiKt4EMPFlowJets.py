# this file is meant to be included from the main jetCalibDNN.py script 

conf.update(
    nnName = "AntiKt4EMPFlowJets" ,
    inputFiles = 'AntiKt4EMPFlowJets_*.root',
    treeName = "AntiKt4EMPFlowJets2" ,
    
    #features = ['eta', 'e_var','m_var', "EffNConsts", "EffNTracks", "D2", "EMFrac", "Qw", 'sumPtTrkFrac', "Tile0Frac", "groomMratio", "neutralFrac", "EM3Frac", "mu", 'NPV', ]#"Tau32", "pt_reco"],    # !!!
    #features = ["eta", 'e_reco', 'm_reco',   'area',  'jet_Wtrk1000', 'jet_Ntrk1000',  'jet_nMuSeg', 'NPV', 'mu', 'rho',  'jet_ChargedFraction_charged_tracks', 'jet_eEM3', 'jet_eTile0', "jesR"  ],    # !!!
    #features = ["eta", 'e_reco', 'm_reco',   'area',  'jet_Wtrk1000', 'jet_Ntrk1000',  'jet_nMuSeg', 'NPV', 'mu', 'rho',  'jet_ChargedFraction_charged_tracks', 'jet_eEM3', 'jet_eTile0',  "pt_reco"  ],    # !!!
    #features = ["eta", 'e_reco', 'm_reco',   'area',  'jet_Wtrk1000', 'jet_Ntrk1000',  'jet_nMuSeg', 'NPV', 'mu', 'rho',  'jet_ChargedFraction_charged_tracks', 'jet_eEM3', 'jet_eTile0',  "pt_reco"  ],    # !!!
    features = ["eta", 'e_var', 'm_reco',   'area',  'jet_Wtrk1000', 'jet_Ntrk1000',  'jet_nMuSeg', 'NPV', 'mu', 'rho',  'jet_ChargedFraction_charged_tracks', 'fracEM3', 'fracTile0', "fracPtLeadJet", "pt_reco_lows"   ],    # !!!
    #["eta", 'e_reco', 'm_reco',   'area',  'jet_Wtrk1000', 'jet_Ntrk1000',  'jet_nMuSeg', 'NPV', 'mu', 'rho',  'jet_ChargedFraction_charged_tracks', 'fracEM3', 'fracTile0', "pt_reco_low"   ]
    #features = ["eta", 'e_reco_nolog', 'm_reco_nolog',   'area',  'jet_Wtrk1000', 'jet_Ntrk1000',  'jet_nMuSeg', 'NPV', 'mu', 'rho',  'jet_ChargedFraction_charged_tracks', 'jet_eEM3', 'jet_eTile0',   ],    # !!!
    #features = ["eta", 'e_reco_nolog', 'm_reco_nolog',   'area',  'jet_Wtrk1000', 'jet_Ntrk1000',  'jet_nMuSeg', 'NPV', 'mu', 'rho',  'jet_ChargedFraction_charged_tracks', 'jet_eEM3', 'jet_eTile0', "pt_reco"   ],    # !!!

    # 'jet_ChargedFraction_tracks', 'jet_ChargedFraction_combined', 'jet_ChargedFraction_SumPtTrkPt500',  'jet_ChargedFraction_charged', 'jet_ChargedFraction',
    
    targets = ["r_e"],
)

refV=Variable.referenceVars    

def addJes():
    conf.features.append('jesR')
    conf.inputFriends = 'AntiKt4EMPFlowJetsJES_*.root'
    
if 'jesR' in conf.features:
    conf.inputFriends = 'AntiKt4EMPFlowJetsJES_*.root'


def checkEandMVariants():
    for v in ['e','m']:
        for vfull in [f'{v}_reco_nolog', f'{v}_reco']:
            if vfull in conf.features:
                refV[f'r_raw_{v}'].numerator = vfull
                refV[f'r_raw_{v}'].reset_depenencies()
                Variable.aliasConfig[f'{v}_var'].reco = vfull
checkEandMVariants()
# if 'm_reco_nolog' in conf.features:
#     refV.r_raw_m.numerator = 'm_reco_nolog'
#     refV.r_raw_m.reset_depenencies()
#     Variable.aliasConfig.reco.m_var = 'm_reco_nolog'

    
# adapt the variable src_name to the source name in the input file :
refV.e_reco.src_name = "e_reco"
refV.m_reco.src_name = "m_reco"
refV.eta.src_name = "eta_reco"

refV.fracPtLeadJet.src_name = "frac_leadPt"

# Below we adapt the reference variables to our sample
#  - adjust the scale factors and offsets of variable normalizations

# Adjust offset and scale factors. Check the normalized histo with :
# exec(open('plotAndDebugDNN.py').read())
# trainer.histogramInputs(formatted=True)

refV.e_reco.pre_filter = [] # no prefilter for smallR (we have e>80 for large R)
refV.e_reco_nolog.pre_filter = [] # no prefilter for smallR (we have e>80 for large R)
refV.e_true.pre_filter = [] # no prefilter for smallR (we have e>80 for large R)

# refV.eta.setScaleFactors(scaleFactor=2.7, offset=0.0 ) 
# refV.eta_det.setScaleFactors(scaleFactor=2.9, offset=-0.000 ) 




# refV.e_reco.setScaleFactors(scaleFactor=1.8, offset=-0.17 )
# refV.e_reco_nolog.setScaleFactors(scaleFactor=4.5, offset=-0.8 )

# refV.m_reco.setScaleFactors(scaleFactor=3.6, offset=0.05 ) 
# refV.m_reco_nolog.setScaleFactors(scaleFactor=4.5, offset=-0.8 ) 

# refV.pt_reco.setScaleFactors(scaleFactor=4.2, offset=-0.85 )

# refV.area.setScaleFactors(scaleFactor=15.2, offset=-0.287 )
# refV.jet_Wtrk1000.setScaleFactors(scaleFactor=2.83, offset=-0.6 )
# refV.jet_Ntrk1000.setScaleFactors(scaleFactor=6.540, offset=-0.827 )
# refV.jet_nMuSeg.setScaleFactors(scaleFactor=54.97, offset=-0.98 )
# refV.rho.setScaleFactors(scaleFactor=4.5, offset=-0.5 )
# refV.NPV.setScaleFactors(scaleFactor=4.200, offset=-0.2 )
# refV.mu.setScaleFactors(scaleFactor=3.5, offset=-0.1 )  
# refV.jet_ChargedFraction_charged_tracks.setScaleFactors(scaleFactor=4., offset=-0.6 )

# refV.jet_eEM3.setScaleFactors(scaleFactor=7, offset=-0.8 )
# refV.jet_eTile0.setScaleFactors(scaleFactor=12., offset=-0.9 )


# #refV.jesR.setScaleFactors(scaleFactor=8.8, offset=-0.3 )
# refV.jesR.setScaleFactors(scaleFactor=4.5, offset=0.0 ) # keep same factors as output r_e scaling


# refV.r_raw_e.setScaleFactors(scaleFactor=4.500, offset=0.000 ) 
# refV.r_raw_m.setScaleFactors(scaleFactor=4.00, offset=-0.400 ) 




refV.pt_reco_low.setScaleParameters(1/42., -1. )

refV.eta.setScaleParameters(2.16e-01,-3.10e-04)
refV.e_reco_nolog.setScaleParameters(5.63e-04,-8.71e-01)
refV.m_reco_nolog.setScaleParameters(1.68e-02,-8.96e-01)
refV.area.setScaleParameters(3.27e+00,-1.92e+00)
refV.jet_Wtrk1000.setScaleParameters(4.85e+00,-9.77e-01)
refV.jet_Ntrk1000.setScaleParameters(2.55e-02,-9.54e-01)
refV.jet_nMuSeg.setScaleParameters(3.64e-02,-9.80e-01)
refV.NPV.setScaleParameters(5.29e-02,-9.41e-01)
refV.mu.setScaleParameters(3.81e-02,-9.95e-01)
refV.rho.setScaleParameters(9.75e-05,-9.95e-01)
refV.jet_ChargedFraction_charged_tracks.setScaleParameters(7.58e-01,-9.58e-01)
refV.jet_eEM3.setScaleParameters(1.24e-02,-8.22e-01)
refV.jet_eTile0.setScaleParameters(3.03e-03,-9.01e-01)
refV.pt_reco.setScaleParameters(0.0004,-9.03e-01)
refV.pt_reco_lows.setScaleParameters(0.005,-8.03e-01)
refV.pt_true_lows.setScaleParameters(0.005,-8.9e-01)

refV.r_raw_e.setScaleParameters(1.44e+00,-1.25e+00)
refV.r_raw_m.setScaleParameters(8.52e-01,-1.04e+00)
refV.e_reco.setScaleParameters(3.23e-01,-1.73e+00)
refV.e_ni.setScaleParameters(3.23e-01,-1.73e+00)
refV.m_reco.setScaleParameters(2.25e-01,-3.44e-01)
refV.jesR.setScaleParameters(1.44e+00,-1.25e+00)
refV.fracTile0.setScaleParameters(sf=2.000, o=-0.600 ) 
refV.fracEM3.setScaleParameters(sf=3.000, o=-0.600 ) 

refV.e_true.setScaleParameters(sf=0.323, o=-1.600 ) 

refV.fracPtLeadJet.setScaleParameters(sf=1.9, o=-0.95 ) 

refV.Eweights.fname = "hE_ak4.npz"

# IMPORTANT : scaling of eta annotation. We know the measure of the std of eta is 1.71810531 so we set the full scale factor here :
#  (can not be done automatically in an easy way )
from JetCalibDNN.ModelDefinitions  import EtaAnnotBase, TailAESplitE
#EtaAnnotBase.eta_scale = refV.eta.scaleAdjust * 1.71810531
EtaAnnotBase.eta_scale = 5.15431593 # WRONG !!!! temporary ! should be 1./refV.eta_scale

# # set the scale factors for energy. Get them from trainer.inscaler.scale_parameters() after normalization is set.
# TailAESplitE.scaleparams = ( 0.0005625045215118536, -0.8705735526935172)
# if 'pt_reco' in conf.features:
#     # probably scaling on pT :
#     TailAESplitE.scaleparams = ( 0.0012331317995759132, -0.9027407065905682)  # if scaling on pT
