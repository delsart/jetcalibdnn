"""Top level object coordinating the training of models for jet calibration

The Trainer class is in charge of steering all the tasks needed to obtain a calibration NN :
 - input preparation
 - training & validation 
 - saving/loading models
 - building Numerical Inversed inputs 
 - building calibrated quantities for testing/validation

all these tasks are performed following the options registered in the configuration (see ConfigUtils).
...




"""
import h5py
import numpy as np

# Import Keras (either standalone, either as embedded in tensorflow )
from .ImportKeras import *
from .ConfigUtils import ConfigDict
from .GeneratorFromRootFile import ROOTDataGeneratorValid, writeTTree
from .Variables import Variable, dataPartitions

from . import NNUtils as utils







class Trainer:
    net = None # placeholder for the keras.Model
    pnet = None # a placeholder for parralelized model (in case we use this feature)

    # optimizer definition should be placed somewhere else...
    optimizers=ConfigDict(
        nada=keras.optimizers.Nadam(lr=0.01),
        nada0=keras.optimizers.Nadam(lr=0.001),
        nada00=keras.optimizers.Nadam(lr=0.0001),
        ada=keras.optimizers.Adam(lr=0.0003,amsgrad=True),
        sgd=keras.optimizers.SGD(lr=0.001, momentum=0.9, ), #clipvalue=9.),
        #psgd=PerturbSGD(lr=0.0001, momentum=0.9, ), #clipvalue=9.),
        rada00d = tfa.optimizers.RectifiedAdam(lr=1e-4, decay=0.0001),
        rada = tfa.optimizers.RectifiedAdam(lr=0.001, ),
        rada4e4 = tfa.optimizers.RectifiedAdam(lr=0.0004, ),
        rada1e4 = tfa.optimizers.RectifiedAdam(lr=0.0001, ),
        rada3e5 = tfa.optimizers.RectifiedAdam(lr=0.00003, ),
        radaw1e5 = lambda trainer : tfa.optimizers.RectifiedAdam(lr=5e-4, total_steps=len(trainer.gen), warmup_proportion=0.05, min_lr=1e-5),
        radaw1e6 = lambda trainer : tfa.optimizers.RectifiedAdam(lr=5e-5, total_steps=len(trainer.gen), warmup_proportion=0.05, min_lr=1e-6),

    )

    custom_objects=dict(
                        GausAnnotation=utils.GausAnnotation,
                        GausAnnotationSym=utils.GausAnnotationSym,
                        GateAnnotationSym=utils.GateAnnotationSym,
                        TrapezoidAnnotation = utils.TrapezoidAnnotation,
                        MinMaxLinearAnnot = utils.MinMaxLinearAnnot,
                        EtaLinC = utils.EtaLinC,
                        OneMinus = utils.OneMinus,
                        ScaleOutput = utils.ScaleOutput,
                        DuplicateEntry = utils.DuplicateEntry,
                        AddJES = utils.AddJES,
                        ExtractN = utils.ExtractN,
                        #GatedAnnotation = utils.GatedAnnotation,
                        ExcludeIndices = utils.ExcludeIndices,                                               
                        mish = utils.mish,
                        tanhp = utils.tanhp,
                        tanhSF = utils.tanhSF,
                        tanh0 = utils.tanh0,
                        )

    # placeholder for the configuation dict.
    config = None

    # tag describing target, build automatically from config.targets
    targetTag = ''

    redraw = False

    # name of the partition of the input data (if any) we're using for training.
    currentPartition = None


    # placeholders for utility object used to scale the input and targets. Typically something like NNutils.MeanStdScaler
    inscaler = None
    outscaler = None


    # filled once fullPredictions has been called
    last_predictions = None

    # will be used to build the full tag of this trainer
    reffeatureVars = [] 

    lastTrainSummary = None
    
    def __init__(self):
        #self.lossHistory = []
        self.inscaler = utils.NoScaler( (0,0)  )
        self.outscaler = utils.NoScaler( (0,0)  )
        self.firstRun = True
        self.stop_training = False

        self.current_loss = None

        self.loglossnseen = utils.LogLossNumSeen()        
        self.savegoodLoss = utils.SaveGoodLoss()
        from JetCalibDNN.HistoAnalysis.BinnedArrays import BinnedPhaseSpace,  AxisSpec            
        self.lossColl = utils.LossCollector(self,
                                            AxisSpec( 'e_var', edges=[0,500,1000,2000,3000,4000,5000] ) ,
                                            #AxisSpec( 'eta', edges=[0,0.2,0.8,1.,1.2,1.4,1.5,2.,2.4,3.,4,5] ) )
                                            AxisSpec( 'eta', edges=[0,0.5,1.,1.5,2.,2.5,3.,4,5] ) )
        self.valLoss = utils.ValidCallBack(self)
        
        self.inputfileshuffler = utils.ShuffleInputFiles()


    @staticmethod
    def fromFile(fname, config=None ):
        t = Trainer()

        if config:
            t.setupModel(config, reload=fname)
        else:
            t.loadModel(fullName=fname)        
            t.config = ConfigDict()
            if t.lastTrainSummary:
                # fill minimally from trainSummary
                t.config.features = t.lastTrainSummary.features.keys()
                t.config.targets  = t.lastTrainSummary.targets.keys()
                t.config.nPredictedParam = t.lastTrainSummary.get('nPredictedParam', 2)
                if t.config.nPredictedParam==True: t.config.nPredictedParam=2
                t.config.optimTag = t.lastTrainSummary.optimTag
            
        return t

    def loadWeightsScale(self, specialTag='', fullName=None):
        fullName = fullName or self.outName(specialTag)
        self.net.load_weights(fullName)
        self.loadScaleParams(fullName)

    def loadWeights(self, specialTag='', fullName=None):
        fullName = fullName or self.outName(specialTag)
        self.net.load_weights(fullName)
        
    def loadModel(self, specialTag='', fullName=None):
        fullName = fullName or self.outName(specialTag)
        self.net = keras.models.load_model(fullName,compile=False,
                                           # Define all the custom layers we may add.
                                           custom_objects=self.custom_objects )

        self.loadScaleParams(fullName)
        print( ' !!!! RELOADED  net from ', fullName )        
        return self.net

    
        
    def loadScaleParams(self, fullName = None ):
        #self.gen.unscale_current_sample()
        fullName = fullName or self.outName()
        f = h5py.File(fullName, 'r')
        try :
            self.inscaler.loadFromFile(f,'inputscaler')
            self.outscaler.loadFromFile(f,'outputscaler')
            print (" XXXXXXXXXXX Loaded scalers from file means=", self.inscaler.mean, self.outscaler.mean)            
        except :
            self.setupScalers(self.gen)

        if 'trainingSummary' in f:
            from pickle import loads
            ts = loads( f['trainingSummary'][()].tostring()  )
            self.lastTrainSummary = ts
            self.loglossnseen.fromDict(  ts['lossHistory'] )
            self.valLoss.fromDict( ts )
        self.lossColl.load(f)
        f.close()


    def fullTag(self,specialTag=''):
        """Build a string identifying the input features, targets, NN model, optimization method. 
        This is mainly to build a ~unique name and thus allow to easily compare various procedures."""
        from hashlib import md5

        nameTrDic ={}
        if self.config.inputMode=='true':
            # transform e_true and m_true name to have constitant name files between true and ni            
            nameTrDic.update(e_true = 'e_reco', m_true='m_reco')
        if self.config.inputMode=='ni':
            # transform e_true and m_true name to have constitant name files between true and ni            
            nameTrDic.update(e_ni = 'e_reco', m_ni='m_reco')
        nameTr = lambda n:nameTrDic.get(n,n)
            
        featureTag = md5('.'.join( [nameTr(v.name)+'.'.join(v.array_transfo) for v in self.reffeatureVars] ).encode()).hexdigest()[:11]
        targetTag = '_'.join(self.config.originalTargets)
        suffix =  dict(ni='_ni', true='', reco ='_direct')[ self.config.inputMode ]
        specialTag = specialTag or self.config.specialTag
        return self.config.lossType+featureTag+targetTag+self.config.modelTag+self.config.optimTag+specialTag+suffix

    def outName(self, specialTag=''):
        from os.path import join
        return join(self.config.outputDir, self.config.nnName+self.fullTag(specialTag)+'.h5')

    def niInputFriends(self, specialTag=''):

        
        return self.config.inputFriends or 'ni_'+self.fullTag(specialTag)[:-3]+'*'+self.config.inputFiles
        
    
    def save_model(self, specialTag = ''):
        print (" saving in ", self.outName(specialTag))
        outname = self.outName(specialTag)
        self.net.save( outname )
        f = h5py.File(outname, 'a')
        self.inscaler.save(f, 'inputscaler')
        self.outscaler.save(f, 'outputscaler')

        self.lossColl.save(f)
        
        self.saveTrainSummary(f)

        f.close()
        
    def trainingSummary(self):
        td = dict(name = self.outName() )

        td.update( features = dict( (v,Variable.referenceVars[v].asDict()) for v in self.config.features ) )

        td.update( targets = dict( (v,Variable.referenceVars[v].asDict()) for v in self.config.originalTargets ) )
        td.update( nPredictedParam=self.config.nPredictedParam)
        td.update( optimTag = self.config.optimTag)
        td.update( lossHistory = self.loglossnseen.toDict())
        td.update( self.valLoss.toDict() )
        
        netSumL=[]
        def netsummary( s):
            if 'activation' in s:return
            if 'dropout' in s:return 
            if '______' in s:return 
            netSumL.append(s)
        self.net.summary( print_fn=netsummary)
        td.update( layers = netSumL)

        return td

    def saveTrainSummary(self, fname):
        # also save a summary
        summary = self.trainingSummary()
        from h5py import File
        if isinstance(fname, str):
            f = File(fname, 'a')
        else: f = fname
        if 'trainSummary' in f: del(f['trainSummary'])
        from pickle import dumps
        f['trainingSummary'] = np.bytes_( dumps(summary ) )
    
    
        

    def setupConfig(self, config, ):
        """ """
        if self.config != None:
            # WARNING !! Not overwriting config !
            return self.config
        if config is None:
            return self.config
        self.config = config
        


        config.nPredictedParam = dict( LGK = 1,
                                       MDN = 2,
                                       MDNA= 3,)[config.lossType]
            

        inputMode, nnmode = config.mode.split(':')
        if inputMode not in ('true', 'ni', 'reco') or nnmode not in ('train', 'predict', 'testnipred') :
            print("ERROR setupConfig config.mode invalid : ",config.mode)
            raise
        
        config.inputMode = inputMode
        
        # if no input files, try to find some according to nnName 
        config.inputFiles = config.inputFiles or config.nnName+'*'

        # adapt the generic e/m_var aliases  according to the mode
        referenceVars = Variable.referenceVars

        if config.mode=='ni:predict':
            # when predicting, we actually predict from the reco values, not the NI ones
            for k,v in Variable.aliasConfig.items():
                v.ni = v.reco
        # set the aliases :
        for k,v in Variable.aliasConfig.items():
            # alias k to the actual name in this mode :
            Variable.add_alias( v[inputMode] , k )

        self.reffeatureVars = [referenceVars[f] for f in config.features]

        # make simple aliases :
        referenceVars.e = referenceVars.e_var
        referenceVars.m = referenceVars.m_var
            
        config.originalTargets = list(config.targets)

        
        # if using MDN loss, we also target sigmas, so double the number of targets.
        if config.nPredictedParam >1:
            ntargets=[]
            n=config.nPredictedParam
            for t in config.targets:
                if n==2 :ntargets +=[t,t+'_sig']                
                elif n==3 :ntargets +=[t,t+'_sig', t+"_sig2"]                
            config.targets = ntargets

        for t in config.targets:
            referenceVars[t].is_target = True

        # unused yet ???
        #config.cyclicOPtim = config.cyclicOPtim or utils.cyclNoop

        # Done in callbacks
        #self.lossColl.lossFunc = config.get( 'lossCollectorFunc' , utils.gk1Loss(1e-2))
        #self.valLoss.lossFunc = config.get( 'lossCollectorFunc' , utils.gk1Loss(1e-2))
        #if config.lossType == "MDNA":
        #    self.lossColl.lossFunc = utils.amdnLoss
        #    self.valLoss.lossFunc = utils.amdnLoss
            
        return config


        

    def prepareData(self,  config=None,   ):
        config =self.setupConfig(config)

        referenceVars = Variable.referenceVars
        from glob import glob
        from os.path import join

        ## **********************
        # prepare the exact list of input files 
        inputFileList = sorted( glob(join(config.inputDir, config.inputFiles) ) , reverse=False) 
        print( "UUUUU", inputFileList)
        inputFriendList = []
        if config.inputMode == 'ni':
            inputFriendList = sorted( glob(join(config.inputDir, self.niInputFriends()) ) )
        elif config.inputFriends:
            inputFriendList = sorted( glob(join(config.inputDir, config.inputFriends)), reverse=False)
            
        if config.nInputFiles>0:
            inputFileList=inputFileList[:config.nInputFiles]
            inputFriendList=inputFriendList[:config.nInputFiles]
            setupValidFile = False
            
        elif 'train' in config.mode and len(inputFileList)>1:
            setupValidFile = True            
            # for now keep the last file for test & validation            
            validationFile =inputFileList[-1]
            inputFileList = inputFileList[:-1]
            validationFriend=''
            if inputFriendList != [] :
                validationFriend = inputFriendList[-1]
                inputFriendList = inputFriendList[:-1]
        else: # calib or createni 
            setupValidFile = False
        print("\n INPUTS :  using ",len(inputFileList), " from ",inputFileList[0] , "to", inputFileList[-1] )
        if inputFriendList != []:
            print(" INPUTS :  friends ",len(inputFriendList), " from ",inputFriendList[0] , "to", inputFriendList[-1] )
        ## **********************
            

        ## **********************
        # prepare the variables : features and targets.

        # in some rare cases, we may want to filter out some variables for efficiency reason (mostly when we don't need DNN prediction, just to load some variables)
        filterIfNeeded = config.filterLoadedVar or (lambda l:l)

        # create utils.Variable objects by cloning the reference objects
        x_vars = filterIfNeeded([ referenceVars[v].clone() for v in config.features   ])
        y_vars   = filterIfNeeded([referenceVars[v].clone() for v in config.originalTargets  ])        
        all_vars = set([v.name for v in x_vars+y_vars])  # used to avoid double conting variables
        aux_vars = filterIfNeeded( [referenceVars[v].clone() for v in config.additionalVars if referenceVars[v].name not in all_vars] )
        for v in x_vars+y_vars+aux_vars:
            for dn in v.dependencies:
                dn = referenceVars[dn].name
                if dn not in all_vars:
                    aux_vars.append(referenceVars[dn].clone() )
                    all_vars.add( dn )
        ## **********************
        

        if len(inputFileList)<2:
            # force to use non MultiThread generator . VERY HACKY ...
            from .GeneratorFromRootFile import Generator1Target, Generator2Target            
            config.inputClassList = [Generator1Target, Generator2Target]

        ## **********************
        # build the actual Generator from GeneratorFromRootFile.py
        InputClass = config.inputClassList[len(config.originalTargets)-1]
        gen = InputClass(
            config.treeName,
            inputFileList,
            x_vars   = x_vars,
            y_vars   = y_vars,
            aux_vars = aux_vars,
            shuffle = config.shuffle,
            input_friends = inputFriendList,
            maxEvents = config.maxEvents,
        )
        
        self.gen = gen

        config.setdefault( 'batch_size' , 1000)
        gen.setup_variables()

        if config.sampleWeights:
            gen.set_weights(config.sampleWeights)

            # re-prepare scalers in case using weights implied changing the adjustements
            #if initScalers: self.prepareScalers(gen.chain.allVars)

        self.setupScalers(gen)
            
        if setupValidFile:
            self.gen_valid = ROOTDataGeneratorValid(gen,validationFile, validationFriend)


    def setupScalers(self, gen):
        """Prepare the input (self.inscaler) and output (self.outscaler) scaler instances.  
           Their scaling factors and offset are set according to those encoded in the variables found in the referenceVars dictionnary which  is Variables.referenceVars.
           IMPORTANT : it is expected that the initialization scripts have assigned these scale factors and offsets to the ref variables,  this is NOT automatic.
                       see trainer/setupAntikXXX.py for examples.
        """
        self.inscaler = utils.VariableScaler(fromVars=gen.x_vars)
        if self.config.modelBuilder.scale_output:
            self.outscaler = utils.VariableScaler(fromVars=gen.y_vars)
        else:
            self.outscaler = utils.NoScaler()

        gen.set_scalers(self.inscaler, self.outscaler)

            
    def setupScalersFromData(self, gen):
        """Expected to be called by user in order to set the scale factors in interactive sessions.
        This sets the scale factor according to the mean and RMS of each input variable. User will then tune the scale factors manually"""
        # load data to be able to calculate automatically the parameters of scalers.
        gen.set_scalers(utils.NoScaler(), utils.NoScaler()) # gen needs Scaler to be set, so use a dummy one for the 1st read.
        gen.loadFile(0)
        X,Y = gen.current_sample(formatted=True)  # -> transformed, but not scaled since NoScaler

        utils.debug("xxxxxxxxxxxxx pre= setupScalers e_Var =", self.gen.chain.allVars.e_var.array[:5], " transformed = ", self.gen.current_file.is_transformed)
        if isinstance(Y,tuple):
            Y = np.stack( Y, 1 )

        self.inscaler = utils.VariableScaler()
        self.outscaler = utils.VariableScaler()

        # Now do the actual calculations.
        self.inscaler.setFromMeanStdErr(X)
        if self.config.modelBuilder.scale_output:
            self.outscaler.setFromMeanStdErr(Y)
        else:
            self.outscaler = utils.NoScaler()
            
                
        utils.debug (" Input scaler : evaluated mean -->",self.inscaler.mean)
        gen.set_scalers(self.inscaler, self.outscaler) # 

        
        
        
    def setupModel(self,config=None , reload=False):
        config = self.setupConfig(config)
        
        #self.currentOptimizer = self.optimizers[config.optimizer]
        modelBuilder = config.modelBuilder
        if reload:
            net = modelBuilder(self, )                        
            if not isinstance(reload,str) : reload='' # so we reload the default model as define by self.config
            if reload.endswith('.h5'):
                net = self.loadModel( fullName = reload)
            else:
                net = self.loadModel(specialTag=reload)            
        else:
            net = modelBuilder(self)            
            #self.loglossnseen.njetprocessed = 0

        self.net = net
        
        self.loglossnseen.setupTrainer(self)

        # Remove this seems not necessary 
        #for a in self.lossColl.bps.axis:
        #    if a.name=='e_var':                
        #        a.name = Variable.aliasConfig.e_var.reco # ???
        #    if a.name=='m_var':                
        #        a.name = Variable.aliasConfig.m_var.reco
        
                
        # The actual loss func might need the net to be ready. We can call it now :

        #self.setLoss(config.ni_loss if config.inputMode=='ni' else config.loss)

    def reloadCurrentModel(self):
        self.setupModel(None, reload=self.config.modelTag)


    def setDataPartition(self, partition):
        if self.currentPartition != partition:
            preFilters, postFilters = dataPartitions[partition]
            #self.gen.applyPartition(partition)
            self.gen.set_filters(preFilters, postFilters)
            self.currentPartition = partition
        
        

    def refit_noMP(self,nepoch=1, optimizer=None, loss=None, partition=None, **args):
        # very strange behaviour : after restart and reload weights, can NOT use multiprocessing ???
        args['use_multiprocessing'] = False
        return self.refit(nepoch, optimizer, loss, partition, **args)

    def refit(self,nepoch=1, optimizer=None, loss=None, partition=None, loss_weights=None, compileargs={}, **args):
        if self.config.mode.endswith('predict'):
            print( "ERROR : do not train if mode = ", self.config.mode )
            return
        try:
            lastEpoch=self.net.history.epoch[-1]+1
        except:
            lastEpoch=0

        if self.stop_training: 
            print("Stopped training !!!")
            return

        if not self.firstRun:
            self.gen.reset()
        self.firstRun = False
        
        self.setDataPartition(partition)
        metrics = args.pop('metrics', None)
        if optimizer or loss :
            self.compileModel(optimizer, loss, metrics=metrics, loss_weights=loss_weights, **compileargs)
        
        self.current_loss = loss
        # remove batch_size from args (fit_generator does not accept it):
        self.gen.set_batch_size(args.pop('batch_size',1000))
        
        print(" xxxxx Epochs ", lastEpoch, lastEpoch+nepoch)
        args.update(initial_epoch=lastEpoch,epochs=lastEpoch+nepoch,
                    verbose = self.config.fitVerbosity)
        #args.setdefault( 'callbacks', self.config.callbacks+[self.loglossnseen, self.inputfileshuffler, utils.StopAtNan(self), self.valLoss])
        args.setdefault( 'callbacks', self.config.callbacks+[self.loglossnseen, self.inputfileshuffler, utils.StopAtNan(self),])
        args['shuffle'] = False # The ROOTDataGenerator takes care of that

        w=args.pop('sample_weight',None)
        if w : self.gen.set_weights(w)
        
        net = self.parallelModel()
        
        args.setdefault('use_multiprocessing',  False)
        #args.setdefault('workers' , 4)

        print ("MP == ", args['use_multiprocessing'])
        args.setdefault("steps_per_epoch",None)

        self.gen.num_seensofar = max(self.loglossnseen.njetprocessed,0)
        net.gen = self.gen # need from loglossnseen callback
        net.trainer = self # need from loglossnseen callback

        try:
            fitH= net.fit( self.gen,  **args )
        except tf.errors.NotFoundError: # happens randomly on large batches ???
            return 
        #fitH= pseudoFit( self.gen,  **args )

        
        #self.lossHistory.append( tuple( l[-1] for k,l in fitH.history.items() if 'loss' in k ) )

        if self.redraw:
            self.checkNN()
        return fitH

    

    def validation(self, batch_size=None, loss=None):
        batch_size = batch_size or self.config.batch_size
        self.gen_valid.set_batch_size(batch_size)
        self.gen_valid.reset()
        #self.gen_valid.addVar1 = 0 # assume it's eta
        self.compileModel( loss=loss)
        r = self.net.evaluate( self.gen_valid  )
        #self.gen.addVar1 = None
        return r

        
    def compileModel(self,optimizer=None, loss=None,**args):
        net = self.parallelModel()        
        loss = self.formatLossFunc(loss)
        
        optimizer = self.optimizers.get(optimizer,optimizer) if optimizer else self.currentOptimizer
        if callable(optimizer):
            optimizer = optimizer(self)
        if self.config.doLookahead:
            optimizer = tfa.optimizers.Lookahead(optimizer)


        args.setdefault('metrics',self.config.metrics)
        self.currentOptimizer = optimizer
        net.compile(loss=loss, optimizer=optimizer, **args)

        
    def train(self,config, fitSequence=None, reload=False, ):
        """Setup a model and its inputs according to config.
           Then starts a fit sequence if fitSequence is not None. It is expected to be a function taking a Trainer as argument and implementing a series of call to Trainer.refit()
           if reload evaluates to true a model is reloaded from file (see setupModel() for the logic) 
        """

        self.prepareData( config=config, )
        
        net = self.setupModel(config=config,reload=reload)
            

        
        if reload or fitSequence is None:
            return net
        elif callable(fitSequence) : # it is a function 
            fitSequenceFunc = fitSequence
        if isinstance(fitSequence,dict): # assume we are given arguments to refit()
            fitSequenceFunc = lambda t : t.refit( **fitSequence)

        self.lossL=[]

        self.fitH=fitSequenceFunc( self )
        
        self.save_model()

        return net

    def formatLossFunc(self, loss):
        addModelToLoss = lambda func : func(self.net) if hasattr(func,"dependsOnModel") else func 
        if len(self.config.originalTargets)==2:
            if not isinstance(loss, list): loss = [loss,loss]
            loss = [addModelToLoss(l) for l in loss]
        else:
            loss = addModelToLoss(loss) 
        return loss
            
    
    
    def evaluate(self, batch_size=100 ):
        x=self.inscaler.transform(self.X_test)
        return self.net.evaluate(x, self.Y_test, batch_size=batch_size)

    def evaluateAt(self, i):
        x0 = self.X_test.iloc[i:i+1].to_numpy()
        y0= self.Y_test.iloc[i].to_numpy()
        r = self.net.evaluate(self.inscaler.transform(x0), y0)
        print(x0)
        print(y0)
        return r


    def parallelModel(self):
        if self.pnet: return self.pnet
        elif self.config.ngpu > 1 :
            from keras.utils import multi_gpu_model
            self.pnet = multi_gpu_model(self.net, gpus=self.config.ngpu)
            self.pnet.metrics_updates = [] ## ?? for lookahead
            self.pnet.metrics_tensors = [] ## ?? for lookahead
            return self.pnet
        else:
            return self.net

    


    def predict(self, inputs, rescalePreds=True):
        fi = self.gen.format_inputs( inputs )
        return self.predict_formatted(fi, rescalePreds=rescalePreds)
        
    def fullPredictions(self,rescalePreds=True):
        x,y = self.gen.current_unfiltered_sample(formatted=True)
        pred= self.predict_formatted( x, rescalePreds=rescalePreds, batch_size=200000, verbose=1)
        self.last_predictions = pred
        return pred

    def predict_formatted(self, inputs, rescalePreds=True, **predargs):
        pred=self.net.predict( inputs , **predargs)
        if not self.gen.singleTarget:
            pred = np.concatenate( pred, 1 )

        if not rescalePreds:
            return pred
            
        hasSig = self.config.nPredictedParam >1   
        
        if hasSig:
            self.outscaler.unscale_inplace(pred[:,::2])
            # only scale the sigmas (and no offset rescaling)
            pred[:,1::2] *=  self.outscaler.full_scale_factor()
        else:
            self.outscaler.unscale_inplace(pred)
                        
        return pred


    

    def createNIDataSet(self, ):
        from os.path import basename
        config = self.config

        if 'true:predict' != config.mode:
            print ("ERROR cant createNIDataSet : config.mode is ", config.mode, ". Set it to 'true:predict' ")
        
        if self.net is None:
            self.loadModel()

        if not self.firstRun:
            self.gen.reset()
            
        hasSig = int('r_e_sig' in self.config.targets)
        
        for i in range(len(self.gen.input_files)):
        
            self.gen.loadFile(i)
            r = self.fullPredictions()
            # unscale / untransform input variables 
            x,y = self.gen.current_unfiltered_sample(formatted=False) 

            # re-acquire e and m arrays because we may use several of them if MT  
            N = self.gen.current_file.Nentries
            e_var = self.allVariables().e_var
            e = e_var.e_var.array[:N]

            #elikeVars = [ v for v in self.allVariables() if v.isElike and v=!e_var]
            
            
            out_file = self.gen.input_files[ i ]
            base = basename(out_file)
            out_file = out_file.replace(base, 'ni_'+self.fullTag()+base)

            
            if not self.gen.singleTarget:
                m = self.allVariables().m_var.array[:N]                
                e_ni = r[:,0]*e
                if 'r_m' in config.targets:                                
                    m_ni = r[:,1+hasSig]*m
                    r_m =  r[:,1+hasSig]
                else:
                    m_ni = r[:,1+hasSig]
                    r_m = m_ni / m
                    
                writeTTree(out_file, self.config.treeName,
                           e_ni = e_ni[:N],
                           r_ni_e = r[:N,0],
                           m_ni = m_ni[:N],
                           r_ni_m = r_m[:N],
                )
                print("wrote ",out_file, 'r_ni_e=',r[:4,0], 'r_ni_m=',r_m[:4], ' m_ni=', m_ni[:4])
            else:                
                e_ni = r[:,0]*e
                writeTTree(out_file, self.config.treeName,
                           e_ni = e_ni[:N],
                           r_e_ni = r[:N,0] )
                print("wrote ",out_file,  " e_ni=", e_ni[:4] , 'r_ni=',r[:4,0])



        
    def createCalibDataset(self, ename='e_dnn', mname='m_dnn'):
        from os.path import basename
        config = self.config

        if 'ni:predict' != config.mode and 'reco:predict' != config.mode:
            print ("ERROR cant createCalibDataset : config.mode is ", config.mode, ". Set it to 'ni:predict' or 'reco:predict' ")
        
        
        if self.net is None:
            self.loadModel()

        hasSig = int('r_e_sig' in self.config.targets)
        hasM = not self.gen.singleTarget
        
        
        for i in range(len(self.gen.input_files)):
    
            self.gen.loadFile(i)


            r = self.fullPredictions()

            self.gen.format_current_sample(reverse=True) # to get back e and m at normal scales.
            
            N = self.gen.current_file.Nentries
            out_file = self.gen.input_files[ i ]
            base = basename(out_file)
            out_file = out_file.replace(base, 'cal_'+self.fullTag()+base)

            # re-acquire e&m arrays (they might change from a file to an other with MT)
            e_reco = self.allVariables().e_var.array
            #e_reco = self.allVariables()['clusterE'].array
            

            
            if hasM:
                m_reco = self.allVariables().m_var.array
                
                e_cal = e_reco[:N]/r[:N,0]
                if 'm_true' in config.targets:                                
                    m_cal = r[:N,1+hasSig]
                else:
                    m_cal = m_reco[:N]/r[:N,1+hasSig]
                writeTTree(out_file, self.config.treeName,
                           e_dnn = e_cal[:N], m_dnn=m_cal[:N])
                print("wrote ",out_file,  " e_cal=", e_cal[:4], m_cal[:4] , "from ", self.gen.chain.last_loaded_file.fname )
            else:
                e_cal = e_reco[:N]/r[:N,0]
                writeTTree(out_file, self.config.treeName,
                           **{ename : e_cal[:N], } )
                           #**{ename : e_cal[:N], "r_pred" : r[:N,0] } )
                print("wrote ",out_file,  " e_cal=", e_cal[:4], " r=", r[:4])





        
    
    def allVariables(self):
        return self.gen.chain.allVars

    

    def saveJSON(self):
        arch = self.net.to_json()
        nnFile  =  self.outName()
        with open(nnFile+'_arch.json', 'w') as arch_file:
            arch_file.write(arch)
        self.net.save_weights(nnFile+'_weights.h5')
        #nnInputVFile = buildInputVarJson(nnFile,targets, features)

        d_seq = {
            "inputs": [],
            "class_labels": self.config.targets, # ex: ["r_e","r_m"]
            }

        varDict = {
            "name": "variable_0",
            "offset": 0,
            "scale": 1
        }
        mean, stdvar = self.inscaler.mean,self.inscaler.std
        for i,f in enumerate(self.config.features):
            #varDict.update(name=f, offset=mean[i], scale=stdvar[i])
            varDict.update(name=f, offset=-float(mean[i]), scale=float(1./stdvar[i]))
            d_seq["inputs"].append(varDict.copy())

        mean, stdvar = self.outscaler.mean,self.outscaler.std
        d_seq['miscellaneous'] = dict(outOffset=str(list(mean)), outScale=str(list(stdvar)) )
        with open(nnFile+'_inputV.json','w') as f:
            import json
            json.dump(d_seq,f)

        nnInputVFile= nnFile+'_inputV.json'
        
        print("keras2json.py ", nnFile+"_arch.json ", nnInputVFile, nnFile+'_weights.h5',' > ' , nnFile+'_lwtnn.json')




    ## ***********************************************
    ## functions to manipulate the model between trainings

    def scaleInputValues(self, varname, values):
        v_ind = self.config.features.index(varname)
        var = Variable.referenceVars[varname]
        m = np.stack([self.inscaler.mean]*len(values) )
        
        m[:,v_ind] = var.transformed_array(array=values)

        return self.inscaler.scale(m)[:,v_ind]

    
    def stretchWeights(self, f=1.05):
        Dense = keras.layers.Dense
        for i,layer in enumerate(self.net.layers):
            #if isinstance(layer,Dense):
            if 'dense' in layer.name:
                a,b=layer.get_weights()
                a*=f
                layer.set_weights([a,b])
    
    
    def reweightAnomalousRow(self, lowT=1e-4, lowF=0.1, highT=10, highF=0.2):
        Dense = keras.layers.Dense
        nReweighted=0
        for i,layer in enumerate(self.net.layers):
            #if isinstance(layer,Dense):
            if 'dense' in layer.name:
                a,b=layer.get_weights()
                N = a.shape[0]
                reset=False
                print ("--------",layer.name)
                for i in range(a.shape[1]):
                    if np.linalg.norm(a[:,i]) < lowT:
                        print("  ",i, "=",np.linalg.norm(a[:,i]))
                        a[:,i] = np.random.sample(N)*lowF
                        b[i] = np.random.sample()*lowF
                        reset=True
                        nReweighted+=1
                    elif np.linalg.norm(a[:,i]) > highT:
                        a[:,i] *= highF
                        reset=True
                        nReweighted+=1
                    if abs(b[i])>10:
                        print("  ",i, "b[i]=",b[i])
                        b[i] = np.random.sample()
                        nReweighted+=1
                        reset=True
                if reset:
                    layer.set_weights([a,b])
        print(" reweightAnomalousRow : nReweighted =",nReweighted, " \n")
        return nReweighted
    
    def update_regularizers(self, factor):
        for i,layer in enumerate(self.net.layers):
            #if isinstance(layer,Dense):
            if 'dense' in layer.name:
                layer.kernel_regularizer.l2 *=factor

    def printLayerNorms(self, ancestorsOf=None):
        utils.printLayerNorms(self.net, ancestorsOf)

    def featureIndex(self, fname):
        return self.config.features.index(fname)

    def varIndexAndScaler(self, vname):
        print('vvvvvvvvvvv ',vname)
        i, scaler = None, None
        if vname in self.config.features:
            i = self.config.features.index(vname)
            scaler = self.inscaler
        elif vname in self.config.originalTargets:
            i = self.config.originalTargets.index(vname)
            scaler = self.outscaler
        
        return i, scaler


    def copy(self, nLayer=-1, layerFilter=lambda l:False, **confUpdate):
        
        oTrainer = Trainer()
        conf = ConfigDict( **self.config )
        for v in ['inputClassList',  ]:
            conf.pop(v,None)
        conf.targets = conf.originalTargets
        
        confUpdate.setdefault('modelBuilder',  conf.pop('modelBuilder') )  # deepcopy of this fails 
        confUpdate['callbacks'] =  conf.pop('callbacks', []) # deepcopy of this fails 
        oTrainer.setupModel( conf.clone( **confUpdate, modelTag=''))

        oTrainer.gen = self.gen
        if hasattr(self, 'gen_valid'): oTrainer.gen_valid = self.gen_valid
        oTrainer.inscaler = self.inscaler
        oTrainer.outscaler = self.outscaler
        
        utils.copyWeights( self.net, oTrainer, nLayer, layerFilter)
        return oTrainer
        
            



    def freezeBatchNorm(self):
        for l in self.net.layers:
            if l.name.startswith("batch_norm"):
                l.trainable=False


    def freezeAncestorLayers(self,  initname='outputE'):
        for l in utils.findDenseAncestorLayers(self.net,initname):
            l.trainable = False

    def setTrainableStatus(self, status, layerL = None, ancestorsOf=None):
        if ancestorsOf:
            if layerL != None:
                print("ERROR layerL and ancestorsOf both non None !!")
                return
            layerL = utils.findDenseAncestorLayers(self.net,ancestorsOf)
        elif layerL is None:            
            layerL =  self.net.layers
            
        for l in layerL:
            if isinstance(l,str):
                l = self.net.get_layer(l)
            if 'dense' in l.name:
                l.trainable = status

    
    def concatenate_models(self,):

        trainer_model = self.net
        outputE = trainer_model.layers[-2]
        outputM = trainer_model.layers[-1]

        from . import ModelDefinitions
        weights = ModelDefinitions.sub_network(self, self.net.layers[5])

        # Condition layer

        weights = utils.ConditionLayer( var_pos = 1, cut = 200.)([trainer_model.inputs, weights])

        #Multiplication

        outputE = layers.multiply( [outputE.output, weights[0]] , name = 'outputE_bis' )
        outputM = layers.multiply( [outputM.output, weights[1]] , name = 'outputM_bis' )

        #Merge model
        #merge = keras.models.Model(inputs=trainer_model.inputs, outputs=[outputE.output,outputM.output,]+ weights)
        merge = keras.models.Model(inputs=trainer_model.inputs, outputs=[outputE,outputM])

        self.net = merge
        
        return


                


## ******************************************************
## Tests of alternative methods and developments
## ******************************************************

    
def pseudoFit(generator, **args):
    N = len(generator)

    for b in range(N):
        #print(" batch ",b)
        x,y = generator[b]
        #print("   ->  ",x[:5])

    print(" ***************** \n\n")
    for b in range(N):
        #print(" batch ",b)
        x,y = generator[b]
        #print("   ->  ",x[:5])
    
