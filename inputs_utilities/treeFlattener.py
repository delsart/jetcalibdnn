## **********************************************
# Flattening and randomized merging of several ttree containing jet data
#  input TTrees are containing vector<float> per event
#  output tree contains only scalars (float, int) with each entry representing 1 jet
#  output tree is also a randomized merge of the input samples
#  output tree also contains a "eventWeight" corresponding to (MC weight)x(xsection weight)
#
# usage :
#  f=createFlattener("flattreeName",neventPerOutFile=12000000)
#  f.run()

# If sum of weights inside histograms in TFiles : 
# for i in user.delsart.3647*; do  cd $i; hadd -T CountHisto.root user.delsart* ; cd .. ;done
#
import ROOT
import glob

#inputPattern = "/home/delsart/analysis/ufo/Data/CSSKUFOSoftDrop/user.delsart.3647*{sample}*v2_tree.root/user*root"
#inputPattern = "/home/delsart/analysis/ufo/Data/LCTopoFull/user.delsart.3610*{sample}*MYSTREAM/user*root"
#inputTreeName = "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets"
#inputPattern = "/sps/atlas/d/delsart/JetData/SmallR/nonflat/{sample}*root"
inputPattern = "JetConstits.{sample}.root"
#inputPattern = "/home/delsart/analysis/ufo/Data/Clusters/topo-cluster_{sample}.pool.root"

#inputTreeName =  "AntiKt10UFOCSSKSoftDrop"
inputTreeName =  "AntiKt4EMPFlowJets2"
inputTreeName =  "jetconstits"
#inputTreeName =  "ClusterTree"

#outputFlat = True
outputFlat = False

# the xsection and efficiencies for each samples
xsectionMap= {
    # sample : (xsection, gen_eff) 
    "JZ0" : (7.8420E+07 , 9.7550E-01), 
    "JZ1" : (7.8420E+07 , 6.7152E-04), 
    "JZ2" : (2.4332E+06 , 3.3434E-04), 
    "JZ3" : (2.6454E+04 , 3.2012E-04), 
    "JZ4" : (2.5463E+02 , 5.3137E-04), 
    "JZ5" : (4.5535E+00 , 9.2395E-04), 
    "JZ6" : (2.5753E-01 , 9.4270E-04), 
    "JZ7" : (1.6215E-02 , 3.9280E-04), 
    "JZ8" : (6.2503E-04 , 1.0176E-02), 
    "JZ9" : (1.9639E-05 , 1.2077E-02), 
    "JZ10":  (1.1962E-06 , 5.9083E-03), 
    "JZ11":  (4.2259E-08 , 2.6761E-03), 
    "JZ12":  (1.0367E-09 , 4.2592E-04), 
    }


def xsectionFactor_xs_eff(sample, norm):
    xs , eff = xsectionMap[sample]
    return xs*eff/norm;
def xsectionFactor_onlynorm(sample, norm):
    return 1000./norm
def xsectionFactor_none(sample, norm):
    return 1.

# depending on how the weight is calculated & set in inputs,
# either :
# eventWeightName = "eventWeight"
# xsectionFactor = xsectionFactor_xs_eff
# normFromHisto = False

# or :
# eventWeightName = "weight"
# xsectionFactor = xsectionFactor_onlynorm
# normFromHisto = ("MetaData_EventCount", 4) # 4 = sumOfWeight selected , 2 = n event selected

eventWeightName = "eventWeightXS"
xsectionFactor = xsectionFactor_none
normFromHisto = False

vetoVariables = ["phi_reco", "phi_true"]
#vetoVariables = ['cluster_sumCellECAlib']

nameForPtFrac = ""
#nameForPtFrac = "pt_reco"

#samples = [ "JZ0", "JZ1", "JZ2", "JZ3","JZ4", "JZ5", "JZ6", "JZ7", "JZ8" , "JZ9", "JZ10", "JZ11", "JZ12" ]
#samples = [ "JZ00", "JZ01", "JZ02", "JZ03","JZ04", "JZ05", "JZ06", "JZ07", "JZ08" , "JZ09", "JZ10", "JZ11", "JZ12" ]
samples = [ "JZ1","JZ2",  ]

#refName=None
refName="cst_e"


#eventLevelCut = ""
eventLevelCut = """
float e_resp = *currentReader->jet_reco_E/ *currentReader->jet_true_E;
bool ignore =  (*currentReader->jet_ratioe<0.05 && *currentReader->jet_reco_nconst==1) || (e_resp<0.5) || (e_resp>2);
if (ignore) continue;
 """

# ***************************
# create one TChain per slice :
chains = []
normalizations = []



for s in samples:
    c = ROOT.TChain(inputTreeName)
    l = sorted(glob.glob(inputPattern.format(sample=s)))
    for f in l:
        print( f)
        c.AddFile(f)
    chains.append(c)
    norm = 0
    if normFromHisto:
        hname, hbin = normFromHisto
        for f in l:
            tf = ROOT.TFile(f)
            norm += tf.Get(hname).GetBinContent(hbin)
    else:
        norm = c.GetEntries()
    normalizations.append( norm )
    
    

# ***************************
# Helper to describe the variables we want to read & write
#  they contain the C++ code which we're going to inject in the main cpp class and compile on-the-fly
class Var:
    def __init__(self,name, typ):
        self.name = name
        self.typ = typ
        self.declInVar = self.t_declInVar.format(typ,name)
        self.initVar = self.t_initVar.format(name,name)
        self.declOutVar = self.t_declOutVar.format(typ, name)
        self.createBranch = self.t_createBranch.format(name, name, name, 'F' if 'oat' in typ else 'I')

    def copy(self):
        print("COPY ",self.name )
        return self.t_copyVar.format(self.name,self.name)
        
        
class ScalarVar(Var):
    t_declInVar = 'TTreeReaderValue<{}> {}'
    t_initVar = '{}(reader, "{}")'
    t_declOutVar = '{} out_{}'
    t_createBranch = 'outTree->Branch("{}",&out_{} , "{}/{}")'
    t_copyVar = 'out_{} = *currentReader->{}'
    

class WeightVar(ScalarVar):
    t_copyVar = 'out_{} = (*currentReader->{})*currentReader->sampleWFactor'

    
    
class VectVar(Var):        
    t_declInVar = 'TTreeReaderArray<{}> {}'
    t_initVar = '{}(reader, "{}")'
    t_declOutVar = '{} out_{}'
    t_createBranch = ' outTree->Branch("{}",&out_{} , "{}/{}")'
    t_copyVar = 'out_{} = currentReader->{}[i]'

    def __init__(self, name, typ):        
        typ = typ.replace('vector<','')[:-1]
        Var.__init__(self, name, typ)

    def resetOutVect(self):
        # by default, we're outputing flat ttree : no need to reset output vectors.
        return ""


class VectVarOutVec(VectVar):        
    t_declOutVar = 'vector<{}> out_{};'
    t_createBranch = ' outTree->Branch("{}",&out_{}  ); // {} {}'
    t_copyVar = '(out_{})[i] = currentReader->{}[i]'
    def resetOutVect(self):
        # by default, we're outputing flat ttree : no need to reset output vectors.
        return f"out_{self.name} = vector<{self.typ}>(currentReader->{self.name}.begin() , currentReader->{self.name}.end() )"

    def copy(self):
        return ""
    
class FixEinGeV(VectVar):
    ## Very specific case to fix stupid issue in a private ntuple
    t_copyVar = 'out_{} = 0.001*currentReader->{}[i]'

class FracOfLeadingJet(VectVar):
    t_declInVar = 'int dumy_FracToLeadingJet /* {} {} */'
    t_initVar = 'dumy_FracToLeadingJet(0) /* {} {} */'
    t_copyVar = "if(refVec.GetSize()>0){{ out_{} = currentReader->PTVAR[i] / currentReader->PTVAR[0]; /* {} */ }}".replace("PTVAR", nameForPtFrac)
    


# ***************************
#  Builds the c++ source code needed to flatten the given input TChain c
def buildCode(c):
    global refName
    varList = []
    seenNames = set()
    for l in c.GetListOfLeaves():
        typ = l.GetTypeName()
        name = l.GetName()
        if name in seenNames:
            print("!!! appearing twice : ",name)
            continue
        seenNames.add(name)
        if name in vetoVariables:
            continue
        if name in []: #["Qw","m","m_true"]:
            varList.append( FixEinGeV(name,typ))
        elif 'vector' in typ:
            refName = refName or name
            v = VectVar(name,typ) if outputFlat else VectVarOutVec(name,typ)
            varList.append( v )
        elif name==eventWeightName:
            varList.append( WeightVar(name,typ) )
        else:
            varList.append( ScalarVar(name,typ))        
            
    if nameForPtFrac != "":
        varList.append( FracOfLeadingJet("frac_leadPt","vector<float>"))
        
    from string import Template
    templateCode = Template(open('treeFlattener.cpp').read())
    return varList, templateCode.substitute(
        declInVar = ';\n'.join( [ v.declInVar for v in varList ] +[''] ),
        declOutVar = ';\n'.join( [ v.declOutVar for v in varList ]+[''] ),
        createBranch = ';\n'.join( [ v.createBranch for v in varList ]+[''] ),
        scalarCopy = ';\n'.join( [ v.copy() for v in varList if isinstance(v,ScalarVar) ] +['']),
        vectorCopy = ';\n'.join( [ v.copy() for v in varList if isinstance(v,VectVar) ] +[''] ),
        ref = refName,
        varInit = ',\n'.join( [ v.initVar for v in varList ] ),
        vecInput = "#define VECTOR_INPUT" if refName != None else "",
        resetOutVect = ';\n'.join( [ v.resetOutVect() for v in varList if isinstance(v,VectVar) ] +[''] ),
        outputFlat = int(outputFlat),
        eventLevelCut = eventLevelCut,
    )



# ***************************
#  Setup the Flattener C++ object. This is done by 1st generating the C++ code according to the input TTree  variable content,
#  then compiling this code, then setting up JetReader object for each of the JZ slices.
def createFlattener(outName='out', maxEvents=None, neventPerOutFile=-1, saveCode=None, loadCode=None):
    varList, code = buildCode(chains[0])
    if saveCode:
        open(saveCode, 'w').write(code)
    if loadCode:
        # for debugging, we might prefer to load a precompiled librairie
        if loadCode.endswith('.cpp'):
            ROOT.gROOT.ProcessLine(f".L {loadCode}++g")
        else:
            ROOT.gSystem.Load(loadCode)
    else:
        print( code)    
        ROOT.gInterpreter.LoadText(code)
    f = ROOT.Flattener()
    f.varList = varList
    f.outFileName = outName
    if maxEvents : f.maxEvents = maxEvents
    f.neventPerOutFile = neventPerOutFile
    py_jetreaders =[]
    for s,c, norm in zip(samples,chains, normalizations):
        r = ROOT.JetReader()
        r.sampleWFactor = xsectionFactor(s,norm)
        r.init(c)
        py_jetreaders.append(r)
        f.jetreaders.push_back(r) 
    f.init()
    f.totNumEntries = sum( [c.GetEntries() for c in chains])
    f.py_jetreaders = py_jetreaders

    def run(self):
        # remove previous file (ROOT will not owverwrite in this case)
        from os import remove
        for outF in glob.glob(outName+"*.root"):
            remove(outF)
        self.cpp_run()
    ROOT.Flattener.run = run
    
    return f




def testRnd():
    f = createFlattener()

    h = ROOT.TH1F("t","t", 10,0,10)
    for i in xrange(10000):
        h.Fill(f.rndReaderIndex() )
    h.Draw()
    hc=h.GetCumulative()
    hc.Draw()
    hc.Scale(1./hc.GetBinContent(9))
    hc.Print("all")
    list(f.cumulDist)

    return f,h, hc


def testResult(outName):
    outc = ROOT.TChain(inputTreeName)
    for ff in glob.glob(outName+"*.root"):
        outc.AddFile(ff)
    return outc

    
